%% Analysis of a signal
%  ------------------------------------------------------------------------
clf; cla; fig = figure(1); hold on

%% Read data (Srivathsan time-discontinuous space-time data)
fid = fopen('data-spacetime.txt');
dat = textscan(fid, '%f\t%f\t%f\t%f %*[^\n]'); % read first and second column
fclose(fid);

time = dat{:,1};
data = dat{:,2};

time = time(1:2:end); % take every second data value (time-discontinuous!)
data = data(1:2:end);

time = time(end/2+1:end); % select second half of time history (limit cycle reached)
data = data(end/2+1:end);

%% Read data (earthquake El Centro)
% fid = fopen('data-elcentro.txt');
% dat = textscan(fid, '%f %f %*[^\n]'); % read first and second column
% fclose(fid);
% 
% time = dat{:,1};
% data = dat{:,2};

%% Define some test data
% start_time = 1.0;    % start time of signal
% end_time = 5.0;      % end time of signal
% num_samples = 10000; % number of data samples
% 
% time = linspace(start_time, end_time, num_samples);
% 
% % simple harmonic signals 
% %data = 1.0 * sin(time * 1.0 * (2*pi)); % harmonic of 1.0 Hz with a = 1.0
% %data = 1.2 * sin(time * 3.0 * (2*pi)); % harmonic of 3.0 Hz with a = 1.2
% %data = 0.4 * sin(time * 0.5 * (2*pi)); % harmonic of 0.5 Hz with a = 0.4
% %data = 0.5 * sin(time * 8.5 * (2*pi)); % harmonic of 8.5 Hz with a = 0.4
% % a signal composed of three harmonics
% data = 0.6 * cos(time * 2.0 * (2*pi)) + 1.2 * sin(time * 3.5 * (2*pi)) + 0.8 * sin(time * 7.0 * (2*pi));

%% Plot original (non-periodic) signal
subplot('Position',[0.035 0.05 0.45 0.90]); hold on;
plot(time, data, '-b');
% labels
xlabel('Time [s]');
ylabel('Amplitude [m]');
title('Time History (of e.g. a Displacement)');

%% Discrete Fourier Transform (DFT)
N = length(data);
dt = time(2) - time(1);

y = fft(data);

frequency = (0:N-1)/dt/N;
amplitude = abs(y)*2/N;

%% Plot amplitude spectrum of the DFT
subplot('Position',[0.52 0.05 0.465 0.90]); hold on;
xlim([ 0 1 ]); % make sure you take an appropriate window for the frequency!
bar(frequency(1:end/2), amplitude(1:end/2), 0.5);
% labels
xlabel('Frequency [Hz] = [1/s]');
ylabel('Amplitude [m]');
title('Amplitude Spectrum');
grid on;
