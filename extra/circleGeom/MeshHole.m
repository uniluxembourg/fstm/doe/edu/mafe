%% Describe the goemetry of the problem for the mesh generator
% helfpul parameters (problem specific)
n = 10;% should be >1! % d: length measure; n = number of grid nodes per d
% create all geo points

gp1 = mgen.MgGeoPoint( [-1, -1] );
gp2 = mgen.MgGeoPoint( [1, -1] );
gp3 = mgen.MgGeoPoint( [1, 1] );
gp4 = mgen.MgGeoPoint( [-1, 1] );
gcenter = mgen.MgGeoPoint([0.,0.]);
gl1  = mgen.MgGeoLine2Point( [gp1, gp2], n );
gl2  = mgen.MgGeoLine2Point( [gp2, gp3], n );
gl3  = mgen.MgGeoLine2Point( [gp3, gp4], n );
gl4  = mgen.MgGeoLine2Point( [gp4, gp1], n );

%number of ring => refine near the circle
a= 0.5%
b= 0.5%
c=0; %ellipse rotation angle in rad
normFunc = mgen.MgNormFunction( mgen.MgNormFunctionType.Quadratic);

ga1  = mgen.MgGeoArea4LineEllipsoidalVoid([gl1,gl2,gl3,gl4],20,a,b,c,normFunc)

% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], ...
                  [gl1, gl2, gl3, gl4], ...
                  [ga1] );
% print geo and mesh data info
fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot();