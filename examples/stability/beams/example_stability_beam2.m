%% Example Beam2
% A horizontal beam in the plane composed of multiple (N) beam finite element
% subject to axial load
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% General properties
L = 1.0;  % length of the cantilever
N = 10;   % number of finite elements
%% Definition of sections
s1 = mafe.Section1D();
s1.E  = 2.0e11; % [N/m^2]
s1.Iy = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A  = 1.0e-4; % [m^2] = 0.01m * 0.01m
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for ii = 1:N+1
    % create node
    nodes(end+1) = mafe.Node2D( [L/N*(ii-1), 0.0] );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ii = 1:N
    % create element
    elems(end+1) = mafe.Member2D( [nodes(ii), nodes(ii+1)], s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% Constraint: kinematic, clamped at the left
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Rota3, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint: kinematic, pinned at the right
const(end+1) = mafe.ConstraintNode( nodes(end), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint: static, axial force at the right
const(end+1) = mafe.ConstraintNode( nodes(end), mafe.DofType.Disp1, mafe.ConstraintType.Neumann  ,-1.0 );
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: stability
ana = mafe.FeAnalysisStability(fep);
%% Calculate response
ana.analyse();
%% Extract some data from stability analysis
%ana.L
%ana.X
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
ana.putModeToDof(1)
fep.plotSystem('deformed');
fep.printSystem();
