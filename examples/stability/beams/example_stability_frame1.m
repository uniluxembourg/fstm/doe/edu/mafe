%% Example Frame1
% A half-open frame structure with axial forces in the left colum and hinge
% at right support
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E  = 2.0e11; % [N/m^2]
s1.Iy = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A  = 1.0e-4; % [m^2] = 0.01m * 0.01m
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -1.0e5 ); % [N/m]
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [0.0, 1.0] );
% Node 3
n3 = mafe.Node2D( [1.0, 1.0] );
% Node 4
n4 = mafe.Node2D( [1.0, 0.0] );
% Vector of nodes
nodes = [ n1, n2, n3, n4 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Member2D( [n1, n2], s1 );
% Element 2
e2 = mafe.Member2D( [n2, n3], s1, l1 );
% Element 3
e3 = mafe.Member2D( [n3, n4], s1 );
% vector of elements
elems = [ e1, e2, e3 ];
%% Definition of constraints in the system
% Constraints: fully clamped at left support
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c2 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
c3 = mafe.ConstraintNode( n1, mafe.DofType.Rota3, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint: vertical force in left column, applied at top
c4 = mafe.ConstraintNode( n2, mafe.DofType.Disp2, mafe.ConstraintType.Neumann,  -0.0 );
% Constarint: pin supported at the right
c5 = mafe.ConstraintNode( n4, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c6 = mafe.ConstraintNode( n4, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% vector of constraints
const = [ c1, c2, c3, c4, c5, c6 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: stability
ana = mafe.FeAnalysisStability(fep);
%% Calculate response
ana.analyse();
%% Extract some data from stability analysis
ana.L % eigenvalues = buckling load factors
ana.X % eigenvectors = buckling shapes
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
%ana.putModeToDof(3)
fep.plotSystem('deformed');
ana.putModeToDof(1)
fep.plotSystem('deformed');
fep.printSystem();
