%% Example Beam1a
% A one-dimensional beam composed of a single beam finite element: cantilever
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E   = 2.0e11; % [N/m^2]
s1.Iy  = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A   = 5.0e-4; % [m^2] = 0.01m * 0.05m
s1.rho = 8.0e+3; % [kg/m^3]
s1.d_w = 0.0e+0; % [N/(m*s)]
s1.d_alpha = 0.1000;
s1.d_beta  = 0.0010;
%% Definition of time functions: Omega, factor_cosine, factor_sine
t1 = mafe.TimeFunction( 125, 0.0, 1.0 ); % sine   time function for prescribed vertical base displacement
t2 = mafe.TimeFunction( 200, 1.0, 0.0 ); % cosine time function for vertical tip force
% vector of time functions
tfuns = [ t1, t2 ];
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -2.e3 ); % [N/m]
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node1D( [0.0] );
% Node 2
n2 = mafe.Node1D( [1.0] );
% Vector of nodes
nodes = [ n1, n2 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Beam1D( [n1, n2], s1, l1 );
% vector of elements
elems = [ e1 ];
%% Definition of constraints in the system
% Constraint 1 (clamped at left end)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.05, t1 ); % [m]
c2 = mafe.ConstraintNode( n1, mafe.DofType.Rota3, mafe.ConstraintType.Dirichlet, 0.00     ); % [m]
c3 = mafe.ConstraintNode( n2, mafe.DofType.Disp2, mafe.ConstraintType.Neumann,  -1.e3, t2 ); % [N]
% vector of constraints
const = [ c1, c2, c3 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: dynamic
ana = mafe.FeAnalysisDynamicTD( fep, tfuns );
%% Calculation init
ana.initialise( 0.001 );
%% Compute and visualise time steps
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
for ii = 1:200
   clf; hold on; xlim([0 1]); ylim([-0.5 0.5])
   ana.solveTimeStep();
   fep.plotSystem('deformed');
   fep.printSystem();
   drawnow;
end
