%% Example Beam1c
% A one-dimensional beam composed of a single beam finite element: 2-point bending
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E   = 1.0e00; % [N/m^2]
s1.Iy  = 1.0e-0; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A   = 1.0e-0; % [m^2] = 0.01m * 0.05m
s1.rho = 1.0e+0; % [kg/m^3]
s1.d_w = 0.0e+0; % [N/(m*s)]
s1.d_alpha = 1.0000;
s1.d_beta  = 0.0100;
%% Definition of time functions: Omega, factor_cosine, factor_sine
t1 = mafe.TimeFunction( 3.1416, 0.0, 1.0 ); % sine   time function for prescribed vertical base displacement (left)
t2 = mafe.TimeFunction( 6.2832, 1.0, 0.0 ); % cosine time function for vertical tip force
% vector of time functions
tfuns = [ t1, t2 ];
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -1 ); % [N/m]
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node1D( [0.0] );
% Node 2
n2 = mafe.Node1D( [0.5] );
% Node 3
n3 = mafe.Node1D( [1.0] );
% Vector of nodes
nodes = [ n1, n2, n3 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Beam1D( [n1, n2], s1, l1 );
% Element 2
e2 = mafe.Beam1D( [n2, n3], s1, l1 );
% vector of elements
elems = [ e1, e2 ];
%% Definition of constraints in the system
% Constraint 1 (simple support at left)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.00, t1 ); % [m]
% Constraint 2 (simple support at right)
c2 = mafe.ConstraintNode( n3, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.00     ); % [m]
% Constraint 3 (single vertial force)
c3 = mafe.ConstraintNode( n2, mafe.DofType.Disp2, mafe.ConstraintType.Neumann,  -1.00, t2 ); % [N]
% vector of constraints
const = [ c1, c2, c3 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );

%% Define initial conditions
u0 = zeros(fep.ndofs,1);
v0 = zeros(fep.ndofs,1);

%% Select an analysis type: dynamic in frequency domain
ana_fd = mafe.FeAnalysisDynamicFD( fep, tfuns );
%% Calculate response
ana_fd.analyse();
%% Apply initial conditions
ana_fd.applyInitialConditions( u0, v0 );

deltaT = 0.0125; % time step size for time-domain analysis

%% Select an analysis type: dynamic in time domain
ana_td = mafe.FeAnalysisDynamicTD( fep, tfuns );
%% Calculation init
ana_td.initialise( deltaT );
%% Apply initial conditions
ana_td.applyInitialConditions( u0, v0 );

tt = deltaT * [1:round(4.0/deltaT)]; % compute interval up to 4s
uu = [];
ff = [];

cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); clf; axis equal; hold on;
for time = tt
   clf; hold on; xlim([0 1]); ylim([-0.5 0.5])
   % frequency domain result
   ana_fd.putSolToDof(time);
   fep.plotSystem('deformed');
   %fep.printSystem();
   u_fd = fep.nodes(1).lhs( fep.nodes(1).dof == mafe.DofType.Rota3 ); % left rotation angle
   f_fd = fep.nodes(1).rhs( fep.nodes(1).dof == mafe.DofType.Disp2 ); % left vertical support force
   % time domain result
   ana_td.solveTimeStep();
   fep.plotSystem('deformed');
   %fep.printSystem();
   u_td = fep.nodes(1).lhs( fep.nodes(1).dof == mafe.DofType.Rota3 ); % left rotation angle
   f_td = fep.nodes(1).rhs( fep.nodes(1).dof == mafe.DofType.Disp2 ); % left vertical support force
   % store both results
   uu = [uu [u_fd; u_td]];
   ff = [ff [f_fd; f_td]];
   % draw
   drawnow;
end

fig = figure(2); subplot('Position',[0.05 0.05 0.90 0.90]); clf; hold on;
plot(tt,uu(1,:), 'r-')
plot(tt,uu(2,:), 'b.')
fig = figure(3); subplot('Position',[0.05 0.05 0.90 0.90]); clf; hold on;
plot(tt,ff(1,:), 'r-')
plot(tt,ff(2,:), 'b.')
