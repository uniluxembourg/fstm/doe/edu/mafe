%% Example Flow around a cylinder
% example on how to use the integrated mesh generator for
% a rectangular domain with immersed fixed circular cylinder
% constant inflow on the left, slip at top/bottom, zero pressure at the right
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.rho   = 1.0;    % [kg/m^3]
s1.mu    = 1.0;    % [Ns/m^2]
s1.t     = 1.0;    % [m]
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% function that controls grid node distribution along geo lines
nf1 = mgen.MgNormFunction( mgen.MgNormFunctionType.PowRev, [0.0, 1.2] );
% create all geo points
gp1 = mgen.MgGeoPoint( [0.0, 0.0] );
gp2 = mgen.MgGeoPoint( [1.0, 0.0] );
gp3 = mgen.MgGeoPoint( [1.0, 1.0] );
gp4 = mgen.MgGeoPoint( [0.0, 1.0] );
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1], 20 ); % ask for 4 grid nodes along this line
gl2 = mgen.MgGeoLine2Point( [gp3, gp2], 20 ); % ask for 6 grid nodes along this line
gl3 = mgen.MgGeoLine2Point( [gp4, gp3], 20 ); % this will adapt automatically
gl4 = mgen.MgGeoLine2Point( [gp1, gp4], 20 ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4LineEllipsoidalVoid( [gl1, gl2, gl3, gl4], 22, 0.1, 0.1, 0.0, nf1 );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
% cla; clf;
% fig = figure(1); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% geo.plot();
% return;
% %  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.NavierStokes2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% bottom edge: zero velo in x2
nn = nodes( [gl1.getGridNodes.id] ); % get nodes on GeoLine 1 (bottom)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo2, mafe.ConstraintType.Dirichlet, 0.0 );
% top edge: zero velo in x2
nn = nodes( [gl3.getGridNodes.id] ); % get nodes on GeoLine 3 (top)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo2, mafe.ConstraintType.Dirichlet, 0.0 );
% right edge: outflow zero pressure and zero velo2
nn = nodes( [gl2.getGridNodes.id] ); % get nodes on GeoLine 2 (right)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Press, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo2, mafe.ConstraintType.Dirichlet, 0.0 );
% left edge: inflow velo1
nn = nodes( [gl4.getGridNodes.id] ); % get nodes on GeoLine 4 (left)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo1, mafe.ConstraintType.Dirichlet, 1.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo2, mafe.ConstraintType.Dirichlet, 0.0 );
% inner ring: zero velocity
nn = nodes( [ga1.getGridNodesInner.id] ); % get nodes on inner ring of GeoArea 1
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo1, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Velo2, mafe.ConstraintType.Dirichlet, 0.0 );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblemLinearised( nodes, elems, const );
%% Select an analysis type: steady state of nonlinear problem
ana = mafe.FeAnalysisSteadyNonlinear(fep);
disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%% Calculate response
ana.analyse();
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
%fep.plotSystem('pressure');
%fep.plotSystem('velocity', 1.e-2);

fep.plotSystem('vorticity');
