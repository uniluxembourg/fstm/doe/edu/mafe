%% Example Frame2b
% Framework bridge in the plane (with heavier pedestrian pathway)
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E   = 2.0e11; % [N/m^2]
s1.Iy  = 1.7e-5; % [m^4] = 0.02m * (0.10m)^3 / 12
s1.A   = 2.0e-3; % [m^2] = 0.02m * 0.10m
s1.rho = 8.0e+3; % [kg/m^3]
s1.d_w = 0.0e+0; % [kN/(m*s)]
s1.d_alpha = 0.000;
s1.d_beta  = 0.000;
s2 = mafe.Section1D();
s2.E   = 5.0e10; % [N/m^2]
s2.Iy  = 1.7e-3; % [m^4] = 2.00m * (0.10m)^3 / 12
s2.A   = 2.0e-1; % [m^2] = 2.00m * 0.10m
s2.rho = 3.5e+4; % [kg/m^3]
s2.d_w = 0.0e+0; % [kN/(m*s)]
s2.d_alpha = 0.000;
s2.d_beta  = 0.000;
%% Definition of time functions: Omega, factor_cosine, factor_sine
t1 = mafe.TimeFunction( 20, 0.0, 1.0 ); % time function for prescribed vertical base displacement, Omega is very high
% vector of time functions
tfuns = [ t1 ];
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -0.0 ); % [N/m]
%% Definition of nodes in the system
n01 = mafe.Node2D( [ 0.0, 0.0] );
n02 = mafe.Node2D( [ 2.0, 0.0] );
n03 = mafe.Node2D( [ 4.0, 0.0] );
n04 = mafe.Node2D( [ 6.0, 0.0] );
n05 = mafe.Node2D( [ 8.0, 0.0] );
n06 = mafe.Node2D( [10.0, 0.0] );
n07 = mafe.Node2D( [12.0, 0.0] );
n08 = mafe.Node2D( [ 2.0, 2.0] );
n09 = mafe.Node2D( [ 4.0, 2.0] );
n10 = mafe.Node2D( [ 6.0, 2.0] );
n11 = mafe.Node2D( [ 8.0, 2.0] );
n12 = mafe.Node2D( [10.0, 2.0] );
% Vector of nodes
nodes = [ n01, n02, n03, n04, n05, n06, n07, n08, n09, n10, n11, n12 ];
%% Definition of elements in the system
e01 = mafe.Member2D( [n01, n02], s2, l1 );
e02 = mafe.Member2D( [n02, n03], s2, l1 );
e03 = mafe.Member2D( [n03, n04], s2, l1 );
e04 = mafe.Member2D( [n04, n05], s2, l1 );
e05 = mafe.Member2D( [n05, n06], s2, l1 );
e06 = mafe.Member2D( [n06, n07], s2, l1 );
e07 = mafe.Member2D( [n08, n09], s1, l1 );
e08 = mafe.Member2D( [n09, n10], s1, l1 );
e09 = mafe.Member2D( [n10, n11], s1, l1 );
e10 = mafe.Member2D( [n11, n12], s1, l1 );
e11 = mafe.Member2D( [n02, n08], s1, l1 );
e12 = mafe.Member2D( [n03, n09], s1, l1 );
e13 = mafe.Member2D( [n04, n10], s1, l1 );
e14 = mafe.Member2D( [n05, n11], s1, l1 );
e15 = mafe.Member2D( [n06, n12], s1, l1 );
e16 = mafe.Member2D( [n01, n08], s1, l1 );
e17 = mafe.Member2D( [n02, n09], s1, l1 );
e18 = mafe.Member2D( [n03, n10], s1, l1 );
e19 = mafe.Member2D( [n05, n10], s1, l1 );
e20 = mafe.Member2D( [n06, n11], s1, l1 );
e21 = mafe.Member2D( [n07, n12], s1, l1 );
% vector of elements
elems = [ e01, e02, e03, e04, e05, e06, e07, e08, e09, e10, ...
          e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21 ];
%% Definition of constraints in the system
% Constraint 1 and 2 (fixed vertically and horizontally at node 1)
c1 = mafe.ConstraintNode( n01, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0      );
c2 = mafe.ConstraintNode( n01, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.02, t1 ); % [m] = 2 cm max amplitude
% Constraint 3 and 4 (fixed vertically and horizontally at node 7)
c3 = mafe.ConstraintNode( n07, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c4 = mafe.ConstraintNode( n07, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.02, t1 ); % [m] = 2 cm max amplitude
% vector of constraints
const = [ c1, c2, c3, c4 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select a	n analysis type: dynamic
ana = mafe.FeAnalysisDynamicFD( fep, tfuns );
%% Calculate response
ana.analyse();
%% Visualise system response (individual modes of vibration = eigenvectors)
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
ana.putModeToDof(1, 'real');
fep.plotSystem('reference');
fep.plotSystem('deformed');
disp('press key to proceed'); pause;
%% Visualise system response (homogeneous solution for given initial conditions)
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% apply initial conditions
modeshape = real(ana.X(:,1:2:fep.ndofs)); % get some selected (or all) modes
u0 =  0.00 * modeshape(:,1) +  0.00 * modeshape(:,2);
v0 =  0.00 * modeshape(:,1) +  0.00 * modeshape(:,2);
ana.applyInitialConditions( u0, v0 );
% evaluate and plot for given time instants
video = VideoWriter( mfilename('fullpath') );
open(video);
% first undamped eigenfrequency is oemga = ~25 [rad/s] -> T = 2pi/omega = 0.25 [s]
% excitation: Omega = 20 [rad/s] -> T = 2pi/Omega = 0.3 [s]
for tt = 0:0.01:1.0
    ana.putSolToDof(tt);
    clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
    xlim([0 12])
    ylim([-1 3])
    fep.plotSystem('reference');
    fep.plotSystem('deformed');
    writeVideo(video, getframe);
    drawnow;
end
close(video);
