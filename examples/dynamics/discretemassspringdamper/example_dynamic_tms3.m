%% Example SpringMassNet
% A system composed of one mass connected by rheological devices (2D setup)
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section0D();
s1.k  =  1.0; % [N/m]
s1.d  =  0.0; % [Ns/m]
s2 = mafe.Section0D();
s2.m  =  1.0; % [Ns^2/m]
%% Definition of time functions: Omega, factor_cosine, factor_sine
t1 = mafe.TimeFunction(  1.5, 0.0, 0.0 ); % time function = 0.1*cos(1.5*t)
% vector of time functions
tfuns = [ t1 ];
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [1.0, 1.0] );
% Node 3
n3 = mafe.Node2D( [2.0, 0.0] );
% Node 4
n4 = mafe.Node2D( [1.0, 2.0] );
% Vector of nodes
nodes = [ n1, n2, n3, n4 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.RheoKelvinVoigt2D( [n2, n1], s1 );
% Element 2
e2 = mafe.RheoKelvinVoigt2D( [n2, n3], s1 );
% Element 3
e3 = mafe.RheoKelvinVoigt2D( [n2, n4], s1 );
% Element 4
e4 = mafe.Point( n2, s2, mafe.DofType.Disp1 );
% Element
e5 = mafe.Point( n2, s2, mafe.DofType.Disp2 );
% vector of elements
elems = [ e1, e2, e3, e4, e5 ];
%% Definition of constraints in the system
% Constraint 1, 2 (fixed at n1)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
c2 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
% Constraint 3, 4 (fixed at n3)
c3 = mafe.ConstraintNode( n3, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
c4 = mafe.ConstraintNode( n3, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
% Constraint 5, 6 (fixed at n4)
c5 = mafe.ConstraintNode( n4, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
c6 = mafe.ConstraintNode( n4, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.00 ); % [m]
% Constraint 5 (force at mass)
%c5 = Constraint( n2, DofType.Disp1, ConstraintType.Dirichlet, 0.00 ); % [N]
% vector of constraints
const = [ c1, c2, c3, c4, c5, c6 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: dynamic
ana = mafe.FeAnalysisDynamicFD( fep, tfuns );
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% Select mode
ana.putModeToDof(1, 'real');
fep.plotSystem('reference');
fep.plotSystem('deformed');
disp('press key to proceed'); pause;
% Visualise system response (total solution for given initial conditions)
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% apply initial conditions
modeshape = real(ana.X(:,1:2:4)); % get some selected (or all) modes
u0 =  0.20 * modeshape(:,1) +  0.10 * modeshape(:,2);
v0 =  0.00 * modeshape(:,1) +  0.00 * modeshape(:,2);
ana.applyInitialConditions( u0, v0 );
% evaluate and plot for given time instants
video = VideoWriter( mfilename('fullpath') );
open(video);
for tt = 0:0.1:20 % first undamped eigenfrequency is oemga = ~1.0 [rad/s] -> T = 2pi/omega = 6.28 [s]
    ana.putSolToDof(tt);
    clf; hold on; axis equal;
    xlim([0 2])
    ylim([0 2])
    fep.plotSystem('reference');
    fep.plotSystem('deformed');
    writeVideo(video, getframe);
    drawnow;
end
close(video);
