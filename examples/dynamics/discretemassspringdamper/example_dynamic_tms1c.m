%% Example SpringMassDamper
% A system composed of two masses connected by rheological devices (car on bridge), here vertically oriented
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section0D();
s1.k  = 350000.0; % [N/m]
s1.d  =      0.0; % [Ns/m]
s2 = mafe.Section0D();
s2.k  =  70000.0; % [N/m]
s2.d  =   3000.0; % [Ns/m]
s3 = mafe.Section0D();
s3.m  =     50.0; % [Ns^2/m]
s4 = mafe.Section0D();
s4.m  =    300.0; % [Ns^2/m]
%% Definition of time functions: Omega, factor_cosine, factor_sine
t1 = mafe.TimeFunction(  32., 1.0, 0.0 ); % time function = 0.1*cos(1.5*t)
% vector of time functions
tfuns = [ t1 ];
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [0.0, 0.5] );
% Node 3
n3 = mafe.Node2D( [0.0, 1.0] );
% Vector of nodes
nodes = [ n1, n2, n3 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.RheoKelvinVoigt1D( [n1, n2], s1, mafe.DofType.Disp2 );
% Element 2
e2 = mafe.RheoKelvinVoigt1D( [n2, n3], s2, mafe.DofType.Disp2 );
% Element 3
e3 = mafe.Point( n2, s3, mafe.DofType.Disp2 );
% Element 4
e4 = mafe.Point( n3, s4, mafe.DofType.Disp2 );
% vector of elements
elems = [ e1, e2, e3, e4 ];
%% Definition of constraints in the system
% Constraint 1 (fixed at left end)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 ); % [m]
% vector of constraints
const = [ c1 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: dynamic
ana = mafe.FeAnalysisDynamicFD( fep, tfuns );
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% Select mode
ana.putModeToDof(1, 'real');
fep.plotSystem('reference');
fep.plotSystem('deformed');
disp('press key to proceed'); pause;
% Visualise system response (total solution for given initial conditions)
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
% apply initial conditions
modeshape = real(ana.X(:,1:2:fep.ndofs)); % get some selected (or all) modes
u0 =  0.50 * modeshape(:,1) +  1.00 * modeshape(:,2);
v0 =  0.00 * modeshape(:,1) +  0.00 * modeshape(:,2);
ana.applyInitialConditions( u0, v0 );
% evaluate and plot for given time instants
video = VideoWriter( mfilename('fullpath') );
open(video);
%for tt = 0:0.05:10
for tt = 0:0.01:1 % first undamped eigenfrequency is oemga = ~13.85 [rad/s] -> T = 2pi/omega = 0.4 [s]
    ana.putSolToDof(tt);
    clf; hold on;
    ylim([-0.1 1.3])
    xlim([-0.25 0.25])
    fep.plotSystem('reference');
    fep.plotSystem('deformed');
    writeVideo(video, getframe);
    drawnow;
end
close(video);
