%% Example Beam1b
% A one-dimensional cantilever beam composed of N finite beam elements
% subject to initial conditions 
% with visualisation of the resulting amplitude spectrum
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% General properties
L = 1.0;  % length of the cantilever
N = 8;    % number of finite elements
%% Definition of sections
s1 = mafe.Section1D(); % beam element section
s1.E   = 2.0e11; % [N/m^2]
s1.Iy  = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A   = 5.0e-4; % [m^2] = 0.01m * 0.05m
s1.rho = 8.0e+3; % [kg/m^3]
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for ii = 1:N+1
    % create node
    nodes(end+1) = mafe.Node1D( [L/N*(ii-1)] );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ii = 1:N
    % create element
    elems(end+1) = mafe.Beam1D( [nodes(ii), nodes(ii+1)], s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% Constraint 1 and 2 (clamped at left end)
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Rota3, mafe.ConstraintType.Dirichlet, 0.0 );
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: dynamic
ana = mafe.FeAnalysisDynamicFD( fep );
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); hold on;
% apply initial conditions
u0 = 0.20 * rand(fep.ndofs,1); % put some (small) random values
v0 = 0.05 * rand(fep.ndofs,1);
% u0 = 0.2 * ana.X(:,1) + 0.10 * ana.X(:,3); % put a combination of modes
% v0 = 0.0 * ana.X(:,1) + 0.00 * ana.X(:,3); % 1 (=1st distinct mode) and 3 (=2nd distinct mode)
ana.applyInitialConditions( u0, v0 );
% obtain and plot the amplitude spectrum
omega = abs(imag(ana.L(1:2:end)));
ampli = abs(ana.A(1:2:end)) * 2.; % complex-conjugate contributes
plot(omega,ampli,'o');
bar(omega,ampli);