%% Example Plate Bending 2ax
% example on how to use the integrated mesh generator for
% a rectangular plate strip - in x1 direction - with constant area load
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2] |??steel
s1.nue   = 0.0;    % [-]
s1.t     = 0.01;   % [m]
%% Definition of element loads
p1 = mafe.EleLoad( mafe.DofType.Disp3, -1.0 ); % vertical load in z [N/m^2]
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% create all geo points
gp1 = mgen.MgGeoPoint( [ 0.0, 0.0] );
gp2 = mgen.MgGeoPoint( [10.0, 0.0] );
gp3 = mgen.MgGeoPoint( [10.0, 1.0] );
gp4 = mgen.MgGeoPoint( [ 0.0, 1.0] );
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1], 11 ); % ask for 4 grid nodes along this line
gl2 = mgen.MgGeoLine2Point( [gp3, gp2],  2 ); % ask for 6 grid nodes along this line
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],  2 ); % this will adapt automatically
gl4 = mgen.MgGeoLine2Point( [gp1, gp4],  2 ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
fprintf(geo.print());
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Plate2D( nodes( [ ge.nodes.id ] ), s1, p1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% left edge: simply supported
nn = nodes( [gl4.getGridNodes.id] ); % get nodes on GeoLine 4 (left)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 );
%const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 );
% right edge: simply supported
nn = nodes( [gl2.getGridNodes.id] ); % get nodes on GeoLine 2 (right)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 );
%const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
%fep.plotSystem('reference');
fep.plotSystem('deformed', 1e2);
fep.plotSystem('moment11'); colorbar();
%fep.plotSystem('tensor', 1.e-2);
fep.printSystem();
