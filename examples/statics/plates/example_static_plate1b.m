%% Example Plate 1b
% A two-dimensional quadratic plate structure composed of 5 distorted elements
% -> standard patch test (using nonrectangular-shaped quadrilateral elements)
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.1e6;  % [N/m^2]
s1.nue   = 0.0;    % [-]
s1.t     = 1.0e-3; % [m]
s1.kappa = 5./6.;
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [ 0.0, 10.0] );
% Node 2
n2 = mafe.Node2D( [ 0.0,  0.0] );
% Node 3
n3 = mafe.Node2D( [ 4.0,  7.0] );
% Node 4
n4 = mafe.Node2D( [ 2.0,  2.0] );
% Node 5
n5 = mafe.Node2D( [ 8.0,  7.0] );
% Node 6
n6 = mafe.Node2D( [ 8.0,  3.0] );
% Node 7
n7 = mafe.Node2D( [10.0, 10.0] );
% Node 8
n8 = mafe.Node2D( [10.0,  0.0] );
% Vector of nodes
nodes = [ n1, n2, n3, n4, n5, n6, n7, n8 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Plate2D( [n1, n2, n4, n3], s1 );
% Element 2
e2 = mafe.Plate2D( [n2, n8, n6, n4], s1 );
% Element 3
e3 = mafe.Plate2D( [n4, n6, n5, n3], s1 );
% Element 4
e4 = mafe.Plate2D( [n3, n5, n7, n1], s1 );
% Element 5
e5 = mafe.Plate2D( [n6, n8, n7, n5], s1 );
% vector of elements
elems = [ e1, e2, e3, e4, e5 ];
%% Definition of constraints in the system
testcase = 'bend';
% vector of constraints
switch(testcase)
case 'bend'
    % impose constant m11, support in Disp3
    %
    const = [ mafe.ConstraintNode( n1, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n1, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n7, mafe.DofType.Rota1, mafe.ConstraintType.Neumann,   1.0 ),
              mafe.ConstraintNode( n8, mafe.DofType.Rota1, mafe.ConstraintType.Neumann,   1.0 ),
              ]';
case 'shear'
    % impose constant shear
    %
    const = [ mafe.ConstraintNode( n1, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n1, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n3, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n4, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n5, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n6, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n7, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n8, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n1, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n3, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n4, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n5, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n6, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n7, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n8, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n8, mafe.DofType.Disp3, mafe.ConstraintType.Neumann,   1.0 ),
              mafe.ConstraintNode( n7, mafe.DofType.Disp3, mafe.ConstraintType.Neumann,   1.0 ),
              ]';
case 'twist'
    % impose constant twist
    %
    const = [ mafe.ConstraintNode( n1, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n2, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n8, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ),
              mafe.ConstraintNode( n7, mafe.DofType.Disp3, mafe.ConstraintType.Neumann,   1.0 ),
              ]';
otherwise
    disp('Unknown test case! Constraints not set');
    const = [];
end
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
%fep.plotSystem('deformed', 1.e0);
%fep.plotSystem('moment11'); colorbar;
%fep.plotSystem('shear13'); colorbar;
fep.plotSystem('tensor', 1.e0);
fep.printSystem();
