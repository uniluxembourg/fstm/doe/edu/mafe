%% Example Plate Bending 2b
% example on how to use the integrated mesh generator for
% a rectangular plate with Navier-type support and constant area load, 1/4 plate
% in addition, the FE solution is compared with the analytical solution at midpoint
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E   = 1.7e10; % [N/m^2] | concrete (compression)
s1.nue = 0.20;   % [-]
s1.t   = 0.16;   % [m]
%% Definition of element loads
p1 = mafe.EleLoad( mafe.DofType.Disp3, -1.e-3*s1.E*s1.t^3 ); % vertical load in z [N/m^2]
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% helfpul parameters (problem specific)
lx = 8.0; ly = 4.0; nx = 4*4; ny = nx*ly/lx; % lx, ly: total lengths ; nx, ny: number of elements per direction
% function that controls grid node distribution along geo lines
nf1 = mgen.MgNormFunction( mgen.MgNormFunctionType.Pow   , [0.0, 0.1] );
nf2 = mgen.MgNormFunction( mgen.MgNormFunctionType.PowRev, [0.0, 0.1] );
% create all geo points
gp1 = mgen.MgGeoPoint( [ 0.0,  0.0] );
gp2 = mgen.MgGeoPoint( [lx/2,  0.0] );
gp3 = mgen.MgGeoPoint( [lx/2, ly/2] );
gp4 = mgen.MgGeoPoint( [ 0.0, ly/2] );
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1],  nx+1 ); % ask for nx+1 grid nodes along this line
gl2 = mgen.MgGeoLine2Point( [gp3, gp2],  ny+1 ); % ask for ny+1 grid nodes along this line
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],     2 ); % this will adapt automatically
gl4 = mgen.MgGeoLine2Point( [gp1, gp4],     2 ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
%fprintf(geo.print());
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Plate2D( nodes( [ ge.nodes.id ] ), s1, p1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% left edge: vertical support (Navier-type support)
nn = nodes( [gl4.getGridNodes.id] ); % get nodes on GeoLine 4 (left)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 ); % hard support (Kirchhoff)
% lower edge: vertical support (Navier-type support)
nn = nodes( [gl1.getGridNodes.id] ); % get nodes on GeoLine 1 (bottom)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 ); % hard support (Kirchhoff)
% right edge: bending angle Rota1!=0 (because of symmetry about y-axis, 1/4 plate)
nn = nodes( [gl2.getGridNodes.id] ); % get nodes on GeoLine 2 (right)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota1, mafe.ConstraintType.Dirichlet, 0.0 );
% upper edge: bending angle Rota2!=0 (because of symmetry about x-axis, 1/4 plate)
nn = nodes( [gl3.getGridNodes.id] ); % get nodes on GeoLine 3 (top)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Rota2, mafe.ConstraintType.Dirichlet, 0.0 );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
%fep.plotSystem('reference');
%fep.plotSystem('deformed', 1e1); view(45.0, 25.0);
%fep.plotSystem('shear13'); colorbar();
fep.plotSystem('tensor', 1.e+2/(s1.E*s1.t^3));
%fep.printSystem();
% ------------------------------------------COMPUTE REFERENCE SOLUTION VALUES---

% reference solution based on double fourier series
u_z_a  = 0.0; m_xx_a = 0.0; m_yy_a = 0.0; % displacement, bending moments
tx = 64*4; ty = tx/2; % series terms to be evaluated per direction

B = s1.E * s1.t^3 / (12*(1-s1.nue^2)); % plate bending stiffness
G = ( s1.E / (2*(1.0 + s1.nue)) ); % shear modulus

x = lx/2; y = ly/2; % point of evaluation: at centre

for m = 1:2:tx %
    for n = 1:2:ty %
        % coefficients from load function
        p_mn = 16 * p1.p(mafe.DofType.Disp3)/(pi^2*m*n); % constant area load p1
        % series contribution to transversal displacement and derivatives
        zx = (m/lx)^2; zy = (n/ly)^2; zz = zx + zy;
        f = p_mn / (B*pi^4*zz^2);
        g = 1.0 + B*pi^2/(G*s1.kappa*s1.t) * zz;
        %
        s = sin(m*pi*x/lx) * sin(n*pi*y/ly);
        %
        w      =  f * g * s            ;
        phix_x =  f     * s * zx*(pi^2);
        phiy_y =  f     * s * zy*(pi^2);
        % compute values
        u_z_a  = u_z_a  + w;
        m_xx_a = m_xx_a + B * (          phix_x + s1.nue * phiy_y );
        m_yy_a = m_yy_a + B * ( s1.nue * phix_x +          phiy_y );
    end
end

% obtain data from the FE analysis: access nodal solution, element solution
centre_node = nodes( gp3.getGridNodes.id );
centre_elem = elems( ny ); % by inspection

u_z_c  = centre_node.lhs( centre_node.dof == mafe.DofType.Disp3 );
m_xx_c = centre_elem.internal(1,2); % mxx (1) at node (2) (by inspection of mesh)
m_yy_c = centre_elem.internal(2,2); % myy (2) at node (2) (by inspection of mesh)

% compute L2 error
u_z_e  = sqrt( ( u_z_c  - u_z_a  )^2 / u_z_a^2  );
m_xx_e = sqrt( ( m_xx_c - m_xx_a )^2 / m_xx_a^2 );
m_yy_e = sqrt( ( m_yy_c - m_yy_a )^2 / m_yy_a^2 );

% output
fprintf( 'Results for central point (x1,x2) = (%2.2f,%2.2f):\n', lx/2, ly/2 );
fprintf( '%6s%30s\t%30s\t%30s\n', '---', ...
                                  sprintf('analytical (%3dx%3d)', tx, ty), ...
                                  sprintf('computed (%4dx%4d)', nx, ny), ...
                                  sprintf('relative L2 error') );
fprintf( '%6s%30s\t%30s\t%30s\n', sprintf('u_z'), ...
                                  sprintf('%+4.8e', u_z_a), ...
                                  sprintf('%+4.8e', u_z_c), ...
                                  sprintf('%4.8e' , u_z_e) );
fprintf( '%6s%30s\t%30s\t%30s\n', sprintf('m_xx'), ...
                                  sprintf('[Kirchhoff] %+4.8e', m_xx_a), ...
                                  sprintf('%+4.8e', m_xx_c), ...
                                  sprintf('%4.8e' , m_xx_e) );
fprintf( '%6s%30s\t%30s\t%30s\n', sprintf('m_yy'), ...
                                  sprintf('[Kirchhoff] %+4.8e', m_yy_a), ...
                                  sprintf('%+4.8e', m_yy_c), ...
                                  sprintf('%4.8e' , m_yy_e) );
