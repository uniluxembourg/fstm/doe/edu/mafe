%% Example Membrane in Plane Stress 2d
% example on how to use the integrated mesh generator for
% a L-shaped domain under constant vertical distributed boundary load
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2]
s1.nue   = 0.3;    % [-]
s1.t     = 1.0;    % [m]
s1.state = 'plane_stress';
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% function that controls grid node distribution along geo lines
nf1 = mgen.MgNormFunction( mgen.MgNormFunctionType.Pow   , [0.0, 1.0] );
nf2 = mgen.MgNormFunction( mgen.MgNormFunctionType.PowRev, [0.0, 1.0] );
% helpful parameters (problem specific)
d = 0.005; n = 2; % d: length unit; n = number of grid nodes per d
% create all geo points
gp1 = mgen.MgGeoPoint( [0*d, 0*d] );
gp2 = mgen.MgGeoPoint( [4*d, 0*d] );
gp3 = mgen.MgGeoPoint( [0*d, 3*d] );
gp4 = mgen.MgGeoPoint( [4*d, 3*d] );
gp5 = mgen.MgGeoPoint( [8*d, 3*d] );
gp6 = mgen.MgGeoPoint( [0*d, 8*d] );
gp7 = mgen.MgGeoPoint( [4*d, 8*d] );
gp8 = mgen.MgGeoPoint( [8*d, 8*d] );
% create all geo lines
gl1  = mgen.MgGeoLine2Point( [gp1, gp2], 4*n, nf2 );
gl2  = mgen.MgGeoLine2Point( [gp3, gp4],   2, nf2 );
gl3  = mgen.MgGeoLine2Point( [gp6, gp7],   2, nf2 );
gl4  = mgen.MgGeoLine2Point( [gp4, gp5], 4*n, nf1 );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],   2, nf1 );
gl6  = mgen.MgGeoLine2Point( [gp1, gp3], 3*n, nf2 );
gl7  = mgen.MgGeoLine2Point( [gp2, gp4],   2, nf2 );
gl8  = mgen.MgGeoLine2Point( [gp3, gp6], 5*n, nf1 );
gl9  = mgen.MgGeoLine2Point( [gp4, gp7],   2, nf1 );
gl10 = mgen.MgGeoLine2Point( [gp8, gp5],   2, nf2 );
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl7, gl2, gl6] );
ga2 = mgen.MgGeoArea4Line( [gl2, gl9, gl3, gl8] );
ga3 = mgen.MgGeoArea4Line( [gl4, gl10, gl5, gl9] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10], ...
                  [ga1, ga2, ga3] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot(); disp('Showing generated mesh. Press key to continue...'); pause
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% left edge: fix in x1
nn = nodes( [gl6.getGridNodes.id, gl8.getGridNodes.id] ); % get nodes on GeoLine 6+8
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% bottom edge: fix in x2
nn = nodes( [gl1.getGridNodes.id] ); % get nodes on GeoLine 4
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% top right edge: vertical distributed force q [N/m]
nn = nodes( [gl5.getGridNodes.id] ); % get nodes on GeoLine 5
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [0.0,-1.e4] );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed', 1e5); % amplification by factor 10^5 for visualisation
%fep.plotSystem('vonmises'); colorbar;
%fep.plotSystem('tensor', 1.e-7);
%fep.printSystem();
