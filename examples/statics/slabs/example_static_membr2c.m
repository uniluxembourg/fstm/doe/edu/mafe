%% Example Membrane in Plane Stress 2c
% example on how to use the integrated mesh generator for
% a 2-edge supported rectangle with cosine vertical load q along the top edge
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2]
s1.nue   = 0.0;    % [-]
s1.t     = 1.0e-2; % [m]
s1.state = 'plane_stress';
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% helpful parameters (problem specific)
a = 2.0; b = 0.5; % a: half total width; b: half total height
% create all geo points
gp1  = mgen.MgGeoPoint( [-a, -b] );
gp2  = mgen.MgGeoPoint( [ 0, -b] );
gp3  = mgen.MgGeoPoint( [+a, -b] );
gp4  = mgen.MgGeoPoint( [-a,  0] );
gp5  = mgen.MgGeoPoint( [ 0,  0] );
gp6  = mgen.MgGeoPoint( [+a,  0] );
gp7  = mgen.MgGeoPoint( [-a, +b] );
gp8  = mgen.MgGeoPoint( [ 0, +b] );
gp9  = mgen.MgGeoPoint( [+a, +b] );
% create all geo lines
gl1  = mgen.MgGeoLine2Point( [gp1, gp2], 20 );
gl2  = mgen.MgGeoLine2Point( [gp2, gp3], 20 );
gl3  = mgen.MgGeoLine2Point( [gp4, gp5],  2 );
gl4  = mgen.MgGeoLine2Point( [gp5, gp6],  2 );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],  2 );
gl6  = mgen.MgGeoLine2Point( [gp8, gp9],  2 );
gl7  = mgen.MgGeoLine2Point( [gp1, gp4], 10 );
gl8  = mgen.MgGeoLine2Point( [gp2, gp5],  2 );
gl9  = mgen.MgGeoLine2Point( [gp3, gp6],  2 );
gl10 = mgen.MgGeoLine2Point( [gp4, gp7], 10 );
gl11 = mgen.MgGeoLine2Point( [gp5, gp8],  2 );
gl12 = mgen.MgGeoLine2Point( [gp6, gp9],  2 );
% create all geo areas
ga1  = mgen.MgGeoArea4Line( [gl1, gl8 , gl3, gl7 ] ); %
ga2  = mgen.MgGeoArea4Line( [gl2, gl9 , gl4, gl8 ] );
ga3  = mgen.MgGeoArea4Line( [gl3, gl11, gl5, gl10] );
ga4  = mgen.MgGeoArea4Line( [gl4, gl12, gl6, gl11] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8, gp9], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10, gl11, gl12], ...
                  [ga1, ga2, ga3, ga4] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot(); disp('Showing generated mesh. Press key to continue...'); pause
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% left edge: fix in x2
nn = nodes( [gl7.getGridNodes.id, gl10.getGridNodes.id] );  % get nodes on GeoLine 7+10
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% right edge: fix in x2
nn = nodes( [gl9.getGridNodes.id, gl12.getGridNodes.id] );  % get nodes on GeoLine 9+12
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% middle node: fix in x1
nn = nodes( [gp5.getGridNodes.id] ); % get node at GeoPoint 5 (central point)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% top edge: vertical distributed force q = 1e5 * cos(x) [N/m]
nn = nodes( [gl5.getGridNodes.id, gl6.getGridNodes.id] ); % get nodes on GeoLine 5+6 (top)
xx = reshape( [ nn.ref ], 2, [] ); % coordinates of nodes on GeoLines 5+6 : row1 = x, row2 = y
qt =  0.0  * xx(1,:);              % zero tangential force along the edge
qn = -1.e5 * cos( pi/2/a*xx(1,:) );% cosine normal force (directed against to outward normal vector)
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [qt, qn] );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed', 1e2); % amplification by factor 100 for visualisation
%fep.plotSystem('stress11'); colorbar();
%fep.plotSystem('vonmises'); colorbar();
%fep.printSystem();
