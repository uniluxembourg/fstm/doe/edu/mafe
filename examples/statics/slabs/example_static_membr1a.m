%% Example Membrane 1a
% A two-dimensional rectangular plane structure composed of 1 element
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 1.0; % [N/m^2]
s1.nue   = 0.0; % [-]
s1.t     = 1.0; % [m]
s1.state = 'plane_stress';
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [1.0, 0.0] );
% Node 3
n3 = mafe.Node2D( [1.0, 1.0] );
% Node 4
n4 = mafe.Node2D( [0.0, 1.0] );
% Vector of nodes
nodes = [ n1, n2, n3, n4 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Membrane2D( [n1, n2, n3, n4], s1 );
% vector of elements
elems = [ e1 ];
%% Definition of constraints in the system
% Constraint 1 (fixed horizontally at left end, lower node)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 2 (fixed horizontally at left end, upper node)
c2 = mafe.ConstraintNode( n4, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 3 (fixed vertically at left end, lower node)
c3 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 4 (horizontal force at right end, lower node)
c4 = mafe.ConstraintNode( n2, mafe.DofType.Disp1, mafe.ConstraintType.Neumann, 0.1 ); % 0.1 [N]
% Constraint 5 (horizontal force at right end, upper node)
c5 = mafe.ConstraintNode( n3, mafe.DofType.Disp1, mafe.ConstraintType.Neumann, 0.1 ); % 0.1 [N]
% vector of constraints
const = [ c1, c2, c3, c4, c5 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
%fep.plotSystem('stress11'); colorbar;
%fep.plotSystem('tensor');
fep.printSystem();
