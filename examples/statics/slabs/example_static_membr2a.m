%% Example Membrane in Plane Stress 2a
% example on how to use the integrated mesh generator for
% a simple block under constant distributed vertical load q along the top edge
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2]
s1.nue   = 0.0;    % [-]
s1.t     = 1.0e-2; % [m]
s1.state = 'plane_stress';
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% create all geo points
gp1 = mgen.MgGeoPoint( [0.0, 0.0] );
gp2 = mgen.MgGeoPoint( [1.0, 0.0] );
gp3 = mgen.MgGeoPoint( [1.0, 1.0] );
gp4 = mgen.MgGeoPoint( [0.0, 1.0] );
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1],  4 ); % ask for 4 grid nodes along this line
gl2 = mgen.MgGeoLine2Point( [gp3, gp2],  6 ); % ask for 6 grid nodes along this line
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],  2 ); % this will adapt automatically
gl4 = mgen.MgGeoLine2Point( [gp1, gp4],  2 ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot(); disp('Showing generated mesh. Press key to continue...'); pause
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% bottom edge: fix in x2
nn = nodes( [gl1.getGridNodes.id] ); % get nodes on GeoLine 1 (bottom)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% left bottom corner node: fix in x1
nn = nodes( [gp1.getGridNodes.id] ); % get node at GeoPoint 1 (bottom left)
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% top edge: upwards distributed force q = 1e3 [N/m] translates to edge normal load
nn = nodes( [gl3.getGridNodes.id] ); % get nodes on GeoLine 3 (top)
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [0.0, 1.e3] );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed', 1e5);
%fep.plotSystem('stress22'); colorbar();
fep.printSystem();
