%% Example Membrane in Plane Stress 2f
% example on how to use the integrated mesh generator for
% a rectangular block with circular hole
% stressed in horizontal direction leading to stress concentrations,
% meshing with MgGeoLineMulti for application of kinematic boundary conditions
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2]
s1.nue   = 0.0;    % [-]
s1.t     = 1.0e-2; % [m]
s1.state = 'plane_stress';
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% function that controls grid node distribution along geo lines
nf1 = mgen.MgNormFunction( mgen.MgNormFunctionType.QuadraticRev );
% helpful parameters (problem specific)
d = 1.0; n = 8; % d: commom geometrical factor; n = number of grid nodes per d
% create all geo points
gp1 = mgen.MgGeoPoint( [-d, -d] );
gp2 = mgen.MgGeoPoint( [+d, -d] );
gp3 = mgen.MgGeoPoint( [+d, +d] );
gp4 = mgen.MgGeoPoint( [-d, +d] );
gp5 = mgen.MgGeoPoint( [ 0, -d] );
gp6 = mgen.MgGeoPoint( [+d,  0] );
gp7 = mgen.MgGeoPoint( [ 0, +d] );
gp8 = mgen.MgGeoPoint( [-d,  0] );
% create all geo lines
% --- first, some simple 2-point-lines: 2 contributing to an edge
sub1a = mgen.MgGeoLine2Point( [gp2, gp5],  2 ); % sub-line as 2-point-line
sub1b = mgen.MgGeoLine2Point( [gp5, gp1],  2 );

sub2a = mgen.MgGeoLine2Point( [gp3, gp6],  2 );
sub2b = mgen.MgGeoLine2Point( [gp6, gp2],  2 );

sub3a = mgen.MgGeoLine2Point( [gp4, gp7],  2 );
sub3b = mgen.MgGeoLine2Point( [gp7, gp3],  2 );

sub4a = mgen.MgGeoLine2Point( [gp1, gp8],  2 );
sub4b = mgen.MgGeoLine2Point( [gp8, gp4],  2 );
% --- second, construct multilines composed of 2 sub-lines
gl1 = mgen.MgGeoLineMulti( [sub1a, sub1b], 3*n ); % second option: num requested nodes
gl2 = mgen.MgGeoLineMulti( [sub2a, sub2b], 3*n );
gl3 = mgen.MgGeoLineMulti( [sub3a, sub3b],   2 );
gl4 = mgen.MgGeoLineMulti( [sub4a, sub4b],   2 );
% create all geo areas
ga1 = mgen.MgGeoArea4LineEllipsoidalVoid( [gl1, gl2, gl3, gl4], 4*n, 0.2*d, 0.2*d, 0, nf1 ); % circle
%ga1 = MgGeoArea4LineEllipsoidalVoid( [gl1, gl2, gl3, gl4], 4*n, 0.2*d, 0.1*d, 0.15*2*pi, nf1 ); % rotated ellipse
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot(); disp('Showing generated mesh. Press key to continue...'); pause
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% support in Disp1
nn = nodes( [gp5.getGridNodes.id, gp7.getGridNodes.id] ); % get nodes on GeoNode 5+7
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% support in Disp2
nn = nodes( [gp6.getGridNodes.id, gp8.getGridNodes.id] ); % get nodes on GeoNode 6+8
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% horizontal traction on boundary right
nn = nodes( [gl2.getGridNodes.id] ); % get nodes on GeoLine 2 (multiline)
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [0.0, 1.e4] );
% horizontal traction on boundary left
nn = nodes( [gl4.getGridNodes.id] ); % get nodes on GeoLine 2 (multiline)
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [0.0, 1.e4] );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
%fep.plotSystem('reference');
%fep.plotSystem('deformed', 1e4);
fep.plotSystem('stress11'); colorbar();
%fep.plotSystem('vonmises'); colorbar();
%fep.printSystem();
