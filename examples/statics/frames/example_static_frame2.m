%% Example Frame2
% Framework bridge in the plane
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E   = 2.0e11; % [N/m^2]
s1.Iy  = 1.7e-5; % [m^4] = 0.02m * (0.10m)^3 / 12
s1.A   = 2.0e-3; % [m^2] = 0.02m * 0.10m
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -5.0e3 ); % [N/m]
%% Definition of nodes in the system
n01 = mafe.Node2D( [ 0.0, 0.0] );
n02 = mafe.Node2D( [ 2.0, 0.0] );
n03 = mafe.Node2D( [ 4.0, 0.0] );
n04 = mafe.Node2D( [ 6.0, 0.0] );
n05 = mafe.Node2D( [ 8.0, 0.0] );
n06 = mafe.Node2D( [10.0, 0.0] );
n07 = mafe.Node2D( [12.0, 0.0] );
n08 = mafe.Node2D( [ 2.0, 2.0] );
n09 = mafe.Node2D( [ 4.0, 2.0] );
n10 = mafe.Node2D( [ 6.0, 2.0] );
n11 = mafe.Node2D( [ 8.0, 2.0] );
n12 = mafe.Node2D( [10.0, 2.0] );
% Vector of nodes
nodes = [ n01, n02, n03, n04, n05, n06, n07, n08, n09, n10, n11, n12 ];
%% Definition of elements in the system
e01 = mafe.Member2D( [n01, n02], s1, l1 );
e02 = mafe.Member2D( [n02, n03], s1, l1 );
e03 = mafe.Member2D( [n03, n04], s1, l1 );
e04 = mafe.Member2D( [n04, n05], s1, l1 );
e05 = mafe.Member2D( [n05, n06], s1, l1 );
e06 = mafe.Member2D( [n06, n07], s1, l1 );
e07 = mafe.Member2D( [n08, n09], s1, l1 );
e08 = mafe.Member2D( [n09, n10], s1, l1 );
e09 = mafe.Member2D( [n10, n11], s1, l1 );
e10 = mafe.Member2D( [n11, n12], s1, l1 );
e11 = mafe.Member2D( [n02, n08], s1, l1 );
e12 = mafe.Member2D( [n03, n09], s1, l1 );
e13 = mafe.Member2D( [n04, n10], s1, l1 );
e14 = mafe.Member2D( [n05, n11], s1, l1 );
e15 = mafe.Member2D( [n06, n12], s1, l1 );
e16 = mafe.Member2D( [n01, n08], s1, l1 );
e17 = mafe.Member2D( [n02, n09], s1, l1 );
e18 = mafe.Member2D( [n03, n10], s1, l1 );
e19 = mafe.Member2D( [n05, n10], s1, l1 );
e20 = mafe.Member2D( [n06, n11], s1, l1 );
e21 = mafe.Member2D( [n07, n12], s1, l1 );
% vector of elements
elems = [ e01, e02, e03, e04, e05, e06, e07, e08, e09, e10, ...
          e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e21 ];
%% Definition of constraints in the system
% Constraint 1 and 2 (fixed vertically and horizontally at node 1)
c1 = mafe.ConstraintNode( n01, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c2 = mafe.ConstraintNode( n01, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 3 and 4 (fixed vertically and horizontally at node 7)
c3 = mafe.ConstraintNode( n07, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c4 = mafe.ConstraintNode( n07, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% vector of constraints
const = [ c1, c2, c3, c4 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
fep.printSystem();
