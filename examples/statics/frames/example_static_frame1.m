%% Example Frame1
% A frame member in the plane composed of two frame finite element
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E  = 2.0e11; % [N/m^2]
s1.Iy = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.A  = 5.0e-4; % [m^2] = 0.01m * 0.05m
%% Definition of member loads
l1 = mafe.EleLoad( mafe.DofType.Disp2, -8000.0 ); % [N/m]
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [0.5, 0.0] );
% Node 3
n3 = mafe.Node2D( [1.0, 0.0] );
% Vector of nodes
nodes = [ n1, n2, n3 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Member2D( [n1, n2], s1, l1 );
% Element 2
e2 = mafe.Member2D( [n2, n3], s1, l1 );
% vector of elements
elems = [ e1, e2 ];
%% Definition of constraints in the system
% Constraint 1 and 2 (fixed vertically and horizontally at left end)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c2 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 3 (fixed vertically at right end)
c3 = mafe.ConstraintNode( n3, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 4 (horizontal force at right end)
c4 = mafe.ConstraintNode( n3, mafe.DofType.Disp1, mafe.ConstraintType.Neumann,   0.1 );
% vector of constraints
const = [ c1, c2, c3, c4 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
fep.printSystem();
