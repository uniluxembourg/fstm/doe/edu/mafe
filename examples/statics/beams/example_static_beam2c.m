%% Example Beam2c
% A horizontal pin-supported beam in the plane composed of N finite beam elements with a vertical spring at the right end point
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% General properties
L = 1.0;  % length of the cantilever [m]
N = 10;   % number of finite elements
%% Definition of sections
s1 = mafe.Section1D(); % beam element section
s1.E  = 2.0e11; % [N/m^2]
s1.Iy = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s2 = mafe.Section0D(); % point element section
s2.k  = 2.0e+5; % [N/m]
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for ii = 1:N+1
    % create node
    nodes(end+1) = mafe.Node2D( [L/N*(ii-1), 0.0] );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ii = 1:N
    % set up nodal values of the transversal line load p = pval * f(x)
    pval = -8000.0; % [N/m]
    % load factor as function f(x)
    e = 0; % constant load
    %e = 1; % linear load function (based on nodal coordinates)
    %e = 2; % quadratic load fucntion (based on nodeal coordinates)
    p = pval * [ nodes(ii).ref(1), nodes(ii+1).ref(1) ] .^e; % e.g. 1, x, or x^2
    % create element
    elems(end+1) = mafe.Beam2D( [nodes(ii), nodes(ii+1)], s1, mafe.EleLoad( mafe.DofType.Disp2, p ) );
end
% put a Point element for the vertical spring to the right end
elems(end+1) = mafe.Point( nodes(end), s2, mafe.DofType.Disp2 );
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
for ii = 1:N+1
    % since the beam has no axial stiffness, the Disp1 dofs need to be locked
    const(end+1) = mafe.ConstraintNode( nodes(ii), mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
end
% vertical support on the left
const(end+1) = mafe.ConstraintNode( nodes(1), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
fep.printSystem();
