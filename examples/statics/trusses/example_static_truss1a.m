%% Example Truss1a
% A one-dimensional truss composed of a single truss finite element
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E  = 2.0e11; % [N/m^2]
s1.A  = 5.0e-4; % [m^2] = 0.01m * 0.05m
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node1D( [0.0] );
% Node 2
n2 = mafe.Node1D( [1.0] );
% Vector of nodes
nodes = [ n1, n2 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Truss1D( [n1, n2], s1 );
% vector of elements
elems = [ e1 ];
%% Definition of constraints in the system
% Constraint 1 (fixed horizontally at left end)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 2 (axial force at right end)
c2 = mafe.ConstraintNode( n2, mafe.DofType.Disp1, mafe.ConstraintType.Neumann, 100.0e3 ); % 100.000 [N]
% vector of constraints
const = [ c1, c2 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
fep.printSystem();
