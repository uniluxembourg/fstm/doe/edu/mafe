%% Example Truss2b
% A horizontal truss in the plane composed of two truss finite elements
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section1D();
s1.E  = 2.0e11; % [N/m^2]
s1.A  = 5.0e-4; % [m^2] = 0.01m * 0.05m
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [0.5, 0.0] );
% Node 3
n3 = mafe.Node2D( [1.0, 0.0] );
% Vector of nodes
nodes = [ n1, n2, n3 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Truss2D( [n1, n2], s1 );
% Element 2
e2 = mafe.Truss2D( [n2, n3], s1 );
% vector of elements
elems = [ e1, e2 ];
%% Definition of constraints in the system
% Constraint 1 and 2 (fixed vertically and horizontally at left end)
c1 = mafe.ConstraintNode( n1, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
c2 = mafe.ConstraintNode( n1, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% Constraint 3 (axial force at right end)
c3 = mafe.ConstraintNode( n3, mafe.DofType.Disp1, mafe.ConstraintType.Neumann, 100.0e3 ); % 100.000 [N]
% Constraint 4 and 5 (fixed vertically in the middle and at right end)
c4 = mafe.ConstraintNode( n2, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
c5 = mafe.ConstraintNode( n3, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% vector of constraints
const = [ c1, c2, c3, c4, c5 ];
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed');
fep.printSystem();
