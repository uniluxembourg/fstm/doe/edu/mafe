%% Example Truss5a
% Trusswork to "capture" 2D elasticty
addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Some general geometry of the "block"
width = 1.0;
height = 1.0;
nw = 20;
nh = 20;
%% Definition of sections: units [m], [N]
s1 = mafe.Section1D();
s1.E  = 1.0e10; % [N/m^2]
s1.A  = 1.0e-4; % [m^2] = 0.01m * 0.01m
%% Definition of nodes in the system
nodes = mafe.Node2D.empty(0,0);
%
dw = width/(nw-1);
dh = height/(nh-1);
%
for ii = 1:nh     % nodes in the corners of the checkerboard
    for jj = 1:nw
        coordinates = [ (jj-1)*dw, (ii-1)*dh ];
        nodes(end+1) = mafe.Node2D( coordinates );
    end
end
for ii = 1:nh-1   % nodes in the centers of each rectangle
    for jj = 1:nw-1
        coordinates = [ (jj-1+0.5)*dw, (ii-1+0.5)*dh ];
        nodes(end+1) = mafe.Node2D( coordinates );
    end
end
%% Definition of elements in the system
elems = mafe.Truss2D.empty(0,0);
%
for ii = 1:nh     % all horizontal trusses
    for jj = 1:nw-1
        connectivity = [ (ii-1)*nw + jj, (ii-1)*nw + jj + 1 ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
    end
end
for ii = 1:nh-1    % all vertical trusses
    for jj = 1:nw
        connectivity = [ (ii-1)*nw + jj, (ii)*nw + jj ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
    end
end
for ii = 1:nh-1    % all cross trusses
    for jj = 1:nw-1
        center = nw*nh + (ii-1)*(nw-1) + jj;
        connectivity = [ center, (ii-1)*nw + jj     ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
        connectivity = [ center, (ii-1)*nw + jj + 1 ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
        connectivity = [ center, (ii  )*nw + jj + 1 ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
        connectivity = [ center, (ii  )*nw + jj     ];
        elems(end+1) = mafe.Truss2D( nodes(connectivity), s1 );
    end
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
%
const(end+1) = mafe.ConstraintNode( nodes( 1), mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nodes( 1), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nodes(nw), mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
for jj = 1:nw
    const(end+1) = mafe.ConstraintNode( nodes(nw*(nh-1)+jj), mafe.DofType.Disp2, mafe.ConstraintType.Neumann, -100.0e3/nw ); % [N]
end
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse();
%% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
%fep.plotSystem('deformed');
fep.plotSystem('intensity');
%fep.printSystem();
