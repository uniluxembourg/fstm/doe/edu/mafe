%% Example Mesh
% create a rectangle geometry with two 3-point-sides and mesh
clear all; addpath([ '..' filesep '..' filesep ]);

%% Describe the goemetry of the problem for the mesh generator
% create all geo points
gp1 = mgen.MgGeoPoint( [0.0, 0.0] );
gp2 = mgen.MgGeoPoint( [1.0, 0.0] );
gp3 = mgen.MgGeoPoint( [0.8, 0.9] );
gp4 = mgen.MgGeoPoint( [0.0, 1.0] );
gp5 = mgen.MgGeoPoint( [1.2, 0.5] );
gp6 = mgen.MgGeoPoint( [0.5, 0.1] );
% create all geo lines
gl1 = mgen.MgGeoLine3Point( [gp2, gp1, gp6], 9 ); % ask for 9 grid nodes along this line
gl2 = mgen.MgGeoLine3Point( [gp3, gp2, gp5], 9 ); % ask for 9 grid nodes along this line
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],      2 ); % this will adapt automatically
gl4 = mgen.MgGeoLine2Point( [gp1, gp4],      2 ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot();
