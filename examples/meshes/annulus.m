%% Example Mesh
% create a annulus geometry and (refined) mesh
clear all; addpath([ '..' filesep '..' filesep ]);

%% Describe the goemetry of the problem for the mesh generator
% refinement functions
nfr = mgen.MgNormFunction( mgen.MgNormFunctionType.Quadratic );
nfc = mgen.MgNormFunction( mgen.MgNormFunctionType.Quadratic );
% create all geo points
gp1 = mgen.MgGeoPoint( [1.0, 0.0] );
gp2 = mgen.MgGeoPoint( [2.0, 0.0] );
gp3 = mgen.MgGeoPoint( [0.0, 2.0] );
gp4 = mgen.MgGeoPoint( [0.0, 1.0] );
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp1, gp2], 4, nfr ); % ask for 4 grid nodes along this line
gl2 = mgen.MgGeoLine2Point( [gp4, gp3], 2, nfr ); % % this will adapt automatically
gl3 = mgen.MgGeoLine2PointSegment( [gp2, gp3], 2.0, 8, nfc ); % ask for 5 grid nodes along this line
gl4 = mgen.MgGeoLine2PointSegment( [gp1, gp4], 1.0, 2, nfc ); % this will adapt automatically
% create all geo areas
ga1 = mgen.MgGeoArea4LineAnnulus( [gl1, gl3, gl2, gl4] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4], [gl1, gl2, gl3, gl4], [ga1] );
% print geo and mesh data info
% fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot();
