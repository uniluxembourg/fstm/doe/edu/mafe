# mafe
Educational MATLAB framework for finite element analysis of structures

- static linear analysis
- dynamic linear analysis in frequency and time domain

## Installation

Download the ZIP archive and simply extract it to a location of your choice.

Start MATLAB (required minimum version is R2015a), change to one of the 
subfolders in `mafe/examples` folder and run an example:

```
cd mafe/examples/statics/beams
example_static_beam1a
```

## Setting paths correctly

As demonstrated in the example files, you have to set the path to `mafe`'s 
sub-packages correctly:

```
addpath([ '..' filesep '..' filesep '..' filesep ]);
```

The above line assumes that your current script file `my_structure.m` is located 
relative to the location of `mafe`-related packages within the 2-level folder 
hierarchy `my_work_folder/my_case_folder/`:

```
- mafe/
  - +mafe
  - +mgen
  - examples
  - ...
  - my_work_folder/
  	- ...
  	- my_case_folder/
  	- ...
  	- my_structure.m
  	- ...
```

