classdef FeAnalysisSteadyNonlinear < mafe.FeAnalysis
    % steady state nonlinear analysis of finite element problem

    properties
        %
    end

    methods
        %% constructor
        function obj = FeAnalysisSteadyNonlinear(fep)
            if nargin > 0
                obj.fep = fep;
            end
        end
        %% calculate static response
        function analyse(self)
            % make sure the problem is initialised
            self.fep.init();
            % allocate solution vector
            x  = zeros(self.fep.ndofs,1);
            dx = zeros(self.fep.ndofs,1);

            % get system tangent matrix and force vector
            self.fep.updateNodalDofs(x, 0*x);
            L = self.fep.assembleSystemMatrix_Linear_dx();
            p = -L*x;

            % apply constraints
            [f          ] = self.fep.applyConstraintsNeumann(p);
            [f, dbc, act] = self.fep.applyConstraintsDirichlet(L, f);

            ii = 0;

            for lambda = linspace(0.0, 1.0, 10)

                x(dbc) = f(dbc)*lambda;

                self.fep.updateNodalDofs(x, 0*x);

            for iter = 1:12

                % get system tangent matrix and force vector

                tic
                A = L + self.fep.assembleSystemMatrix_Nonlinear_dx();
                toc

                tic
                p = -L*x - self.fep.assembleSystemForceVector();
                toc

                % apply constraints
                [f          ] = self.fep.applyConstraintsNeumann(p);
                [f, dbc, act] = self.fep.applyConstraintsDirichlet(A, f);
                % solve system of equations (active indices only)
                dx(act) = A(act,act)\f(act);

                if iter == 1
                    dx(dbc) = f(dbc)*lambda;
                else
                    dx(dbc) = 0.0;
                end

                % update solution
                x = x + dx;

                % feedback of solution values (x and r) to nodal dofs
                self.fep.updateNodalDofs(x, A*dx-p);

                % cla; clf;
                % fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
                % self.fep.plotSystem('pressure');
                % self.fep.plotSystem('velocity', 1.e-4);
                % saveas(gcf,sprintf('nscyl_%04d',ii),'png');

                ii = ii + 1;

                % check
                n = norm(dx);
                %n = dx'*p;
                fprintf( sprintf('lambda = %5.5f | iter = %2d | norm = %4.6e \n', lambda, iter, n) );

                if (n < 1.e-9)
                    break;
                end
            end

            end

            % postprocess elements
            self.fep.postprocessElements(x);

        end
    end

end
