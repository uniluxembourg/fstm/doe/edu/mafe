classdef Beam1D < mafe.Li2Element1D
    % beam finite element in axial direction only (1D)

    properties
		%
    end

    methods
        %% constructor
        function obj = Beam1D(varargin)
            obj = obj@mafe.Li2Element1D(varargin{:});
        end
        % ======================================================================
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp2 mafe.DofType.Rota3 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
            disp2 = find( [self.node.dof] == mafe.DofType.Disp2 );
            rota3 = find( [self.node.dof] == mafe.DofType.Rota3 );
            ndidx = [ self.node.idx ];
            idx = [ ndidx(disp2(1)), ndidx(rota3(1)), ndidx(disp2(2)), ndidx(rota3(2)) ];
        end
        % ======================================================================
        %% calculate transformation matrix local->global
        function [L, T] = transform(self)
            L = 2.*det( self.J(0.0) );
            T = eye(4); % not needed in fact
        end
        % ======================================================================
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            EI = self.sect.E * self.sect.Iy;
            % element matrix
            h1 = 2*EI/L;
            h2 = 6*EI/L^2;
            h3 = 12*EI/L^3;
            ele_mat = [  +h3,  +h2,  -h3,  +h2 ; ...
                         +h2, 2*h1,  -h2,  +h1 ; ...
                         -h3,  -h2,  +h3,  -h2 ; ...
                         +h2,  +h1,  -h2, 2*h1 ];
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            rhoA = self.sect.rho * self.sect.A;
            % local element matrix
            h1 = rhoA/420 * L^3;
            h2 = rhoA/420 * L^2;
            h3 = rhoA/420 * L;
            ele_mat = [  156*h3,  22*h2,  54*h3, -13*h2 ; ...
                         22*h2,    4*h1,  13*h2,  -3*h1 ; ...
                         54*h3,   13*h2, 156*h3, -22*h2 ; ...
                        -13*h2,   -3*h1, -22*h2,   4*h1 ];
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            d_w = self.sect.d_w;
            % local element matrix
            h1 = d_w/420 * L^3;
            h2 = d_w/420 * L^2;
            h3 = d_w/420 * L;
            ele_mat = [  156*h3,   22*h2,  54*h3, -13*h2 ; ...
                          22*h2,    4*h1,  13*h2,  -3*h1 ; ...
                          54*h3,   13*h2, 156*h3, -22*h2 ; ...
                         -13*h2,   -3*h1, -22*h2,   4*h1 ];
            % provide damping matrix based on Rayleigh assumption
            % get parameters
            d_alpha = self.sect.d_alpha;
            d_beta  = self.sect.d_beta;
            % D = alpha * M + beta * K
            ele_mat = ele_mat + d_alpha * self.mass() + d_beta * self.stiffness();
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            pX([1, 3]) = self.load.p(mafe.DofType.Disp2,:);
            pX([2, 4]) = self.load.p(mafe.DofType.Rota3,:);
            % local element vector
            ele_vec = L/6./210 * [   468, -630/L,   162, -630/L; ...
                                    L*66,    126,  L*39,   -126; ...
                                     162,  630/L,   468,  630/L; ...
                                   -L*39,   -126, -L*66,    126 ] * pX';
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            % coordinates and displacements
            x1 = [ n1.ref(1), n2.ref(1) ];
            uu = [ n1.lhs( find(n1.dof == mafe.DofType.Disp2) ), ...
                   n1.lhs( find(n1.dof == mafe.DofType.Rota3) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Disp2) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Rota3) ) ];
            %
            function H = interpolate_hermite(z)
                [L, T] = self.transform();
                H = [ 0.25*(1.-z).^2 .*( 2.+z), ...
                      0.25*(1.-z).^2 .*( 1.+z) * L/2., ...
                      0.25*(1.+z).^2 .*( 2.-z), ...
                      0.25*(1.+z).^2 .*(-1.+z) * L/2. ];
            end
            NN = 20;
            xx = linspace(x1(1),x1(2),NN);
            zz = linspace(-1,+1,NN);
            ww = interpolate_hermite(zz') * uu';
            %
            % defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                case 'deformed'
                    XX1 = xx;
                    XX2 = ww*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
                    falpha = 0.6;
                    lwidth = 2.0;
                    mwidth = 20.;
                otherwise
                    plot@mafe.Li2Element1D(self, config, scale);
                    return;
            end

            mafe.patchline(XX1, XX2, ...
                     'EdgeColor', lcolor, ...
                     'LineWidth', lwidth, ...
                     'EdgeAlpha', falpha);

            plot(XX1([1 end]), XX2([1 end]), ...
                     marker, ...
					 'color', mcolor, ...
					 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            str = sprintf('%+4.3e ', [ +self.internal(1), ... % left Q
                                       -self.internal(2), ... % left M
                                       -self.internal(3), ... % right Q
                                       +self.internal(4)  ]); % right M
        end
    end

end
