classdef Member2D < mafe.Li2Element2D
    % member (= beam + truss) finite element in the plane (2D)

    properties
        %
    end

    methods
        %% constructor
		function obj = Member2D(varargin)
            obj = obj@mafe.Li2Element2D(varargin{:});
            obj.internal = zeros(6);
        end
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp1 mafe.DofType.Disp2 mafe.DofType.Rota3 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
			      disp1 = find( [self.node.dof] == mafe.DofType.Disp1 );
			      disp2 = find( [self.node.dof] == mafe.DofType.Disp2 );
			      rota3 = find( [self.node.dof] == mafe.DofType.Rota3 );
			      ndidx = [ self.node.idx ];
			      idx = [ ndidx(disp1(1)), ndidx(disp2(1)), ndidx(rota3(1)), ndidx(disp1(2)), ndidx(disp2(2)), ndidx(rota3(2)) ];
        end
        %% calculate transformation matrix local->global
        function [L, T] = transform(self)
            dx1 = self.node(2).ref(1) - self.node(1).ref(1);
            dx2 = self.node(2).ref(2) - self.node(1).ref(2);
            L = sqrt( dx1^2 + dx2^2 );
            T = 1/L*[ dx1, dx2,   0,   0,   0,   0; ...
                     -dx2, dx1,   0,   0,   0,   0; ...
                        0,   0,   L,   0,   0,   0; ...
                        0,   0,   0, dx1, dx2,   0; ...
                        0,   0,   0,-dx2, dx1,   0; ...
                        0,   0,   0,   0,   0,   L ];
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % global element matrix
            ele_mat = self.stiffnessElastic();
        end
        %% calculation of element elastic stiffness matrix
        function ele_mat = stiffnessElastic(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            EA = self.sect.E * self.sect.A ;
            EI = self.sect.E * self.sect.Iy;
            % local element matrix
            h1 = 2*EI/L;
            h2 = 6*EI/L^2;
            h3 = 12*EI/L^3;
            h4 = EA/L;
            ele_mat_local = [ +h4,    0,    0,  -h4,    0,    0 ;
                                0,  +h3,  +h2,    0,  -h3,  +h2 ;
                                0,  +h2, 2*h1,    0,  -h2,  +h1 ;
                              -h4,    0,    0,  +h4,    0,    0 ;
                                0,  -h3,  -h2,    0,  +h3,  -h2 ;
                                0,  +h2,  +h1,    0,  -h2, 2*h1 ];
            % global element matrix
            ele_mat = T' * (ele_mat_local) * T;
        end
        %% calculation of element geometric stiffness matrix
        function ele_mat = stiffnessGeometric(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            N0 = self.internal(4); % extract normal force
            % local element matrix: geometric stiffness
            h5 = N0*6/5/L;
            h6 = N0/10;
            h7 = N0*L/30;
            ele_mat_local = [   0,    0,    0,    0,    0,    0 ;
                                0,  +h5,  +h6,    0,  -h5,  +h6 ;
                                0,  +h6, 4*h7,    0,  -h6,  -h7 ;
                                0,    0,    0,    0,    0,    0 ;
                                0,  -h5,  -h6,    0,  +h5,  -h6 ;
                                0,  +h6,  -h7,    0,  -h6, 4*h7 ];
            % global element matrix
            ele_mat = T' * (ele_mat_local) * T;
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            rhoA = self.sect.rho * self.sect.A;
            % local element matrix
            h1 = rhoA/420 * L^3;
		        h2 = rhoA/420 * L^2;
		        h3 = rhoA/420 * L;
            h4 = rhoA/6   * L;
            ele_mat_local = [ 2*h4,      0,       0,  1*h4,      0,      0 ; ...
                                 0, 156*h3,   22*h2,     0,  54*h3, -13*h2 ; ...
		                             0,  22*h2,    4*h1,     0,  13*h2,  -3*h1 ; ...
                              1*h4,      0,        0, 2*h4,      0,      0 ; ...
		                             0,  54*h3,   13*h2,     0, 156*h3, -22*h2 ; ...
		                             0, -13*h2,   -3*h1,     0, -22*h2,   4*h1 ];
            % global element matrix
            ele_mat = T' * ele_mat_local * T;
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % provide damping matrix based on Rayleigh assumption
            % get parameters
            d_alpha = self.sect.d_alpha;
            d_beta  = self.sect.d_beta;
            % D = alpha * M + beta * K
            ele_mat = d_alpha * self.mass() + d_beta * self.stiffness();
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            px1 = self.load.p(mafe.DofType.Disp1,:);
            px2 = self.load.p(mafe.DofType.Disp2,:);
            pxr = self.load.p(mafe.DofType.Rota3,:);
            % local element vector
            pX([1,4]) = T(1,1)*px1 + T(1,2)*px2;
            pX([2,5]) = T(2,1)*px1 + T(2,2)*px2;
            pX([3,6]) = pxr;
            % local element vector
            ele_vec_local = L/6./210 * [ 420,     0,      0, 210,     0,      0; ...
                                           0,   468, -630/L,   0,   162, -630/L; ...
                                           0,  L*66,    126,   0,  L*39,   -126; ...
                                         210,     0,      0, 420,     0,      0; ...
                                           0,   162,  630/L,   0,   468,  630/L; ...
                                           0, -L*39,   -126,   0, -L*66,    126 ] * pX';
            % global element vector
            ele_vec = T' * ele_vec_local;
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            % coordinates and displacements
            [L, T] = self.transform();
            x1 = [ n1.ref(1), n2.ref(1) ];
            x2 = [ n1.ref(2), n2.ref(2) ];
            aa = [ n1.lhs( find(n1.dof == mafe.DofType.Disp1) ), ...
                   n1.lhs( find(n1.dof == mafe.DofType.Disp2) ), ...
                   n1.lhs( find(n1.dof == mafe.DofType.Rota3) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Disp1) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Disp2) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Rota3) )];
            aa = (T*aa')';
            %
            function N = interpolate_hermite(z)
                [L, T] = self.transform();
                N = [ (0.25*(1.-z).^2) .*(2.+z), ...
                      (0.25*(1.-z).^2) .*( 1.+z) * L/2., ...
                      (0.25*(1.+z).^2) .*(2.-z), ...
                      (0.25*(1.+z).^2) .*(-1.+z) * L/2. ];
            end
            function N = interpolate_linear(z)
                N = [ 0.5*(1.-z), 0.5*(1.+z) ];
            end
            NN = 20;
            zz = linspace(-1,+1,NN);
            xx1 = interpolate_linear(zz') * x1';
            xx2 = interpolate_linear(zz') * x2';
            uu  = interpolate_linear(zz')  * aa([1,4])';
            ww  = interpolate_hermite(zz') * aa([2,3,5,6])';
            ww1 = T(1,1) * uu + T(2,1) * ww;
            ww2 = T(1,2) * uu + T(2,2) * ww;
            %
      			% defaults
      			lcolor = [0.6 0.6 0.6];
      			mcolor = [0.2 0.2 0.2];
      			lwidth = 0.5;
      			mwidth = 10.;
      			ecolor = [0.4 0.4 0.4];
      			marker = '.';
      			%
            switch config
                case 'deformed'
                    XX1 = xx1+ww1*scale;
                    XX2 = xx2+ww2*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
					          falpha = 0.6;
                    lwidth = 1.0;
                    mwidth = 20.;
                case 'intensity'
                    XX1 = xx1;
                    XX2 = xx2;
					          normalforce = 0.5 * ( -self.internal(1) + self.internal(4) );
                    if normalforce < 0.0
                        lcolor = 'b';%[15.3, 31.0, 54.5]/100;
                    elseif normalforce > 0.0
                        lcolor = 'r';%[82.7, 28.4, 16.9]/100;
                    else
                        lcolor = 'g';%[58.8, 65.5,  7.8]/100;
                    end
                    mcolor = [0.0 0.0 0.0 0.0];
					          falpha = 0.6;
                    lwidth = 2.0;
                    mwidth = 0.1;
				        otherwise
                    plot@mafe.Li2Element2D(self, config, scale);
                    return;
            end

			      mafe.patchline(XX1, XX2, ...
                     'EdgeColor', lcolor, ...
                     'LineWidth', lwidth, ...
                     'EdgeAlpha', falpha);

            plot(XX1([1 end]), XX2([1 end]), ...
                     marker, ...
					 'color', mcolor, ...
					 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            str = sprintf('%+4.3e ', [ -self.internal(1), ... % left N
                                        self.internal(2), ... % left Q
                                       -self.internal(3), ... % left M
                                        self.internal(4), ... % right N
                                       -self.internal(5), ... % right Q
                                        self.internal(6)  ]); % right M
        end
    end

end
