
classdef ConstraintType < uint8
    % Enumeration of available constraint types
    enumeration
        Dirichlet (1), % constraint of Dirichlet type, e.g. displacement
        Neumann   (2), % constraint of Neumann type, e.g. force
    end
end
