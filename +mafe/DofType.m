classdef DofType < uint8
    % Enumeration of available dof types
    enumeration
        Disp1 ( 1), % displacement in y1
        Disp2 ( 2), % displacement in y2
        Disp3 ( 3), % displacement in y3
        Rota1 ( 4), % rotation about y1
        Rota2 ( 5), % rotation about y2
        Rota3 ( 6), % rotation about y3
        %
        Velo1 ( 7), % velocity in y1
        Velo2 ( 8), % velocity in y2
        Velo3 ( 9), % velocity in y3
        Omeg1 (10), % rotation velocity about y1
        Omeg2 (11), % rotation velocity about y2
        Omeg3 (12), % rotation velocity about y3
        %
        Press (13), % pressure
        %
        BoundaryStress1 (14), % boundary stress in y1 = Lagrange multiplier
        BoundaryStress2 (15), % boundary stress in y2
        BoundaryStress3 (16)  % boundary stress in y3
    end
end
