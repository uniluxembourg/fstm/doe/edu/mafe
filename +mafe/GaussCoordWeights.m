%% Gauss integration parameters
function [GC, GW] = GaussCoordWeights(num)
    % Gauss integration parameters, local coordinate system z = [-1,+1]
    switch(num)
    case 1
        GC = [ 0. ]; % 1D Gauss coordinates: up to P(1)
        GW = [ 2. ]; % 1D Gauss weights: sum=2
    case 2
        GC = [-sqrt(1./3.), +sqrt(1./3.)]; % up to P(3)
        GW = [          1.,           1.]; %
    case 3
        GC = [-sqrt(3./5.),    0., +sqrt(3./5.)]; % up to P(5)
        GW = [       5./9., 8./9.,        5./9.]; %
    otherwise
        disp('Quadrature.GaussCoordWeights(num) : Order is not implemented!')
    end
end
