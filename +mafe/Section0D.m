classdef Section0D < mafe.Section
    % A rheological device descriptor

    properties
        % material properties
        k = 0.0; % stiffness coefficient
        d = 0.0; % damping coefficient
        m = 0.0; % mass/moment of inertia
        % advanced
        d_alpha = 0.0;
        d_beta  = 0.0;
    end

    methods
        %
    end

end
