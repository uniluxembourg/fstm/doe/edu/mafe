classdef Node < matlab.mixin.Heterogeneous & handle
    % A generic node

    properties
        dof = mafe.DofType.empty(0,0); % vector of active dof types
        idx = uint32.empty(0,0); % vector of system ids of dofs
        lhs = double.empty(0,0); % vector of dof (lhs) unknowns
        rhs = double.empty(0,0); % vector of dof (rhs) reactions
    end

    methods
        %% print nodal data
        function str = print(self)
            str = '';
            for i = 1:length(self.dof)
                str = strcat( str, sprintf('%8s=(%+4.3e|%+4.3e)', ...
                    char(self.dof(i)), self.lhs(i), self.rhs(i)) );
            end
        end
    end

end
