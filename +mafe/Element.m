classdef Element < matlab.mixin.Heterogeneous & handle
    % A generic finite element

    properties
        internal = [];
    end

    methods
        %------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, u, du, ddu)
            if ~exist('ddu', 'var')
                ddu = zeros(length(u),1);
            end
            if ~exist('du', 'var')
                du = zeros(length(u),1);
            end
            % transformation matrix
            [L, T] = self.transform();
            % perform post-processing for local element forces
            f = self.stiffness() * u + self.damping() * du + self.mass() * ddu;
            % consider the effect of the element-local external forces
            self.internal = T * ( f - self.force() ); % TODO: only static elememt load considered here, time fucntions?!
        end
        %------------------------------------------------------------------
        %% placeholder -> graphical output: plot element
        function plot(self, config)
            % do nothing here
        end
        %% placeholder -> print element data
        function str = print(self)
            str = '';
        end
    end

end
