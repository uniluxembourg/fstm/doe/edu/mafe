classdef ConstraintEdge < mafe.Constraint
    % Constraint on degree of freedom along a polygonal edge composed of many nodes

    properties
        node ; % the nodes in correct order (polygon)
        cval ; % the given contraint value(s), can be scalar or vectorial
               % (assumed to act in the local edge coordinate system, not in the global one)
    end

    methods
        %% constructor
        function obj = ConstraintEdge(node, cdof, type, cval, tfun)
            if nargin >= 4
                obj.node = node;
                obj.cdof = cdof;
                obj.type = type;
                obj.cval = cval;
            end
            if nargin >= 5
                obj.tfun = tfun;
            end
            % consistency checks and convenience
            if length(node) < 2
                disp('ConstraintEdge: Error with given number of nodes < 2 !');
            end
            if length(cval) == 1 && length(obj.cdof) == 1 % single scalar given
                obj.cval = cval * ones(1,length(node));
            end
            if length(cval) == 2 && length(obj.cdof) == 2 % single vector given
                obj.cval = [ cval(1) * ones(1,length(obj.node)), cval(2) * ones(1,length(obj.node)) ];
            end
            if length(obj.cval) ~= length(obj.node)*length(obj.cdof)
                warning('ConstraintEdge: Error with given number of values != num nodes * num doftypes!')
            end
        end
        % ----------------------------------------------------------------------
        %% provide constraint index vector of dof
        function idx = getDofSysIndices(self)
            idx = [];
            % loop over dof types
            for cdof = self.cdof
                % get nodal index positions
                dof = find( [self.node.dof] == cdof );
                % check
                if length(dof) ~= length(self.node)
                    warning('ConstraintEdge:getDofSysIndices() --> Node missing requested dof!');
                end
                % get global dof indices
                ndi = [ self.node.idx ];
                idx = [ idx ndi(dof) ];
            end
            % we have: idx = [ idx doftype1 for all nodes, idx dotype2 for all nodes ]
        end
        %% provide constraint values
        function vec = value(self)
            % IMPORTANT NOTICE:
            % All ConstraintEdge values are assumed to act in the **local**
            % edge coordinate system, not in the global one
            % This applies to both, Dirichlet and Neumann constraint types!
            %
            vec = zeros( 1, length(self.node)*length(self.cdof) );
            % loop over all edges = 2-node pairs
            for ii = 1:length(self.node)-1
                % get node pair
                n1 = self.node(ii  );
                n2 = self.node(ii+1);
                % transformation matrix (note order of dof !)
                dx1 = n2.ref(1) - n1.ref(1);
                dx2 = n2.ref(2) - n1.ref(2);
                L = sqrt( dx1^2 + dx2^2 );
                T = 1/L*[ dx1,   0,  dx2,   0; ...
                            0, dx1,    0, dx2; ...
                         -dx2,   0,  dx1,   0; ...
                            0,-dx2,    0, dx1 ];
                % convenvience: handle case of consecutive identical nodes
                if ( L < eps )
                    continue;
                end
                switch (length(self.cdof))
                  % case: scalar-valued constraint
                  case 1
                    % get parameters (in local coordinate system)
                    p = self.cval( [ii, ii+1] );
                    % local element vector
                    vec_global = L/6.*[ +2, +1; ...
                                        +1, +2] * [p]';
                    %
                    idx = [ii, ii+1];
                    vec( idx ) = vec( idx ) + vec_global';
                  % case: vector-valued constraint
                  case 2
                    % get parameters (in local coordinate system)
                    pT = self.cval( [ii, ii+1]                   );
                    pN = self.cval( [ii, ii+1]+length(self.node) );
                    % local element vector
                    vec_local = L/6.*[ +2, +1,  0,  0; ...
                                       +1, +2,  0,  0; ...
                                        0,  0, +2, +1; ...
                                        0,  0, +1, +2 ] * [pT, pN]';
                    % global vector
                    vec_global = T' * vec_local;
                    %
                    idx = [ii, ii+1, ii+length(self.node), ii+1+length(self.node)];
                    vec( idx ) = vec( idx ) + vec_global';
                  % handle
                  otherwise
                    disp('ConstraintEdge: value() -- Error handling cases.')
                end
            end
        end
    end

end
