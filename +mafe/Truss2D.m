classdef Truss2D < mafe.Li2Element2D
    % truss finite element in the plane (2D)

    properties
		%
    end

    methods
        %% constructor
		function obj = Truss2D(varargin)
            obj = obj@mafe.Li2Element2D(varargin{:});
        end
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp1 mafe.DofType.Disp2 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
			disp1 = find( [self.node.dof] == mafe.DofType.Disp1 );
			disp2 = find( [self.node.dof] == mafe.DofType.Disp2 );
			ndidx = [ self.node.idx ];
			idx = [ ndidx(disp1(1)), ndidx(disp2(1)), ndidx(disp1(2)), ndidx(disp2(2)), ];
        end
        %% calculate transformation matrix local->global
        function [L, T] = transform(self)
            dx1 = self.node(2).ref(1) - self.node(1).ref(1);
            dx2 = self.node(2).ref(2) - self.node(1).ref(2);
            L = sqrt( dx1^2 + dx2^2 );
            T = 1/L*[ dx1, dx2,    0,   0; ...
                     -dx2, dx1,    0,   0; ...
                         0,  0,  dx1, dx2; ...
                         0,  0, -dx2, dx1 ];
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            EA = self.sect.E * self.sect.A;
            % local element matrix
            ele_mat_local = EA/L*[ +1,  0, -1,  0; ...
                                    0,  0,  0,  0; ...
                                   -1,  0, +1,  0; ...
                                    0,  0,  0,  0 ];
            % global element matrix
            ele_mat = T' * ele_mat_local * T;
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            rhoA = self.sect.rho * self.sect.A;
            % local element matrix
            ele_mat_local = rhoA*L/6.*[ +2,  0, +1,  0; ...
                                         0,  0,  0,  0; ...
                                        +1,  0, +2,  0; ...
                                         0,  0,  0,  0 ];
            % global element matrix
            ele_mat = T' * ele_mat_local * T;
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % provide damping matrix based on Rayleigh assumption
            % get parameters
            d_alpha = self.sect.d_alpha;
            d_beta  = self.sect.d_beta;
            % D = alpha * M + beta * K
            ele_mat = d_alpha * self.mass() + d_beta * self.stiffness();
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            px1 = self.load.p(mafe.DofType.Disp1,:);
            px2 = self.load.p(mafe.DofType.Disp2,:);
            % local load values at the nodes
            pX([1,3]) = T(1,1)*px1 + T(1,2)*px2;
            pX([2,4]) = T(2,1)*px1 + T(2,2)*px2;
            % local element vector
            ele_vec_local = L/6.*[ +2,  0, +1,  0; ...
                                    0, +2,  0, +1; ...
                                   +1,  0, +2,  0; ...
                                    0, +1,  0, +2 ] * pX';
            % global element vector
            ele_vec = T' * ele_vec_local;
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            % coordinates and displacements
            x1 = [ n1.ref(1), n2.ref(1) ];
            x2 = [ n1.ref(2), n2.ref(2) ];
            u1 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp1) ), n2.lhs( find(n2.dof == mafe.DofType.Disp1) ) ];
            u2 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp2) ), n2.lhs( find(n2.dof == mafe.DofType.Disp2) ) ];
            %
			% defaults
			lcolor = [0.6 0.6 0.6];
			mcolor = [0.2 0.2 0.2];
			lwidth = 0.5;
			mwidth = 10.;
			ecolor = [0.4 0.4 0.4];
			marker = '.';
			%
            switch config
                case 'deformed'
                    XX1 = x1+u1*scale;
                    XX2 = x2+u2*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
					falpha = 0.6;
                    lwidth = 2.0;
                    mwidth = 20.;
                case 'intensity'
                    XX1 = x1+u1*scale;
                    XX2 = x2+u2*scale;
                    normalforce = 0.5 * ( -self.internal(1) + self.internal(2) );
                    if normalforce < 0.0
                        lcolor = 'b';%[15.3, 31.0, 54.5]/100;
                    elseif normalforce > 0.0
                        lcolor = 'r';%[82.7, 28.4, 16.9]/100;
                    else
                        lcolor = 'g';%[58.8, 65.5,  7.8]/100;
                    end
                    mcolor = [0.0 0.0 0.0 0.0];
					falpha = 0.6;
                    lwidth = 3.0;
                    mwidth = 0.1;
				otherwise
                    plot@mafe.Li2Element2D(self, config, scale);
                    return;
            end

			mafe.patchline(XX1, XX2, ...
                     'EdgeColor', lcolor, ...
                     'LineWidth', lwidth, ...
                     'EdgeAlpha', falpha);

            plot(XX1([1 end]), XX2([1 end]), ...
                     marker, ...
					 'color', mcolor, ...
					 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            %
            str = sprintf('%+4.3e ', [ -self.internal(1), ... % left N
                                        self.internal(3)  ]); % right N
        end
    end

end
