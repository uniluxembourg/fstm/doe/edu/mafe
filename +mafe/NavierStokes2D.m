classdef NavierStokes2D < mafe.Qu4Element2D
    % Navier Stokes fluid finite element in the x-y plane (2D) -- velo-pressure

    properties
        %
    end

    methods
        %% constructor
        function obj = NavierStokes2D(varargin)
            obj = obj@mafe.Qu4Element2D(varargin{:});
        end
        % ======================================================================
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Velo1 mafe.DofType.Velo2 mafe.DofType.Press ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
            dof   = [ self.node.dof ];
            ndidx = [ self.node.idx ];
            velo1 = ndidx( dof == mafe.DofType.Velo1 );
            velo2 = ndidx( dof == mafe.DofType.Velo2 );
            press = ndidx( dof == mafe.DofType.Press );
            %velo1 = find( [self.node.dof] == mafe.DofType.Velo1 );
            %velo2 = find( [self.node.dof] == mafe.DofType.Velo2 );
            %press = find( [self.node.dof] == mafe.DofType.Press );

            %idx = [ ndidx(velo1), ndidx(velo2), ndidx(press) ];
            idx = [ velo1, velo2, press ];
        end
        % ======================================================================
        %% calculation of element mass matrix
        function ele_mat = elemat_dot_dx(self, x)
            % mass only affects disp
            ele_mat = zeros(12,12);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    N  = self.N(z);   % ansatz
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % mass approach (in global coordinates)
                    A = [          N, zeros(1,4), zeros(1,4) ;
                          zeros(1,4),          N, zeros(1,4) ;
                          zeros(1,4), zeros(1,4), zeros(1,4)];
                    % inertial virtual work expressions
                    ele_mat = ele_mat + factor * A' * self.sect.rho * A;
                end
            end
        end
        %% calculation of element damping matrix
        function ele_mat = elemat_linear_dx(self)
            % element matrix
            ele_mat = zeros(12,12);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % viscosity matrix
            V = self.sect.mu * [1.0, 0.0, 0.0; 0.0, 1.0, 0.0; 0.0, 0.0, 0.5];

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    N  = self.N(z);   % ansatz
                    Nz = self.Nz(z);  % local derivative
                    Nx = inv(J) * Nz; % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % strain rate approach (in global coordinates)
                    B = [    Nx(1,:), zeros(1,4) ;
                          zeros(1,4),    Nx(2,:) ;
                             Nx(2,:),    Nx(1,:) ];
                    % divergence velo approach (in global coordinates)
                    D = [ Nx(1,:), Nx(2,:) ];
                    % internal virtual work expressions
                    ele_mat(1:8,1: 8) = ele_mat(1:8,1: 8) + factor * B' * V * B;
                    ele_mat(1:8,9:12) = ele_mat(1:8,9:12) - factor * D' * N;
                    ele_mat(9:12,1:8) = ele_mat(9:12,1:8) + factor * N' * D;
                end
            end
        end
        %% calculation of element damping matrix
        function ele_mat = elemat_nonlinear_dx(self)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            n3 = self.node(3);
            n4 = self.node(4);
            % state values from last iteration of the Netwon-Raphson scheme
            V1 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo1) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo1) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo1) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo1) ) ];
            V2 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo2) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo2) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo2) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo2) ) ];
            PP = [ n1.lhs( find(n1.dof == mafe.DofType.Press) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Press) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Press) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Press) ) ];
            % element matrix
            ele_mat = zeros(12,12);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % viscosity matrix
            V = self.sect.mu * [1.0, 0.0, 0.0; 0.0, 1.0, 0.0; 0.0, 0.0, 0.5];

            % stabilisation parameters
            J0 = self.J([0,0]);
            h = sqrt(det(J0)/3.1415);
            v = sqrt( mean(V1)^2 + mean(V2)^2 );
            tau_pspg = 1.e-3;%5.0 * 1./sqrt( (2*v^2/h)^2 + (12*self.sect.mu/self.sect.rho/h^2)^2 );

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    N  = self.N(z);   % ansatz
                    Nz = self.Nz(z);  % local derivative
                    Nx = inv(J) * Nz; % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % linearised advection
                    v1 = N * V1;
                    v2 = N * V2;
                    dv1dx1 = Nx(1,:) * V1;
                    dv1dx2 = Nx(2,:) * V1;
                    dv2dx1 = Nx(1,:) * V2;
                    dv2dx2 = Nx(2,:) * V2;
                    %
                    NN   = N' * N;
                    NNx1 = N' * Nx(1,:);
                    NNx2 = N' * Nx(2,:);
                    %
                    ele_mat(1:4,1:4) = ele_mat(1:4,1:4) + factor * (self.sect.rho * v1)     * NNx1;
                    ele_mat(1:4,1:4) = ele_mat(1:4,1:4) + factor * (self.sect.rho * v2)     * NNx2;
                    ele_mat(1:4,1:4) = ele_mat(1:4,1:4) + factor * (self.sect.rho * dv1dx1) * NN;
                    ele_mat(1:4,5:8) = ele_mat(1:4,5:8) + factor * (self.sect.rho * dv1dx2) * NN;
                    % %
                    ele_mat(5:8,5:8) = ele_mat(5:8,5:8) + factor * (self.sect.rho * v1)     * NNx1;
                    ele_mat(5:8,5:8) = ele_mat(5:8,5:8) + factor * (self.sect.rho * v2)     * NNx2;
                    ele_mat(5:8,1:4) = ele_mat(5:8,1:4) + factor * (self.sect.rho * dv2dx1) * NN;
                    ele_mat(5:8,5:8) = ele_mat(5:8,5:8) + factor * (self.sect.rho * dv2dx2) * NN;
                    % stabilisation PSPG
                    ele_mat(9:12,9:12) = ele_mat(9:12,9:12) + factor * Nx(1,:)' * tau_pspg * Nx(1,:);
                    ele_mat(9:12,9:12) = ele_mat(9:12,9:12) + factor * Nx(2,:)' * tau_pspg * Nx(2,:);
                    % stabilisation SUPG
                    ele_mat(1:4,1:4) = ele_mat(1:4,1:4) + factor * Nx(1,:)' * tau_pspg*self.sect.rho * Nx(1,:);
                    ele_mat(1:4,1:4) = ele_mat(1:4,1:4) + factor * Nx(2,:)' * tau_pspg*self.sect.rho * Nx(2,:);
                    ele_mat(5:8,5:8) = ele_mat(5:8,5:8) + factor * Nx(1,:)' * tau_pspg*self.sect.rho * Nx(1,:);
                    ele_mat(5:8,5:8) = ele_mat(5:8,5:8) + factor * Nx(2,:)' * tau_pspg*self.sect.rho * Nx(2,:);
                end
            end
        end
        %% calculation of element residuum vector
        function ele_vec = force(self)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            n3 = self.node(3);
            n4 = self.node(4);
            % state values from last iteration of the Netwon-Raphson scheme
            V1 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo1) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo1) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo1) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo1) ) ];
            V2 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo2) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo2) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo2) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo2) ) ];
            PP = [ n1.lhs( find(n1.dof == mafe.DofType.Press) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Press) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Press) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Press) ) ];
            % element matrix
            ele_vec = zeros(12, 1);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % viscosity matrix
            V = self.sect.mu * [1.0, 0.0, 0.0; 0.0, 1.0, 0.0; 0.0, 0.0, 0.5];

            % stabilisation parameters
            J0 = self.J([0,0]);
            h = sqrt(det(J0)/3.1415);
            v = sqrt( mean(V1)^2 + mean(V2)^2 );
            tau_pspg = 1.e-3;%5.0 * 1./sqrt( (2*v^2/h)^2 + (12*self.sect.mu/self.sect.rho/h^2)^2 );

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    N  = self.N(z);   % ansatz
                    Nz = self.Nz(z);  % local derivative
                    Nx = inv(J) * Nz; % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % velocity and directional derivatives
                    v1 = N * V1;
                    v2 = N * V2;
                    dv1dx1 = Nx(1,:) * V1;
                    dv1dx2 = Nx(2,:) * V1;
                    dv2dx1 = Nx(1,:) * V2;
                    dv2dx2 = Nx(2,:) * V2;
                    %
                    ele_vec(1:4) = ele_vec(1:4) + factor * N' * self.sect.rho * (v1*dv1dx1 + v2*dv1dx2);
                    ele_vec(5:8) = ele_vec(5:8) + factor * N' * self.sect.rho * (v1*dv2dx1 + v2*dv2dx2);
                    % stabilisation PSPG
                    ele_vec(9:12) = ele_vec(9:12) + factor * Nx(1,:)' * tau_pspg * Nx(1,:) * PP;
                    ele_vec(9:12) = ele_vec(9:12) + factor * Nx(2,:)' * tau_pspg * Nx(2,:) * PP;
                    % stabilisation SUPG
                    ele_vec(1:4) = ele_vec(1:4) + factor * Nx(1,:)' * tau_pspg*self.sect.rho * dv1dx1;
                    ele_vec(1:4) = ele_vec(1:4) + factor * Nx(2,:)' * tau_pspg*self.sect.rho * dv1dx2;
                    ele_vec(5:8) = ele_vec(5:8) + factor * Nx(1,:)' * tau_pspg*self.sect.rho * dv2dx1;
                    ele_vec(5:8) = ele_vec(5:8) + factor * Nx(2,:)' * tau_pspg*self.sect.rho * dv2dx2;
                end
            end
        end
        %% calculation of element force vector
        function ele_vec = force_(self)
            % get parameters
            p1 = 0.0;%mean( self.load.p(mafe.DofType.Velo1,:) ); % assume constant loads
            p2 = 0.0;%mean( self.load.p(mafe.DofType.Velo2,:) ); % per element
            % global element vector
            ele_vec = zeros(12,1);

            % Gauss integration parameters (1 point sufficent for constant load)
            [GC, GW] = mafe.GaussCoordWeights(1);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    N  = self.N(z);   % same ansatz for geometry and displacements
                    J  = self.J(z);   % Jacobi matrix
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % external virtual work expressions
                    ele_vec = ele_vec + factor * self.sect.rho * [ N'*p1; N'*p2; zeros(4,1) ];
                end
            end
        end
        %------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, x, dot_x, ddot_x)
            % if ~exist('ddu', 'var')
            %     ddu = zeros(length(u),1);
            % end
            % if ~exist('du', 'var')
            %     du = zeros(length(u),1);
            % end
            % % obtain sub matrices
            % [ele_mat_su, ele_mat_ss] = self.ele_sub_matrices();
            % % compute local stress dof
            % s = ele_mat_ss \ (-ele_mat_su * u);
            % % transformation
            % J0 = self.J([0,0]);
            % T0 = self.T0(J0);
            % % evaluate stress ansatz at corner nodes and store tensor components
            % self.internal = T0 * [ self.S( [-1., -1.] ) * s, ...
            %                        self.S( [+1., -1.] ) * s, ...
            %                        self.S( [+1., +1.] ) * s, ...
            %                        self.S( [-1., +1.] ) * s ] ;

            self.internal = [0.0, 0.0, 0.0];
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            n3 = self.node(3);
            n4 = self.node(4);
            % coordinates and velocities
            x1 = [ n1.ref(1), n2.ref(1), n3.ref(1), n4.ref(1) ];
            x2 = [ n1.ref(2), n2.ref(2), n3.ref(2), n4.ref(2) ];
            v1 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo1) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo1) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo1) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo1) ) ]';
            v2 = [ n1.lhs( find(n1.dof == mafe.DofType.Velo2) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Velo2) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Velo2) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Velo2) ) ]';
            pp = [ n1.lhs( find(n1.dof == mafe.DofType.Press) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Press) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Press) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Press) ) ]';
            %
            % defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                case 'velocity'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
                    falpha = 0.2;
                    lwidth = 1.0;
                    mwidth = 20.;
                case 'stress11'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(1,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'stress22'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(2,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'stress12'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(3,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'pressure'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = pp;
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'velo1'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = v1;
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'velo2'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = v2;
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'vorticity'
                    % compute vorticity
                    z = [-1.0, -1.0]; J  = self.J(z); Nz = self.Nz(z); Nx = inv(J) * Nz;
                    w1 = Nx(1,:)*v2' - Nx(2,:)*v1';
                    z = [+1.0, -1.0]; J  = self.J(z); Nz = self.Nz(z); Nx = inv(J) * Nz;
                    w2 = Nx(1,:)*v2' - Nx(2,:)*v1';
                    z = [+1.0, +1.0]; J  = self.J(z); Nz = self.Nz(z); Nx = inv(J) * Nz;
                    w3 = Nx(1,:)*v2' - Nx(2,:)*v1';
                    z = [-1.0, +1.0]; J  = self.J(z); Nz = self.Nz(z); Nx = inv(J) * Nz;
                    w4 = Nx(1,:)*v2' - Nx(2,:)*v1';
                    %
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [w1 w2 w3 w4];
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.2 0.2 0.2];
                    falpha = 1.0;
                    lwidth = 0.5;
                    mwidth = 12.;
            end
            patch(XX1, XX2, -0.005*[1 1 1 1], lcolor, ...
                    'EdgeColor', ecolor, ...
                    'LineWidth',  lwidth, ...
                    'Marker', marker, ...
                    'MarkerSize', mwidth, ...
                    'FaceAlpha', falpha);

            % put a vector representation if required
            if strcmp(config, 'velocity')
                plot([x1(1) x1(1)+scale*v1(1)],[x2(1) x2(1)+scale*v2(1)], 'b', 'linewidth', 2.0 );
                plot([x1(2) x1(2)+scale*v1(2)],[x2(2) x2(2)+scale*v2(2)], 'b', 'linewidth', 2.0 );
                plot([x1(3) x1(3)+scale*v1(3)],[x2(3) x2(3)+scale*v2(3)], 'b', 'linewidth', 2.0 );
                plot([x1(4) x1(4)+scale*v1(4)],[x2(4) x2(4)+scale*v2(4)], 'b', 'linewidth', 2.0 );
            end
        end
        %% print element data
        function str = print(self)
            %
            str_s11 = sprintf('%+4.3e ', self.internal(1,:)); % nodal s11 stresses
            str_s22 = sprintf('%+4.3e ', self.internal(2,:)); % nodal s22 stresses
            str_s12 = sprintf('%+4.3e ', self.internal(3,:)); % nodal s12 stresses
            str = [str_s11 '| ' str_s22 '| ' str_s12 ];
        end
    end

end
