classdef ConstraintNode < mafe.Constraint
    % Constraint on degree of freedom at single or multiple node(s)

    properties
        node ; % the involved node(s)
        cval ; % the given contraint value(s), can be scalar or vectorial
    end

    methods
        %% constructor
        function obj = ConstraintNode(node, cdof, type, cval, tfun)
            if nargin >= 4
                obj.node = node;
                obj.cdof = cdof;
                obj.type = type;
                obj.cval = cval;
            end
            if nargin >= 5
                obj.tfun = tfun;
            end
            % consistency checks and convenience
            if length(cval) == 1 && length(type) == 1 % single scalar given
                obj.cval = cval * ones(1,length(node));
            end
            if length(cval) == 2 && length(obj.cdof) == 2 % single vector given
                obj.cval = [ cval(1) * ones(1,length(obj.node)), cval(2) * ones(1,length(obj.node)) ]
            end
            if length(obj.cval) ~= length(obj.node)*length(obj.cdof)
                warning('ConstraintNode: Error with given number of values != num nodes * num doftypes!')
            end
        end
        % ----------------------------------------------------------------------
        %% provide constraint index vector of dof
        function idx = getDofSysIndices(self)
            idx = [];
            % loop over dof types
            for cdof = self.cdof
                % get nodal index positions
                dof = find( [self.node.dof] == cdof );
                % check
                if length(dof) ~= length(self.node)
                    warning('ConstraintNode:getDofSysIndices() --> Node missing requested dof!');
                end
                % get global dof indices
                ndi = [ self.node.idx ];
                idx = [ idx ndi(dof) ];
            end
            % we have: idx = [ idx doftype1 for all nodes, idx doftype2 for all nodes ]
        end
        %% provide constraint values
        function vec = value(self)
            vec = [ self.cval ];
        end
    end

end
