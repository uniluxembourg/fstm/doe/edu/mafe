classdef Constraint < matlab.mixin.Heterogeneous & handle
    % Constraint on degree of freedom at node(s)

    properties
        cdof; % the dof type to constrain
        type; % the constraint type
        tfun = mafe.TimeFunction.Static; % default time function of constraint value 
    end

    methods (Abstract)
        getDofSysIndices(obj)
        value(obj)
    end

end
