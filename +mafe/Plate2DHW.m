classdef Plate2DHW < mafe.Plate2DHR
    % Plate (Reissner theory) finite element in the x-y plane (2D) -- Hu-Washizu

    properties

    end

    methods
        %% constructor
        function obj = Plate2DHW(varargin)
            obj = obj@mafe.Plate2DHR(varargin{:});
        end
        % ======================================================================
        %% bending moment ansatz for contra-variant components m^ij
        function M = M(~, z)
            M  = [ 1, 0, 0, z(2),    0;   % m11 = linear in z2
                   0, 1, 0,    0, z(1);   % m22 = linear in z1
                   0, 0, 1,    0,    0 ]; % m12 = constant
        end
        %% shear force ansatz for contra-variant components q^i3
        function Q = Q(~, z)
            Q  = [ 1, 0, z(2),    0;   % q13 = linear in z2
                   0, 1,    0, z(1) ]; % q23 = linear in z1
        end
        %% shear strains ansatz for co-variant components gamma_i3
        function E = E(~, z)
            E  = [ 1, 0, z(2),    0;   % e13 = linear in z2
                   0, 1,    0, z(1) ]; % e23 = linear in z1
        end
        % ======================================================================
        %% calculation of element sub-stiffness matrices of the mixed principle
        function [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_eq, ele_mat_ee] = ele_sub_matrices(self)
            % sub-matrices of the mixed-hybrid form used
            % (Hellinger-Reissner in bending + Hu-Washizu in shear)
            deformndof = 3*4;
            momentndof = size(self.M([0,0]), 2);
            shearfndof = size(self.Q([0,0]), 2);
            shearsndof = size(self.E([0,0]), 2);
            ele_mat_mu = zeros(momentndof,deformndof);
            ele_mat_qu = zeros(shearfndof,deformndof);
            ele_mat_mm = zeros(momentndof,momentndof);
            ele_mat_eq = zeros(shearsndof,shearfndof);
            ele_mat_ee = zeros(shearsndof,shearsndof);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % flexibility matrix for bending
            D = self.sect.flexMatPlateBending();
            % elasticity matrix for shear
            C = self.sect.elastMatPlateShear();

            % transformation
            J0 = self.J([0,0]);
            T  = J0;
            iT = inv(T);
            T0 = self.T0(T);

            % helper variables for the shear strain approach at tying points (Bathe/Dvorkin)
            zA = [-1, 0]; NA = self.N(zA); NzA = self.Nz(zA); JA = self.J(zA);
            zB = [ 0,-1]; NB = self.N(zB); NzB = self.Nz(zB); JB = self.J(zB);
            zC = [+1, 0]; NC = self.N(zC); NzC = self.Nz(zC); JC = self.J(zC);
            zD = [ 0,+1]; ND = self.N(zD); NzD = self.Nz(zD); JD = self.J(zD);
            % shear strain approach at tying points B-D and A-C
            g_z1_B = [ NzB(1,:), JB(1,1)*NB, JB(1,2)*NB ];
            g_z1_D = [ NzD(1,:), JD(1,1)*ND, JD(1,2)*ND ];
            g_z2_A = [ NzA(2,:), JA(2,1)*NA, JA(2,2)*NA ];
            g_z2_C = [ NzC(2,:), JC(2,1)*NC, JC(2,2)*NC ];

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    N  = self.N(z);   % same ansatz for geometry and displacements
                    Nz = self.Nz(z);  % local derivative
                    J  = self.J(z);   % Jacobi matrix
                    iJ = inv(J);      % inverse Jacobi matrix
                    Nx = iJ * Nz;     % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ;
                    % bending strain approach (in global coordinates)
                    B = [ zeros(1,4),    Nx(1,:), zeros(1,4) ;
                          zeros(1,4), zeros(1,4),    Nx(2,:) ;
                          zeros(1,4),    Nx(2,:),    Nx(1,:) ];
                    % % shear strain approach (in global coordinates) - this locks.
                    % S = [ Nx(:,1),          N, zeros(1,4) ;
                    %       Nx(:,2), zeros(1,4),         N ];
                    % shear strain approach (in global coordinates)
                    S = iJ * 0.5 * [ ( 1.0 - z(2) ) * g_z1_B + ( 1.0 + z(2) ) * g_z1_D ;
                                     ( 1.0 - z(1) ) * g_z2_A + ( 1.0 + z(1) ) * g_z2_C ]; % strictly use iJ, not iJ0
                    % moment approach (in global coordinates)
                    M = T0 * self.M(z);
                    % shear force approach (in global coordinates)
                    Q = T' * self.Q(z);
                    % shear strain approach (in global coordinates)
                    E = iT * self.E(z);
                    % internal virtual work expressions
                    ele_mat_mu = ele_mat_mu + factor * M' * B;
                    ele_mat_qu = ele_mat_qu + factor * Q' * S;
                    ele_mat_mm = ele_mat_mm - factor * M' * D * M;
                    ele_mat_eq = ele_mat_eq - factor * E' * Q;
                    ele_mat_ee = ele_mat_ee + factor * E' * C * E;
                end
            end
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % obtain sub matrices
            [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_eq, ele_mat_ee] = self.ele_sub_matrices();
            % static condensation of element-local shear strains
            ele_mat_qq = -ele_mat_eq' * inv(ele_mat_ee) * ele_mat_eq;
            % static condensation of element-local shear forces
            ele_mat_c1 = -ele_mat_qu' * inv(ele_mat_qq) * ele_mat_qu;
            % static condensation of element-local moments
            ele_mat_c2 = -ele_mat_mu' * inv(ele_mat_mm) * ele_mat_mu;
            % collect element matrix
            ele_mat = ele_mat_c1 + ele_mat_c2;
            %[ ,L] = eig(ele_mat, 'vector') % get eigenvalues of element matrix
        end
        %-----------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, u, du, ddu)
            if ~exist('ddu', 'var')
                ddu = zeros(length(u),1);
            end
            if ~exist('du', 'var')
                du = zeros(length(u),1);
            end
            % obtain sub matrices
            [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_eq, ele_mat_ee] = self.ele_sub_matrices();
            % compute local moment dof
            m =  ele_mat_mm \ (-ele_mat_mu * u);
            ele_mat_qq =  ele_mat_eq' * inv(ele_mat_ee) * ele_mat_eq; % sign?
            q =  ele_mat_qq \ (-ele_mat_qu * u);
            e =  ele_mat_ee \ (-ele_mat_eq * q);
            % transformation
            T  = self.J([0,0])';
            iT = inv(T);
            T0 = self.T0(T);
            % evaluate stress ansatz at corner nodes and store tensor components
            self.internal = [ T0 * [ self.M( [-1., -1.] ) * m, ...
                                     self.M( [+1., -1.] ) * m, ...
                                     self.M( [+1., +1.] ) * m, ...
                                     self.M( [-1., +1.] ) * m ] ;
                              T' * [ self.Q( [-1., -1.] ) * q, ...
                                     self.Q( [+1., -1.] ) * q, ...
                                     self.Q( [+1., +1.] ) * q, ...
                                     self.Q( [-1., +1.] ) * q ] ;
                              iT * [ self.E( [-1., -1.] ) * e, ...
                                     self.E( [+1., -1.] ) * e, ...
                                     self.E( [+1., +1.] ) * e, ...
                                     self.E( [-1., +1.] ) * e ] ;
                            ];
        end
        %-----------------------------------------------------------------------
        %% print element data
        function str = print(self)
            %
            str_m11 = sprintf('%+4.3e ', self.internal(1,:)); % nodal s11 moments
            str_m22 = sprintf('%+4.3e ', self.internal(2,:)); % nodal s22 moments
            str_m12 = sprintf('%+4.3e ', self.internal(3,:)); % nodal s12 moments
            %
            str_q13 = sprintf('%+4.3e ', self.internal(4,:)); % nodal q13 shear forces
            str_q23 = sprintf('%+4.3e ', self.internal(5,:)); % nodal q23 shear forces
            %
            str_e13 = sprintf('%+4.3e ', self.internal(6,:)); % nodal e13 shear strains
            str_e23 = sprintf('%+4.3e ', self.internal(7,:)); % nodal e23 ahear strains

            str_1 = [ '\n   moment: ' str_m11 '| ' str_m22 '| ' str_m12 ];
            str_2 = [ '\n   shearf: ' str_q13 '| ' str_q23 ];
            str_3 = [ '\n   shears: ' str_e13 '| ' str_e23 ];

            str = [ str_1 str_2 str_3 ];
        end
    end

end
