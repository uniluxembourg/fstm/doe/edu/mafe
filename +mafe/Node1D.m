classdef Node1D < mafe.Node
    % Node along x1 axis of (axial) finite element mesh

    properties
        ref = zeros(1,1); % vector of global coordinate
    end

    methods
        %% constructor
        function obj = Node1D(ref_coords)
          if exist('ref_coords','var')
            obj.ref = ref_coords;
          end
        end
        %% print nodal data
        function str = print(self)
          str = sprintf('[% 7.3f]', self.ref(1));
          str = strcat( str, print@mafe.Node(self) );
        end
    end

end
