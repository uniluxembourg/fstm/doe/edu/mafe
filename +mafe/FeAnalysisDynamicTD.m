classdef FeAnalysisDynamicTD < mafe.FeAnalysis
    % dynamic analysis of finite element problem in time domain

    properties
        L = []; % lower part of decomposition of the effective system matrix
        U = []; % upper part of decomposition of the effective system matrix

        M = [];
        D = [];
        K = [];

        Mf = [];
        Df = [];
        Kf = [];

        u  = []; % vector of displacement at t_{n+1}
        v  = []; % vector of velocity     at t_{n+1}
        a  = []; % vector of acceleration at t_{n+1}

        tn = 0.; % time instant at t_{n}
        dt = 1.; % time step size

        scheme = mafe.TimeIntegrationType.CrankNicolson; % default scheme
        params; % parameter set of the time integration scheme

        tfuns = [ mafe.TimeFunction.Static ]; % vector of time functions used
    end

    methods
        %% constructor
        function obj = FeAnalysisDynamicTD(fep, tfuns)
            if nargin >= 1
                obj.fep = fep;
            end
            if nargin >= 2
                obj.tfuns = [ mafe.TimeFunction.Static, tfuns ];
            end
        end
        %% initialise the calculation of the dynamic response
        function [] = initialise(self, dt, scheme, params)
            if nargin >= 2
                self.dt = dt;
            end
            if nargin >= 3
                self.scheme = scheme;
            end
            if nargin >= 4
                self.params = params;
            end
            % ------------------------------------------------------------------
            % scheme identification and settings
            switch (self.scheme)
               case mafe.TimeIntegrationType.CrankNicolson
                  %
                  beta  = 0.25;
                  gamma = 0.50;
                  %
                  self.params.theta_ = [ 1.0,      1.0,       1.0 ];
                  self.params.gamma_ = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  self.params.beta_  = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  %
               case mafe.TimeIntegrationType.CentralDifference
                  %
                  beta  = 0.00;
                  gamma = 0.50;
                  %
                  self.params.theta_ = [ 1.0,      1.0,       1.0 ];
                  self.params.gamma_ = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  self.params.beta_  = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  %
               case mafe.TimeIntegrationType.LinearAcceleration
                  %
                  beta  = 1./6;
                  gamma = 0.50;
                  %
                  self.params.theta_ = [ 1.0,      1.0,       1.0 ];
                  self.params.gamma_ = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  self.params.beta_  = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  %
               case mafe.TimeIntegrationType.FoxGoodwin
                  %
                  beta  = 1/12;
                  gamma = 0.50;
                  %
                  self.params.theta_ = [ 1.0,      1.0,       1.0 ];
                  self.params.gamma_ = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  self.params.beta_  = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  %
               case mafe.TimeIntegrationType.GeneralisedNewmark
                  %
                  beta  = self.params.beta;
                  gamma = self.params.gamma;
                  %
                  self.params.theta_ = [ 1.0,      1.0,       1.0 ];
                  self.params.gamma_ = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  self.params.beta_  = [ 6.0*beta, 2.0*gamma, 1.0 ];
                  %
               case mafe.TimeIntegrationType.GeneralisedAlpha
                  %
                  beta  = self.params.beta;
                  gamma = self.params.gamma;
                  alpha_f = self.params.alpha_f;
                  alpha_m = self.params.alpha_m;
                  % ensure stability and 2nd order accuracy for given alpha
                  beta  = 0.25 + 0.50 * ( alpha_f - alpha_m );
                  gamma = 0.50 + 1.00 * ( alpha_f - alpha_m );
                  %
                  self.params.theta_ = [ 1.0-alpha_f, 1.0-alpha_f, 1.0-alpha_f                 ];
                  self.params.gamma_ = [ 6.0*beta,    2.0*gamma,   (1.0-alpha_m)/(1.0-alpha_f) ];
                  self.params.beta_  = [ 6.0*beta,    2.0*gamma,   1.0                         ];
                  %
               case mafe.TimeIntegrationType.Bathe
                  %
                  % TODO --> check if this scheme can be fit into the above!?
                  %
               otherwise
                  disp('FeAnalysisDynamicTD::initialise() : Unknown scheme!');
                  return;
            end
            % ------------------------------------------------------------------
            % make sure the problem is initialised
            self.fep.init();
            % get system mass and stiffness matrix and force vector
            M = self.fep.assembleSystemMassMatrix();
            D = self.fep.assembleSystemDampingMatrix();
            K = self.fep.assembleSystemStiffnessMatrix();
            f = self.fep.assembleSystemForceVector();
            % store system matrices
            self.Mf = M;
            self.Df = D;
            self.Kf = K;
            % apply constraints
            [~, ~, ~, M] = self.fep.applyConstraintsDirichlet(M, zeros(self.fep.ndofs,1));
            [~, ~, ~, D] = self.fep.applyConstraintsDirichlet(D, zeros(self.fep.ndofs,1));
            [~, ~, ~, K] = self.fep.applyConstraintsDirichlet(K, zeros(self.fep.ndofs,1));
            % ------------------------------------------------------------------
            A =     ( self.params.gamma_(3)*self.params.theta_(1)                ) * M;
            A = A + ( self.params.gamma_(2)*self.params.theta_(2)*(self.dt)^1/2. ) * D;
            A = A + ( self.params.gamma_(1)*self.params.theta_(3)*(self.dt)^2/6. ) * K;
            %
            [self.L, self.U] = lu(A);
            % ------------------------------------------------------------------
            % set initial conditions to default zero
            self.u = zeros(self.fep.ndofs,1);
            self.v = zeros(self.fep.ndofs,1);
            self.a = zeros(self.fep.ndofs,1);
            % ------------------------------------------------------------------
            % store system matrices
            self.M = M;
            self.D = D;
            self.K = K;
        end
        %% perform one time integration step
        function solveTimeStep(self)
            % compute estimates
            da_tilde = self.a * 0.0;
            u_tilde =                                                      self.u;
            u_tilde = u_tilde + ( self.params.theta_(1)*(self.dt)      ) * self.v;
            u_tilde = u_tilde + ( self.params.theta_(2)*(self.dt)^2/2. ) * self.a;
            u_tilde = u_tilde + ( self.params.theta_(3)*(self.dt)^3/6.*self.params.gamma_(1) ) * da_tilde / self.dt;
            v_tilde =                                                      self.v;
            v_tilde = v_tilde + ( self.params.theta_(1)*(self.dt)      ) * self.a;
            v_tilde = v_tilde + ( self.params.theta_(2)*(self.dt)^2/2.*self.params.gamma_(2) ) * da_tilde / self.dt;
            a_tilde =                                                      self.a;
            a_tilde = a_tilde + ( self.params.theta_(1)*(self.dt)^2/1.*self.params.gamma_(3) ) * da_tilde / self.dt;
            % compute forcing vector at current time instant (using time fcts)
            fp = zeros(self.fep.ndofs,1);
            fa = zeros(self.fep.ndofs,1);
            fv = zeros(self.fep.ndofs,1);
            fu = zeros(self.fep.ndofs,1);
            % current and next time instant
            time = self.tn;
            tnext = time + self.dt;
            % identify indices of prescribed Dirchlet constraints
            [~,dbc] = self.fep.applyConstraintsDirichlet(self.Kf, fu);
            % load contributions from stationary and non-stationary loads and constraints
            for tf = self.tfuns
               % compute load vector associated with time function tf
               fp_ = self.fep.assembleSystemForceVector(tf);
               % apply Neumann constraints associated with time function tf
               fp_ = self.fep.applyConstraintsNeumann(fp_, tf);
               % apply Dirichlet constraints associated with time function tf
               fa_ = self.fep.applyConstraintsDirichlet(self.Mf, zeros(self.fep.ndofs,1), tf);
               fv_ = self.fep.applyConstraintsDirichlet(self.Df, zeros(self.fep.ndofs,1), tf);
               fu_ = self.fep.applyConstraintsDirichlet(self.Kf, zeros(self.fep.ndofs,1), tf);
               % multiply load vector with evaluated time function
               fa = fa + fa_ * ( (1.0-self.params.theta_(1))*( tf.eval_cos_ddotf(time) + tf.eval_sin_ddotf(time) ) + self.params.theta_(1)*( tf.eval_cos_ddotf(tnext) + tf.eval_sin_ddotf(tnext) ) );
               fv = fv + fv_ * ( (1.0-self.params.theta_(1))*( tf.eval_cos_dotf(time)  + tf.eval_sin_dotf(time)  ) + self.params.theta_(1)*( tf.eval_cos_dotf(tnext)  + tf.eval_sin_dotf(tnext)  ) );
               fu = fu + fu_ * ( (1.0-self.params.theta_(1))*( tf.eval_cos_f(time)     + tf.eval_sin_f(time)     ) + self.params.theta_(1)*( tf.eval_cos_f(tnext)     + tf.eval_sin_f(tnext)     ) );
               fp = fp + fp_ * ( (1.0-self.params.theta_(1))*( tf.eval_cos_f(time)     + tf.eval_sin_f(time)     ) + self.params.theta_(1)*( tf.eval_cos_f(tnext)     + tf.eval_sin_f(tnext)     ) );
            end
            % compose load vector
            p = fp + fa + fv + fu;
            % compute resudial force vector
            r = p - ( self.M * a_tilde + self.D * v_tilde + self.K * u_tilde );
            % forward/backward solve re-using factorisation from initialisation
            y = self.L\r;
            x = self.U\y;
            % update state vectors
            da = da_tilde + x;
            self.u = self.u + ( (self.dt)      ) * self.v;
            self.u = self.u + ( (self.dt)^2/2. ) * self.a;
            self.u = self.u + ( self.params.beta_(1)*(self.dt)^3/6. ) * da / self.dt;
            self.v = self.v + ( (self.dt)      ) * self.a;
            self.v = self.v + ( self.params.beta_(2)*(self.dt)^2/2. ) * da / self.dt;
            self.a = self.a + ( self.params.beta_(3)*(self.dt)^1/1. ) * da / self.dt;
            % include given dirichlet values
            self.u(dbc) = fu(dbc);
            self.v(dbc) = fv(dbc);
            self.a(dbc) = fa(dbc);
            % update time
            self.tn = self.tn + self.dt;
            % --- POST-PROCESSING ---
            % compute reaction forces (including dynamic effects)
            r = self.Mf * self.a + self.Df * self.v + self.Kf * self.u - fp;
            % feedback of solution values (u and r) to nodal dofs
            self.fep.updateNodalDofs(self.u, r);
            % postprocess elements
            self.fep.postprocessElements(self.u, self.v, self.a);
        end
        %% apply initial conditions
        function applyInitialConditions(self, u0, v0, t0)
            self.u = u0;
            %
            if ~exist('v0', 'var')
                self.v = zeros(self.fep.ndofs,1);
            end
            if ~exist('t0', 'var')
                self.tn = 0.0;
            end
        end
    end

end
