classdef Plate2DHR < mafe.Qu4Element2D
    % Plate (Reissner theory) finite element in the x-y plane (2D) -- Hellinger-Reissner

    properties

    end

    methods
        %% constructor
        function obj = Plate2DHR(varargin)
            obj = obj@mafe.Qu4Element2D(varargin{:});
        end
        % ======================================================================
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp3 mafe.DofType.Rota1 mafe.DofType.Rota2 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
            disp3 = find( [self.node.dof] == mafe.DofType.Disp3 );
            rota1 = find( [self.node.dof] == mafe.DofType.Rota1 );
            rota2 = find( [self.node.dof] == mafe.DofType.Rota2 );
            ndidx = [ self.node.idx ];
            idx = [ ndidx(disp3), ndidx(rota1), ndidx(rota2) ];
        end
        % ======================================================================
        %% bending moment ansatz for contra-variant components m^ij
        function M = M(~, z)
            M  = [ 1, 0, 0, z(2),    0 ;  % m11 = linear in z2
                   0, 1, 0,    0, z(1) ;  % m22 = linear in z1
                   0, 0, 1,    0,    0 ]; % m12 = constant
        end
        %% shear force ansatz for contra-variant components q^i3
        function Q = Q(~, z)
            Q  = [ 1, 0, z(2),    0 ;  % q13 = linear in z2
                   0, 1,    0, z(1) ]; % q23 = linear in z1
        end
        % ======================================================================
        %% calculation of element sub-stiffness matrices of the mixed principle
        function [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_qq] = ele_sub_matrices(self)
            % sub-matrices of the mixed-hybrid form used
            % (Hellinger-Reissner in bending + shear + Bathe/Dvorkin in shear strain)
            deformndof = 3*4;
            momentndof = size(self.M([0,0]), 2);
            shearfndof = size(self.Q([0,0]), 2);
            ele_mat_mu = zeros(momentndof,deformndof);
            ele_mat_qu = zeros(shearfndof,deformndof);
            ele_mat_mm = zeros(momentndof,momentndof);
            ele_mat_qq = zeros(shearfndof,shearfndof);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % transformation
            J0 = self.J([0,0]);
            T  = J0;
            T0 = self.T0(T);

            % flexibility matrix for bending
            D = self.sect.flexMatPlateBending();
            % flexibility matrix for shear
            V = self.sect.flexMatPlateShear();

            % helper variables for the shear strain approach at tying points (Bathe/Dvorkin)
            zA = [-1, 0]; NA = self.N(zA); NzA = self.Nz(zA); JA = self.J(zA);
            zB = [ 0,-1]; NB = self.N(zB); NzB = self.Nz(zB); JB = self.J(zB);
            zC = [+1, 0]; NC = self.N(zC); NzC = self.Nz(zC); JC = self.J(zC);
            zD = [ 0,+1]; ND = self.N(zD); NzD = self.Nz(zD); JD = self.J(zD);
            % shear strain approach at tying points B-D and A-C
            g_z1_B = [ NzB(1,:), JB(1,1)*NB, JB(1,2)*NB ];
            g_z1_D = [ NzD(1,:), JD(1,1)*ND, JD(1,2)*ND ];
            g_z2_A = [ NzA(2,:), JA(2,1)*NA, JA(2,2)*NA ];
            g_z2_C = [ NzC(2,:), JC(2,1)*NC, JC(2,2)*NC ];

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    Nz = self.Nz(z);  % local derivative
                    J  = self.J(z);   % Jacobi matrix
                    iJ = inv(J);      % inverse Jacobi matrix
                    Nx = iJ * Nz;     % global derivative
                    % integration factor
                    factor = w * det(J);
                    % bending strain approach (in global coordinates)
                    B = [ zeros(1,4),    Nx(1,:), zeros(1,4) ;
                          zeros(1,4), zeros(1,4),    Nx(2,:) ;
                          zeros(1,4),    Nx(2,:),    Nx(1,:) ];
                    % shear strain approach (in global coordinates)
                    S = iJ * 0.5 * [ ( 1.0 - z(2) ) * g_z1_B + ( 1.0 + z(2) ) * g_z1_D ;
                                     ( 1.0 - z(1) ) * g_z2_A + ( 1.0 + z(1) ) * g_z2_C ]; % strictly use iJ, not iJ0
                    % moment approach (in global coordinates)
                    M = T0 * self.M(z);
                    % shear force approach (in global coordinates)
                    Q = T' * self.Q(z);
                    % internal virtual work expressions
                    ele_mat_mu = ele_mat_mu + factor * M' * B;
                    ele_mat_qu = ele_mat_qu + factor * Q' * S;
                    ele_mat_mm = ele_mat_mm - factor * M' * D * M;
                    ele_mat_qq = ele_mat_qq - factor * Q' * V * Q;
                end
            end
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % obtain sub matrices
            [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_qq] = self.ele_sub_matrices();
            % static condensation of element-local shear forces
            ele_mat_c1 = -ele_mat_qu' * inv(ele_mat_qq) * ele_mat_qu;
            % static condensation of element-local moments
            ele_mat_c2 = -ele_mat_mu' * inv(ele_mat_mm) * ele_mat_mu;
            % collect element matrix
            ele_mat = ele_mat_c1 + ele_mat_c2;
            %[ ,L] = eig(ele_mat, 'vector') % get eigenvalues of element matrix
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % TODO
            ele_mat = zeros(12,12);
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % TODO
            ele_mat = zeros(12,12);
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % get parameters
            p3 = mean( self.load.p(mafe.DofType.Disp3,:) ); % assume constant loads
            % global element vector
            ele_vec = zeros(12,1);

            % Gauss integration parameters (1 point sufficent for constant load)
            [GC, GW] = mafe.GaussCoordWeights(1);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    N  = self.N(z); % same ansatz for geometry and displacements
                    J  = self.J(z); % Jacobi matrix
                    % integration factor
                    factor = w * det(J);
                    % external virtual work expressions
                    ele_vec = ele_vec + factor * [ N'*p3; zeros(4,1); zeros(4,1) ];
                end
            end
        end
        %------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, u, du, ddu)
            if ~exist('ddu', 'var')
                ddu = zeros(length(u),1);
            end
            if ~exist('du', 'var')
                du = zeros(length(u),1);
            end
            % obtain sub matrices
            [ele_mat_mu, ele_mat_qu, ele_mat_mm, ele_mat_qq] = self.ele_sub_matrices();
            % compute local moment dof
            m =  ele_mat_mm \ (-ele_mat_mu * u);
            q =  ele_mat_qq \ (-ele_mat_qu * u);
            % transformation
            T  = self.J([0,0]);
            T0 = self.T0(T);
            % evaluate stress ansatz at corner nodes and store tensor components
            self.internal = [ T0 * [ self.M( [-1., -1.] ) * m, ...
                                     self.M( [+1., -1.] ) * m, ...
                                     self.M( [+1., +1.] ) * m, ...
                                     self.M( [-1., +1.] ) * m ] ;
                              T' * [ self.Q( [-1., -1.] ) * q, ...
                                     self.Q( [+1., -1.] ) * q, ...
                                     self.Q( [+1., +1.] ) * q, ...
                                     self.Q( [-1., +1.] ) * q ] ;
                            ];
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            n3 = self.node(3);
            n4 = self.node(4);
            % coordinates and displacements
            x1 = [ n1.ref(1), n2.ref(1), n3.ref(1), n4.ref(1) ];
            x2 = [ n1.ref(2), n2.ref(2), n3.ref(2), n4.ref(2) ];
            ww = [ n1.lhs( find(n1.dof == mafe.DofType.Disp3) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Disp3) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Disp3) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Disp3) ) ]';
            %
            % defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                case 'deformed'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
                    falpha = 0.2;
                    lwidth = 1.0;
                    mwidth = 20.;
                case 'moment11'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = self.internal(1,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'moment22'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = self.internal(2,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'moment12'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = self.internal(3,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'vonmises'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    % calculate nodal von mises stress at upper surface (z=+t/2) from moment tensor
                    factor =  6. / self.sect.t^2;
                    s11 = self.internal(1,:) * factor;
                    s22 = self.internal(2,:) * factor;
                    s12 = self.internal(3,:) * factor;
                    sigma_v = sqrt( s11.^2 + s22.^2 - s11.*s22 + 3*s12.^2 );
                    lcolor = sigma_v;
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'tensor'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = [1.0 1.0 1.0];
                    mcolor = [0.1 0.1 0.1];
                    falpha = 1.0;
                    lwidth = 0.01;
                    mwidth = 0.01;
                case 'shear13'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = self.internal(4,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'shear23'
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = self.internal(5,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    XX3 = ww*0.0;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.2 0.2 0.2];
                    falpha = 1.0;
                    lwidth = 0.5;
                    mwidth = 12.;
            end

            patch(XX1, XX2, XX3, lcolor, ...
                    'EdgeColor', ecolor, ...
                    'LineWidth',  lwidth, ...
                    'Marker', marker, ...
                    'MarkerSize', mwidth, ...
                    'FaceAlpha', falpha);

            % put a tensor representation if required
            if strcmp(config, 'tensor')
                % compute tensor representation
                S = [ mean(self.internal(1,:)), mean(self.internal(3,:));
                      mean(self.internal(3,:)), mean(self.internal(2,:)) ];
                [D,M] = eig(S);

                M = M * scale;

                % compute center point of quadrilateral to place tensor cross
                C = self.N([0,0]); CX1 = C*XX1'; CX2 = C*XX2';

                % plot tensor cross
                if M(1,1) < 0.0
                    color1 = 'b';
                else
                    color1 = 'r';
                end

                if M(2,2) < 0.0
                    color2 = 'b';
                else
                    color2 = 'r';
                end
                %plot( CX1, CX2, '*k' )
                plot( CX1+M(1,1)*[-D(1,1) +D(1,1)], CX2+M(1,1)*[-D(2,1) +D(2,1)], color1, 'linewidth', 2.0 );
                plot( CX1+M(2,2)*[-D(1,2) +D(1,2)], CX2+M(2,2)*[-D(2,2) +D(2,2)], color2, 'linewidth', 2.0 );
            end
        end
        %% print element data
        function str = print(self)
            %
            str_m11 = sprintf('%+4.3e ', self.internal(1,:)); % nodal s11 moments
            str_m22 = sprintf('%+4.3e ', self.internal(2,:)); % nodal s22 moments
            str_m12 = sprintf('%+4.3e ', self.internal(3,:)); % nodal s12 moments
            %
            str_q13 = sprintf('%+4.3e ', self.internal(4,:)); % nodal q13 shear forces
            str_q23 = sprintf('%+4.3e ', self.internal(5,:)); % nodal q23 shear forces

            str_1 = [ '\n   moment: ' str_m11 '| ' str_m22 '| ' str_m12 ];
            str_2 = [ '\n   shearf: ' str_q13 '| ' str_q23 ];

            str = [ str_1 str_2 ];
        end
    end

end
