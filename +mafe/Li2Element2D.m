classdef Li2Element2D < mafe.Element
    % basis for linear 2-node elements in 2D

    properties
		node = mafe.Node2D.empty(0,2); % the two nodes
		sect = mafe.Section1D(); % the section descriptor
		load = mafe.EleLoad(); % the element load descriptor
    end

    methods
        %% initialisation
        function obj = Li2Element2D(nodes, section, eleload)
            if exist('nodes','var')
                obj.node = nodes;
            end
            if exist('section','var')
                obj.sect = section;
            end
            if exist('eleload','var')
                obj.load = eleload;
            end
        end
        % ======================================================================
        %% matrix of nodal coordinates
        function X = X(self)
            X = reshape( [ self.node.ref ], 2, 2 ); % (2x2) = [ x1_i ]
        end
        %% linear ansatz function in local coordinates z
        function N = N(~, z)
            % (1x2) = [N_1, N_2]
            N = 0.5 * [ 1-z(1), 1+z(1) ];
        end
        %% 1st derivative of ansatz wrt local coordinate z
        function Nz = Nz(~, z)
            % (1x2) = [N_1,z1, N_2,z1 ]
            Nz = 0.5 * [ -1, +1 ];
        end
        %% Hermite ansatz function in local coordinates z
        function H = H(~, z, L)
            % (1x4) = [H_1, H_2, H_3, H_4]
            N = 0.25 * [ (1-z(1))^2 * (2+z(1)), (1-z(1))^2 * (1+z(1)) * L/2, (1+z(1))^2 * (2-z(1)), (1+z(1))^2 * (-1+z(1)) * L/2 ];
        end
        %% 2nd derivative of Hermite ansatz wrt local coordinate z
        function Hzz = Hzz(~, z, L)
            % (1x4) = [H_1,z1, H_2,z1, H_3,z1, H_4,z1 ]
            Nz = 0.125 * [ 12*z(1), L*(6*z(1)-2), -12*z(1), L*(6*z(1)+2) ] ;
        end
        %% Jacobi matrix J = J_ij = d x_j / d z_i
        function J = J(self, z)
            J = self.Nz(z) * self.X();
        end
        % ======================================================================
        %-----------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            X = self.X();
            % coordinates
            x1 = X(1,:);
            x2 = X(2,:);
            % plot defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.2 0.2 0.2];
                    falpha = 1.0;
                    lwidth = 0.5;
                    mwidth = 12.;
            end

			mafe.patchline(XX1, XX2, ...
                     'EdgeColor', lcolor, ...
                     'LineWidth', lwidth, ...
                     'EdgeAlpha', falpha);

			plot(XX1([1 end]), XX2([1 end]), ...
		             marker, ...
					 'color', mcolor, ...
					 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            %
            str = [ 'No local data ! (Not implemented?)' ];
        end
    end

end
