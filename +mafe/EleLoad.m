classdef EleLoad < handle
    % An element load descriptor

    properties
        % nodal values of external distributed load
        p = zeros(6,2); % stored in a matrix, rows = doftypes, cols = nodal vals
        % time function
        tfun = mafe.TimeFunction.Static;
    end

    methods
        function obj = EleLoad(tdof, p, tfun)
            if nargin >= 2
                if length(p) == 1
                    obj.p(tdof,:) = [p, p];
                else
                    obj.p(tdof,:) = p(1:2);
                end
            end
            if nargin >= 3
                obj.tfun = tfun;
            end
        end
    end

end
