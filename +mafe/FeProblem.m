classdef FeProblem < handle
    % A generic FE problem description

    properties
        nodes = mafe.Node.empty(0,0); % the vector of nodes of the system
        elems = mafe.Element.empty(0,0); % the vector of elements of the system
        const = mafe.Constraint.empty(0,0); % the vector of constraints of the system

        ndofs = -1; % the total number of degree of freedom in the system
    end

    methods
        %% constructor
        function obj = FeProblem(nodes, elems, const)
            if nargin >= 3
                obj.nodes = nodes;
                obj.elems = elems;
                obj.const = const;
            end
            % call initialisation of finite element problem
            obj.init();
        end
        %% initialise finite element problem
        function init(self)
            % loop over all elements and setup dof types
            for elem = self.elems
                elem.activateDofTypes();
            end
            % loop over all nodes and setup system indices of dofs
            self.ndofs = 0;
            for node = self.nodes
                node.dof = unique(node.dof,'sorted'); %sort(unique(node.dof));
                ndof = length(node.dof);
                node.idx = self.ndofs + [1:ndof];
                self.ndofs = self.ndofs + ndof;
            end
            % initialise state to zero (global dof values)
            z = zeros(self.ndofs,1);
            % initialise nodal vectors lhs and rhs
            self.updateNodalDofs(z,z)
            % initialise element internal quantities (forces, stresses)
            self.postprocessElements(z)
        end
        %% assemble system K matrix
        function A = assembleSystemStiffnessMatrix(self)
            A = self.assembleSystemStiffnessMatrixElastic() ;%+ self.assembleSystemStiffnessMatrixGeometric();
        end
        %% assemble system K matrix: elastic
        function A = assembleSystemStiffnessMatrixElastic(self)

            % create system stiffness matrix
            est_entries = round( length(self.elems) * 12*12 * 1.10 );
            num_entries = 0;
            id1 = zeros(est_entries,1);
            id2 = zeros(est_entries,1);
            mat = zeros(est_entries,1);
            % loop over all elements and collect element stiffness matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.stiffness();
                % determine number of entries
                e_size = length(e_idx)^2;
                % add element contribution to system matrix
                id1(1+num_entries:num_entries+e_size) = repmat( e_idx, 1, length(e_idx) );
                id2(1+num_entries:num_entries+e_size) = reshape( repmat( e_idx, length(e_idx), 1 ), [], 1 );
                mat(1+num_entries:num_entries+e_size) = reshape( e_mat', [], 1 );
                % add up to total number of entries
                num_entries = num_entries + e_size;
            end
            A = sparse( id1(1:num_entries), id2(1:num_entries), mat(1:num_entries) );
        end
        %% assemble system K matrix: geometric
        function A = assembleSystemStiffnessMatrixGeometric(self)
            % create system stiffness matrix
            est_entries = round( length(self.elems) * 12*12 * 1.10 );
            num_entries = 0;
            id1 = zeros(est_entries,1);
            id2 = zeros(est_entries,1);
            mat = zeros(est_entries,1);
            % loop over all elements and collect element stiffness matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.stiffnessGeometric();
                % determine number of entries
                e_size = length(e_idx)^2;
                % add element contribution to system matrix
                id1(1+num_entries:num_entries+e_size) = repmat( e_idx, 1, length(e_idx) );
                id2(1+num_entries:num_entries+e_size) = reshape( repmat( e_idx, length(e_idx), 1 ), [], 1 );
                mat(1+num_entries:num_entries+e_size) = reshape( e_mat', [], 1 );
                % add up to total number of entries
                num_entries = num_entries + e_size;
            end
            A = sparse( id1(1:num_entries), id2(1:num_entries), mat(1:num_entries) );
        end
        %% assemble system M matrix
        function A = assembleSystemMassMatrix(self)
            % create system mass matrix
            A = sparse(self.ndofs, self.ndofs);
            % loop over all elements and collect element mass matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.mass();
                % add element contribution to system matrix
                A(e_idx,e_idx) = A(e_idx,e_idx) + e_mat;
            end
        end
        %% assemble system D matrix
        function A = assembleSystemDampingMatrix(self)
            % create system damping matrix
            A = sparse(self.ndofs, self.ndofs);
            % loop over all elements and collect element damping matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.damping();
                % add element contribution to system matrix
                A(e_idx,e_idx) = A(e_idx,e_idx) + e_mat;
            end
        end
        %% assemble system f vector
        function f = assembleSystemForceVector(self, tfun)
            % create system force vector
            f = zeros(self.ndofs, 1);
            % loop over all elements and collect element mass matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element vector
                if ~exist('tfun', 'var')
                    e_vec = elem.force(); % tfun not given, collect all
                elseif isprop(elem,'load')
                    if elem.load.tfun == tfun
                        e_vec = elem.force(); % collect matching tfun
                    else
                        e_vec = elem.force() * 0.0; % collect zero
                    end
                else
                    e_vec = zeros(length(e_idx),1);
                end
                % add element contribution to system vector
                f(e_idx) = f(e_idx) + e_vec;
            end
        end
        %% apply constraints of type Neumann to the rhs
        function [b, on, off] = applyConstraintsNeumann(self, b, tfun)
            % Default value for tfun (if not given) is mafe.TimeFunction.Static
            if ~exist('tfun', 'var')
              tfun = mafe.TimeFunction.Static;
            end
            % apply constraints - dynamic/static
            idx = [];
            val = [];
            for con = self.const
                if con.type == mafe.ConstraintType.Neumann
                    if con.tfun == tfun
                        % collect matching tfun value
                        idx = [ idx, con.getDofSysIndices() ];
                        val = [ val, con.value() ];
                    end
                end
            end
            % add values and consider repeated indices:
            if length(idx) > 0 % check for zero-length idx because of accumarray
                b = b + accumarray(idx',val', [length(b) 1]);
            end
            % create index vector of dof system indices without Neumann constraints
            off = setdiff([1:self.ndofs], idx);
            % create index vector of dof system indices with    Neumann constraints
            on  = idx;
        end
        %% apply constraints of type Dirichlet to coefficient matrix and rhs
        function [b, on, off, A] = applyConstraintsDirichlet(self, A, b, tfun)
            % Default value for tfun (if not given) is mafe.TimeFunction.Static
            if ~exist('tfun', 'var')
              tfun = mafe.TimeFunction.Static;
            end
            % apply constraints - kinematic
            % --- pass 1: (homogeneous case)
            idx_h = [];
            for con = self.const
              if con.type == mafe.ConstraintType.Dirichlet
                % constraint of a different tfun is treated as homogeneous
                if con.tfun ~= tfun
                  idx_h = [ idx_h, con.getDofSysIndices() ];
                end
              end
            end
            % check for repeated indices in kinematic constraints
            if length(idx_h) ~= length(unique(idx_h))
               %disp('WARNING: FeProblem::applyConstraints() : Repeated indices in Dirichlet constraint !');
               % give preference to first occurence
               [~,occurence,~] = unique(idx_h, 'first');
               idx_h = idx_h(occurence);
            end
            % --- pass 2: (inhomogeneous case)
            idx = [];
            val = [];
            for con = self.const
              if con.type == mafe.ConstraintType.Dirichlet
                % constraint matches given tfun
                if con.tfun == tfun
                  idx = [ idx, con.getDofSysIndices() ];
                  val = [ val, con.value() ];
                end
              end
            end
            % check for repeated indices in kinematic constraints
            if length(idx) ~= length(unique(idx))
               %disp('WARNING: FeProblem::applyConstraints() : Repeated indices in Dirichlet constraint !');
               % give preference to first occurence
               [~,occurence,~] = unique(idx, 'first');
               val = val(occurence);
               idx = idx(occurence);
            end
            % impose inhomogeneous Dirchlet conditions
            if length(idx)>0
              b = b - A(:,idx) * val'; % assuming distinct indices here
            end
            % put rhs values
            b(idx_h) = 0.0; % homogeneous first
            b(idx  ) = val; % inhomogeneous second
            % determine affected indices
            idx = unique([idx_h, idx]);
            % modify lhs
            A(idx,:) = 0.0; A(:,idx) = 0.0; A(idx,idx) = eye(length(idx));
            % create index vector of dof system indices without Dirichlet constraints
            off = setdiff([1:self.ndofs], idx);
            % create index vector of dof system indices with    Dirichlet constraints
            on  = idx;
        end
        %% update solution values to nodal dofs
        function updateNodalDofs(self, lhs, rhs)
            for node = self.nodes
                node.lhs = lhs(node.idx);
                node.rhs = rhs(node.idx);
            end
        end
        %% postprocess elements
        function postprocessElements(self, u, v, a)
            if ~exist('v', 'var')
                v = zeros(self.ndofs,1);
            end
            if ~exist('a', 'var')
                a = zeros(self.ndofs,1);
            end
            for elem = self.elems
                idx = elem.getDofSysIndices();
                elem.postprocess( u(idx), v(idx), a(idx) );
            end
        end
        %% plot the system
        function plotSystem(self, config, scale)
            if ~exist('scale', 'var')
                scale = 1.0;
            end
            if ~exist('config', 'var')
                config = 'reference';
            end
            % loop over elements and draw them
            for elem = self.elems
                elem.plot(config, scale);
            end
        end
        %% print the system
        function printSystem(self)
            % loop over nodes and print them
            disp('nodal data:')
            id = 0;
            for node = self.nodes
                fprintf( [ sprintf('node %3d: ', id), node.print(), '\n' ] );
                id = id + 1;
            end
            % loop over elements and print them
            disp('element data:')
            id = 0;
            for elem = self.elems
                fprintf( [ sprintf('elem %3d: ', id), elem.print(), '\n' ] );
                id = id + 1;
            end
        end
    end

end
