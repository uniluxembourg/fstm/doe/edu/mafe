classdef FeAnalysisDynamicFD < mafe.FeAnalysis
    % dynamic analysis of finite element problem in frequency domain

    properties
        X = []; % matrix of modes columns-wise
        L = []; % vector of eigenvalues
        A = []; % vector of coefficients (initial conditions)

        Z = []; % column-matrix of particular solutions
        P = []; % column-matrix of forcing vectors

        M = []; % mass matrix
        D = []; % damping matrix
        K = []; % stiffness matrix

        tfuns = [ mafe.TimeFunction.Static ]; % vector of time functions used
    end

    methods
        %% constructor
        function obj = FeAnalysisDynamicFD(fep, tfuns)
            if nargin >= 1
                obj.fep = fep;
            end
            if nargin >= 2
                obj.tfuns = [ mafe.TimeFunction.Static, tfuns ];
            end
        end
        %% calculate dynamic response
        function analyse(self)
            % make sure the problem is initialised
            self.fep.init();
            % get system mass and stiffness matrix and force vector
            self.M = self.fep.assembleSystemMassMatrix();
            self.D = self.fep.assembleSystemDampingMatrix();
            self.K = self.fep.assembleSystemStiffnessMatrix();
            % identify indices of prescribed Dirchlet constraints
            [~,dbc,act] = self.fep.applyConstraintsDirichlet(self.K, zeros(self.fep.ndofs,1));
            % solve eigenproblem -----------------------------------------------
            % (a) compute eigenvalues lambda (including complex conjugate)
            sfac = 1./max( [ normest(self.M,1e-4), normest(self.D,1e-4), normest(self.K,1e-4) ] ); % scale just for polyeig
            self.L = polyeig(self.K(act,act)*sfac, self.D(act,act)*sfac, self.M(act,act)*sfac);
            % (b) compute eigenvectors
            self.X = zeros(self.fep.ndofs,length(self.L));
            for ii = 1:length(self.L)
                lambda = self.L(ii);
                A = lambda^2 * self.M(act,act) + lambda * self.D(act,act) + self.K(act,act);

                % compute eigenvector by solution of linear system A(lambda_k)*x_k=e
                % b = -A(:,1);
                % A(:,1) = 0;
                % A(1,:) = 0;
                % A(1,1) = 1; b(1) = 1;
                % self.X(act,ii) = A\b;

                % compute eigenvector by singular value decomposition
                % TODO: check if this is generalisable
                [U,S,V] = svds(A, 1, 'smallest');
                self.X(act,ii) = V(:,end);
                % scale computed eigenvector
                self.X(:,ii) = 1./sqrt(self.X(:,ii)'*self.M*self.X(:,ii)) * self.X(:,ii); % normalize mode wrt M
            end



            % % (xx) compute eigenvalues of the first order system
            % idx1 = [1:self.fep.ndofs]; idx2 = idx1 + self.fep.ndofs;
            % size = self.fep.ndofs*2;
            % A = zeros(size,size);
            % B = zeros(size,size);
            % A(idx1,idx1) = -eye(self.fep.ndofs,self.fep.ndofs);
            % A(idx2,idx2) = +M;
            % B(idx1,idx2) = +eye(self.fep.ndofs,self.fep.ndofs);
            % B(idx2,idx1) = +K;
            % B(idx2,idx2) = +D;
            % aact = [act act+self.fep.ndofs];
            % self.L = eig(B(aact,aact),-A(aact,aact));
            % self.X = zeros(self.fep.ndofs,length(self.L));
            % for ii = 1:length(self.L)
            %     lambda = self.L(ii);
            %     C = lambda * A(aact,aact) + B(aact,aact);
            %     b = -C(:,1);
            %     C(:,1) = 0;
            %     C(1,:) = 0;
            %     C(1,1) = 1; b(1) = 1;
            %     tempx = C\b;
            %     self.X(act,ii) = tempx(1:end/2);
            %     %norm( ( lambda * A(aact,aact) + B(aact,aact) ) * tempx, 2 )
            %     %norm( ( lambda^2 * M(act,act) + lambda * D(act,act) + K(act,act) ) * self.X(act,ii), 2 )
            %     self.X(:,ii) = 1./sqrt(self.X(:,ii)'*M*self.X(:,ii)) * self.X(:,ii); % normalize mode wrt M
            % end



            % (c) sort eigenvalues/eigenvectors in ascending order
            [Lsorted, permutation] = sort(self.L);
            self.X = self.X(:,permutation);
            self.L = Lsorted;
            self.A = ones(length(self.L),1); % put factor=1 as default
            % solve particular problem(s)
            self.Z = zeros(self.fep.ndofs,length(self.tfuns)); % particular solution vectors
            % store forcing vector(s)
            self.P = zeros(self.fep.ndofs,length(self.tfuns)); % particular forcing vectors
            %
            ii = 1;
            for tf = self.tfuns
                % compute load vector associated with time function tf
                p = self.fep.assembleSystemForceVector(tf);
                % apply Neumann constraints associated with time function tf
                f = self.fep.applyConstraintsNeumann(p, tf);
                % apply Dirichlet constraints associated with time function tf
                fa = self.fep.applyConstraintsDirichlet(self.M, zeros(self.fep.ndofs,1), tf);
                fv = self.fep.applyConstraintsDirichlet(self.D, zeros(self.fep.ndofs,1), tf);
                fu = self.fep.applyConstraintsDirichlet(self.K, zeros(self.fep.ndofs,1), tf);
                % multiply load vector with evaluated time function
                fa = fa * ( -1*tf.Omega^2 );
                fv = fv * ( 1i*tf.Omega   );
                % compose load vector
                ff = f + fa + fv + fu;
                % calculate system response associated with time function tf
                self.Z(act,ii) = ( -tf.Omega^2 * self.M(act,act) + 1i * tf.Omega * self.D(act,act) + self.K(act,act) ) \ ff(act);
                % check resonance case
                if length( find( self.Z(act,ii) == Inf ) ) > 0
                  warning('FeAnalysisDynamicFD: Given excitation frequency of TimeFunction seems to match system eigenfrequency!');
                  tf
                  self.Z(act,ii) = 0; % neglect this solution
                end
                % put kinematic constraint values to solution
                self.Z(dbc,ii) = fu(dbc);
                % put local force vector
                self.P(:,ii)   = f(:);
                %
                ii = ii + 1;
            end
        end
        %% apply initial conditions
        function applyInitialConditions(self, u0, v0, t0)
            u0 = u0;
            %
            if ~exist('v0', 'var')
                v0 = zeros(self.fep.ndofs,1);
            end
            if ~exist('t0', 'var')
                t0 = 0.0;
            end
            %
            C = zeros(2*self.fep.ndofs, length(self.L));
            %
            for ii = 1:length(self.L)
                C(1+0*self.fep.ndofs:1*self.fep.ndofs,ii) = self.X(:,ii)              * exp(self.L(ii)*t0);
                C(1+1*self.fep.ndofs:2*self.fep.ndofs,ii) = self.X(:,ii) * self.L(ii) * exp(self.L(ii)*t0);
            end
            self.A = C\( [u0; v0] - [self.solInhomogeneous(t0,0); self.solInhomogeneous(t0,1)] );
            %
            % plot the amplitude spectrum of complex conjugate pairs
            % clf; hold on;
            % plot( 0.5*real(self.A(1:2:end)), '*b' );
            % plot( 0.5*imag(self.A(1:2:end)), 'xr' );
            % pause;
        end
        %% compute total solution to homogeneous equation of motion at given time t
        function xh = solHomogeneous(self, time, difforder)
            if ~exist('time', 'var')
                time = 0.0;
            end
            if ~exist('difforder', 'var')
                difforder = 0;
            end
            xh = zeros(self.fep.ndofs,1);
            switch(difforder)
                case 0
                    for ii = 1:length(self.L)
                        % component ii of the homogeneous solution x_h
                        % is typically a complex vector
                        xh = xh + self.A(ii) *              exp(self.L(ii)*time) * self.X(:,ii);
                    end
                case 1
                    for ii = 1:length(self.L)
                        % component ii of the homogeneous solution dot x_h
                        % is typically a complex vector
                        xh = xh + self.A(ii) * self.L(ii)  *exp(self.L(ii)*time) * self.X(:,ii);
                    end
                case 2
                    for ii = 1:length(self.L)
                        % component ii of the homogeneous solution ddot x_h
                        % is typically a complex vector
                        xh = xh + self.A(ii) * self.L(ii)^2*exp(self.L(ii)*time) * self.X(:,ii);
                    end
                otherwise
                    disp('Warning: Wrong choice for time diff order!')
            end
            % due to the appearance of xh_ii as complex conjugates,
            % the homogeneous solution should consist of real values only
            norm_imag = norm( imag(xh), 2 );
            if norm_imag > 1.e-6
                disp(sprintf('Warning: Solution has imaginary component! (norm = %2.3e)', norm_imag));
            end
            % return the real part only
            xh = real(xh);
        end
        %% compute solution to particular equation of motion at given time t
        function xp = solInhomogeneous(self, time, difforder)
            if ~exist('time', 'var')
                time = 0.0;
            end
            if ~exist('difforder', 'var')
                difforder = 0;
            end
            xp = zeros(self.fep.ndofs,1);
            switch(difforder)
                case 0
                    for ii = 1:length(self.tfuns)
                        tf = self.tfuns(ii);
                        xp = xp + self.Z(:,ii) * tf.eval_f(time);
                    end
                case 1
                    for ii = 1:length(self.tfuns)
                        tf = self.tfuns(ii);
                        xp = xp + self.Z(:,ii) * tf.eval_dotf(time);
                    end
                case 2
                    for ii = 1:length(self.tfuns)
                        tf = self.tfuns(ii);
                        xp = xp + self.Z(:,ii) * tf.eval_ddotf(time);
                    end
                otherwise
                    disp('Warning: Wrong choice for time diff order!')
            end
            % return the real part only
            xp = real(xp);
        end
        %% put a specific solution component to dof
        function putSolToDof(self, time, component)
            if ~exist('component', 'var')
                component = 'full';
            end
            % extract solution
            switch(component)
               case 'full'
                  u = self.solHomogeneous(time,0) + self.solInhomogeneous(time,0);
                  v = self.solHomogeneous(time,1) + self.solInhomogeneous(time,1);
                  a = self.solHomogeneous(time,2) + self.solInhomogeneous(time,2);
               case 'homogeneous'
                  u = self.solHomogeneous(time,0);
                  v = self.solHomogeneous(time,1);
                  a = self.solHomogeneous(time,2);
               case 'particular'
                  u = self.solInhomogeneous(time,0);
                  v = self.solInhomogeneous(time,1);
                  a = self.solInhomogeneous(time,2);
            end
            % --- POST-PROCESSING ---
            % get local force vector at specific time
            p = zeros(self.fep.ndofs,1);
            for ii = 1:length(self.tfuns)
                tf = self.tfuns(ii);
                p = p + self.P(:,ii) * tf.eval_f(time);
            end
            p = real(p); % transform complex notation to real
            % compute reaction forces (including dynamic effects)
            r = self.M * a + self.D * v + self.K * u - p;
            % feedback of solution values (u and r) to nodal dofs
            self.fep.updateNodalDofs(u, r);
            % postprocess elements
            self.fep.postprocessElements(u,v,a);
        end
        %% put selected mode to dof solution
        function putModeToDof(self, mode, component)
            if nargin < 1
                mode = 1;
            end
            if nargin < 2
                component = 'real';
            end
            lambda = self.L(mode);
            a = self.A(mode);
            switch component
                case 'imag'
                    u = imag( a * self.X(:,mode) );
                case 'real'
                    u = real( a * self.X(:,mode) );
                otherwise
                    u = a * self.X(:,mode);
            end
            u = 1./norm(u,2) * u;
            % compute reaction forces
            K = self.fep.assembleSystemStiffnessMatrix();
            f = self.fep.assembleSystemForceVector();
            r = K * u - f;
            % feedback of solution values (u and r) to nodal dofs
            self.fep.updateNodalDofs(u, r);
            % postprocess elements
            self.fep.postprocessElements(u);
        end
    end

end
