classdef Plate2D < mafe.Plate2DBD
    % The standard plate element --> defaults to Plate2DBD

    methods
        %% constructor
        function obj = Plate2D(varargin)
            obj = obj@mafe.Plate2DBD(varargin{:});
        end
    end

end
