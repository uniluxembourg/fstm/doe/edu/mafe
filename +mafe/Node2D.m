classdef Node2D < mafe.Node
    % Node of the finite element mesh in two dimensions (x1-x2-plane)

    properties
        ref = zeros(1,2); % vector of global coordinates
    end

    methods
        %% constructor
        function obj = Node2D(ref_coords)
          if exist('ref_coords','var')
            obj.ref = ref_coords;
          end
        end
        %% print nodal data
        function str = print(self)
          str = sprintf('[% 7.3f,% 7.3f]', self.ref(1), self.ref(2));
          str = strcat( str, print@mafe.Node(self) );
        end
    end

end
