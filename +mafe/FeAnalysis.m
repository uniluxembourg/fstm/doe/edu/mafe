classdef FeAnalysis < handle
    % An base class for any analysis of problems discretised with FE

    properties
        fep; % the description of the finite element problem
    end

    methods
        %
    end

end
