classdef Qu4Element2D < mafe.Element
    % basis for quadrilaterial 4-node elements in 2D

    properties
        node = mafe.Node2D.empty(0,4); % the four nodes
        sect = mafe.Section2D(); % the section descriptor
        load = mafe.EleLoad(); % the element load descriptor
    end

    methods
        %% initialisation
        function obj = Qu4Element2D(nodes, section, eleload)
            if exist('nodes','var')
                obj.node = nodes;
            end
            if exist('section','var')
                obj.sect = section;
            end
            if exist('eleload','var')
                obj.load = eleload;
            end
        end
        % ======================================================================
        %% matrix of nodal coordinates
        function X = X(self)
            X = reshape( [ self.node.ref ], 2, 4 )'; % (4x2) = [ x1_i | x2_i ]
        end
        %% bi-linear ansatz function in local coordinates z
        function N = N(~, z)
            % (1x4) = [N_1, N_2, N_3, N_4]
            N = 0.25 * [ (1-z(1))*(1-z(2)), (1+z(1))*(1-z(2)), (1+z(1))*(1+z(2)), (1-z(1))*(1+z(2)) ];
        end
        %% 1st derivative of ansatz wrt local coordinate z
        function Nz = Nz(~, z)
            % (2x4) = [N_1,z1, N_2,z1, N_3,z1, N4,z1; N_1,z2, N_2,z2, N_3,z2, N4,z2; ]
            Nz = 0.25 * [ (    -1)*(1-z(2)), (    +1)*(1-z(2)), (    +1)*(1+z(2)), (    -1)*(1+z(2)) ;
                          (1-z(1))*(    -1), (1+z(1))*(    -1), (1+z(1))*(    +1), (1-z(1))*(    +1) ];
        end
        %% Jacobi matrix J = J_ij = d x_j / d z_i
        function J = J(self, z)
            J = self.Nz(z) * self.X();
        end
        %% tensor transform of tensor in voigt notation L_voigt = [l11,l22,l12]
        function T0 = T0(~, T)
            T0 = [       T(1,1)^2,        T(2,1)^2,             2*T(1,1)*T(2,1); ...
                         T(1,2)^2,        T(2,2)^2,             2*T(1,2)*T(2,2); ...
                    T(1,1)*T(1,2),   T(2,1)*T(2,2), T(1,1)*T(2,2)+T(1,2)*T(2,1) ];
        end
        %% tensor transform of tensor in voigt notation L_voigt = [l11,l22,2*l12]
        function T1 = T1(~, T)
            T1 = [       T(1,1)^2,        T(2,1)^2,               T(1,1)*T(2,1); ...
                         T(1,2)^2,        T(2,2)^2,               T(1,2)*T(2,2); ...
                  2*T(1,1)*T(1,2), 2*T(2,1)*T(2,2), T(1,1)*T(2,2)+T(1,2)*T(2,1) ];
        end
        % ======================================================================
        %-----------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            X = self.X();
            % coordinates
            x1 = X(1,:);
            x2 = X(2,:);
            % plot defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.2 0.2 0.2];
                    falpha = 1.0;
                    lwidth = 0.5;
                    mwidth = 12.;
            end

            patch(XX1, XX2, -0.005*[1 1 1 1], lcolor, ...
                    'EdgeColor', ecolor, ...
                    'LineWidth',  lwidth, ...
                    'Marker', marker, ...
                    'MarkerSize', mwidth, ...
                    'FaceAlpha', falpha);
        end
        %% print element data
        function str = print(self)
            %
            str = [ 'No local data ! (Not implemented?)' ];
        end
    end

end
