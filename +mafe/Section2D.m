classdef Section2D < mafe.Section
    % A thin-walled structure's section descriptor

    properties
        % material properties
        E = 0.0; % elasticity modulus
        nue = 0.0; % Poisson's ratio
        rho = 0.0; % density
        mu = 0.0; % dynamic viscosity (for visvous fluid)
        % plane stress/strain state
        state = 'plane_stress';
        % geometrical properties
        t = 1.0; % section thickness
        % shear correction factor
        kappa = 5./6.; % default value according to Reissner plate theory
        % additional effects
        d_u = 0.0; % viscous damping parameter in local axial direction  (x-axis)
        d_v = 0.0; % viscous damping parameter in local transv direction (y-axis)
        d_w = 0.0; % viscous damping parameter in local transv direction (z-axis)
        d_alpha = 0.0; % damping approach after Rayleigh / mass factor
        d_beta  = 0.0; % damping approach after Rayleigh / stiffness factor
    end

    methods
        % elasticity matrix of the elastic material
        function C = elastMat2D(self)
            switch(self.state)
            case 'plane_stress'
                f = self.E / (1.0 - self.nue^2);
                C = [            f,  self.nue * f,                          0;
                      self.nue * f,             f,                          0;
                                 0,             0, f * (1.0 - self.nue) / 2.0 ];
            case 'plane_strain'
                f = self.E / ( (1.0 + self.nue) * (1.0 - 2.0 * self.nue) );
                C = [ (1.0 - self.nue) * f,          self.nue * f,          0;
                              self.nue * f,  (1.0 - self.nue) * f,          0;
                                         0,                     0,  f * (1.0 - 2.0 * self.nue) / 2.0];
            otherwise
                disp('Section2D.elastMat2D() : unknwon state!')
            end
        end
        % flexibility matrix of the elastic material
        function D = flexMat2D(self)
            switch(self.state)
            case 'plane_stress'
                f = 1.0 / self.E;
                D = [            f, -self.nue * f,                          0;
                     -self.nue * f,             f,                          0;
                                 0,             0, 2.0 * (1.0 + self.nue) * f ];
            case 'plane_strain'
                f = (1.0 + self.nue) / self.E;
                D = [ (1.0 - self.nue) * f,         -self.nue * f,          0;
                             -self.nue * f,  (1.0 - self.nue) * f,          0;
                                         0,                     0,    2.0 * f ];
            otherwise
                disp('Section2D.flexMat2D() : unknwon state!')
            end
        end

        % flexibility matrix of plate bending
        function D = flexMatPlateBending(self)
            f = 12. / (self.E * self.t^3);
            D = [          f, -f*self.nue, 0;
                 -f*self.nue,           f, 0;
                           0,           0, 2.0 * (1.0 + self.nue) * f ];
        end
        % flexibility matrix of plate shear
        function C = flexMatPlateShear(self)
            f = self.kappa * ( self.E / (2*(1.0 + self.nue)) ) * self.t;
            %
            C = 1.0/f * eye(2);
        end
        % elasticity matrix of plate bending
        function D = elastMatPlateBending(self)
            f = self.E * self.t^3 / 12 / (1-self.nue^2);
            D = [          f,  f*self.nue, 0;
                  f*self.nue,           f, 0;
                           0,           0, 0.5 * (1.0 - self.nue) * f ];
        end
        % elasticity matrix of plate shear
        function C = elastMatPlateShear(self)
            f = self.kappa * ( self.E / (2*(1.0 + self.nue)) ) * self.t;
            %
            C = f * eye(2);
        end
    end

end
