classdef Truss1D < mafe.Li2Element1D
    % truss finite element in axial direction only (1D)

    properties
		%
    end

    methods
        %% constructor
		function obj = Truss1D(varargin)
            obj = obj@mafe.Li2Element1D(varargin{:});
        end
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp1 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
			disp1 = find( [self.node.dof] == mafe.DofType.Disp1 );
            ndidx = [ self.node.idx ];
            idx = [ ndidx(disp1(1)), ndidx(disp1(2)) ];
        end
        %% calculate transformation matrix local->global
        function [L, T] = transform(self)
            dx1 = self.node(2).ref(1) - self.node(1).ref(1);
            L = dx1;
            T = eye(2); % not needed in fact
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            EA = self.sect.E * self.sect.A;
            % element matrix
            ele_mat = EA/L*[ +1,  -1; ...
                             -1,  +1 ];
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            rhoA = self.sect.rho * self.sect.A;
            % local element matrix
            ele_mat = rhoA*L/6.*[ +2, +1; ...
                                  +1, +2 ];
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            d_u = self.sect.d_u;
            % local element matrix
            ele_mat = d_u*L/6.*[ +2, +1; ...
                                 +1, +2 ];
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % transformation matrix
            [L, T] = self.transform();
            % get parameters
            pX = self.load.p(mafe.DofType.Disp1,:);
            % local element vector
            ele_vec = L/6.*[ +2, +1; ...
                             +1, +2 ] * pX';
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            % coordinates and displacements
            x1 = [ n1.ref(1), n2.ref(1) ];
            u1 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp1) ), ...
                   n2.lhs( find(n2.dof == mafe.DofType.Disp1) ) ];
            %
			% defaults
			lcolor = [0.6 0.6 0.6];
			mcolor = [0.2 0.2 0.2];
			lwidth = 0.5;
			mwidth = 10.;
			ecolor = [0.4 0.4 0.4];
			marker = '.';
			%
            switch config
                case 'deformed'
                    XX1 = x1+u1*scale;
                    XX2 = x1*0.;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
					falpha = 0.6;
                    lwidth = 2.0;
                    mwidth = 20.;
                case 'intensity'
                    XX1 = x1+u1*scale;
                    XX2 = x1*0.;
                    normalforce = 0.5 * ( -self.internal(1) + self.internal(2) );
                    if normalforce < 0.0
                        lcolor = [15.3, 31.0, 54.5]/100;
                    elseif normalforce > 0.0
                        lcolor = [82.7, 58.4, 16.9]/100;
                    else
                        lcolor = [58.8, 65.5,  7.8]/100;
                    end
                    mcolor = [0.0 0.0 0.0 0.0];
					falpha = 0.6;
                    lwidth = 3.0;
                    mwidth = 0.1;
				otherwise
                    plot@mafe.Li2Element1D(self, config, scale);
                    return;
            end

			mafe.patchline(XX1, XX2, ...
                     'EdgeColor', lcolor, ...
                     'LineWidth', lwidth, ...
                     'EdgeAlpha', falpha);

            plot(XX1([1 end]), XX2([1 end]), ...
                     marker, ...
					 'color', mcolor, ...
					 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            str = sprintf('%+4.3e ', [ -self.internal(1), ... % left N
                                        self.internal(2)  ]); % right N
        end
    end

end
