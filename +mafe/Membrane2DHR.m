classdef Membrane2DHR < mafe.Qu4Element2D
    % Membrane finite element in the x-y plane (2D) -- Hellinger-Reissner

    properties
        use_skew_coordinates = false;
    end

    methods
        %% constructor
        function obj = Membrane2DHR(varargin)
            obj = obj@mafe.Qu4Element2D(varargin{:});
        end
        % ======================================================================
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ mafe.DofType.Disp1 mafe.DofType.Disp2 ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
            disp1 = find( [self.node.dof] == mafe.DofType.Disp1 );
            disp2 = find( [self.node.dof] == mafe.DofType.Disp2 );
            ndidx = [ self.node.idx ];
            idx = [ ndidx(disp1), ndidx(disp2) ];
        end
        % ======================================================================
        %% stress ansatz for contra-variant components sigma^ij
        function S = S(~, z)
            S  = [ 1, 0, 0, z(2),    0;   % s11 = linear in z2
                   0, 1, 0,    0, z(1);   % s22 = linear in z1
                   0, 0, 1,    0,    0 ]; % s12 = constant
        end
        % ======================================================================
        %% calculation of element matrices of the mixed principle
        function [ele_mat_su, ele_mat_ss] = ele_sub_matrices(self)
            % sub-matrices of the mixed-hybrid form used (Hellinger-Reissner)
            displmndof = 2*4;
            stressndof = size(self.S([0,0]), 2);
            ele_mat_su = zeros(stressndof,displmndof);
            ele_mat_ss = zeros(stressndof,stressndof);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % flexibility matrix
            D = self.sect.flexMat2D();

            % transformation
            J0 = self.J([0,0]);
            T0 = self.T0(J0);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    Nz = self.Nz(z);  % local derivative
                    Nx = inv(J) * Nz; % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % strain approach (in global coordinates)
                    B = [    Nx(1,:), zeros(1,4) ;
                          zeros(1,4),    Nx(2,:) ;
                             Nx(2,:),    Nx(1,:) ];
                    % stress approach (in global coordinates)
                    S = T0 * self.S(z);
                    % internal virtual work expressions
                    ele_mat_su = ele_mat_su + factor * S' * B;
                    ele_mat_ss = ele_mat_ss - factor * S' * D * S;
                end
            end
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % obtain sub matrices
            [ele_mat_su, ele_mat_ss] = self.ele_sub_matrices();
            % static condensation of element-local stresses
            ele_mat = -ele_mat_su' * inv(ele_mat_ss) * ele_mat_su;
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % mass only affects disp
            displmndof = 2*4;
            ele_mat = zeros(displmndof,displmndof);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % density
            rho = self.sect.rho;

            % transformation
            J0 = self.J([0,0]);
            T0 = self.T0(J0);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    N  = self.N(z);   % local derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % strain approach (in global coordinates)
                    A = [          N, zeros(1,4) ;
                          zeros(1,4),          N ];
                    % internal virtual work expressions
                    ele_mat = ele_mat + factor * A' * rho * A;
                end
            end
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % TODO
            ele_mat = zeros(8,8);
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % get parameters
            p1 = mean( self.load.p(mafe.DofType.Disp1,:) ); % assume constant loads
            p2 = mean( self.load.p(mafe.DofType.Disp2,:) ); % per element
            % global element vector
            ele_vec = zeros(8,1);

            % Gauss integration parameters (1 point sufficent for constant load)
            [GC, GW] = mafe.GaussCoordWeights(1);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    N  = self.N(z);   % same ansatz for geometry and displacements
                    J  = self.J(z);   % Jacobi matrix
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % external virtual work expressions
                    ele_vec = ele_vec + factor * [ N'*p1; N'*p2 ];
                end
            end
        end
        %------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, u, du, ddu)
            if ~exist('ddu', 'var')
                ddu = zeros(length(u),1);
            end
            if ~exist('du', 'var')
                du = zeros(length(u),1);
            end
            % obtain sub matrices
            [ele_mat_su, ele_mat_ss] = self.ele_sub_matrices();
            % compute local stress dof
            s = ele_mat_ss \ (-ele_mat_su * u);
            % transformation
            J0 = self.J([0,0]);
            T0 = self.T0(J0);
            % evaluate stress ansatz at corner nodes and store tensor components
            self.internal = T0 * [ self.S( [-1., -1.] ) * s, ...
                                   self.S( [+1., -1.] ) * s, ...
                                   self.S( [+1., +1.] ) * s, ...
                                   self.S( [-1., +1.] ) * s ] ;
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            n2 = self.node(2);
            n3 = self.node(3);
            n4 = self.node(4);
            % coordinates and displacements
            x1 = [ n1.ref(1), n2.ref(1), n3.ref(1), n4.ref(1) ];
            x2 = [ n1.ref(2), n2.ref(2), n3.ref(2), n4.ref(2) ];
            u1 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp1) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Disp1) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Disp1) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Disp1) ) ]';
            u2 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp2) ),
                   n2.lhs( find(n2.dof == mafe.DofType.Disp2) ),
                   n3.lhs( find(n3.dof == mafe.DofType.Disp2) ),
                   n4.lhs( find(n4.dof == mafe.DofType.Disp2) ) ]';
            %
            % defaults
            lcolor = [0.6 0.6 0.6];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.5;
            mwidth = 10.;
            ecolor = [0.4 0.4 0.4];
            marker = '.';
            %
            switch config
                case 'deformed'
                    XX1 = x1+u1*scale;
                    XX2 = x2+u2*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
                    falpha = 0.2;
                    lwidth = 1.0;
                    mwidth = 20.;
                case 'stress11'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(1,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'stress22'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(2,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'stress12'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = self.internal(3,:);
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'vonmises'
                    XX1 = x1;
                    XX2 = x2;
                    % calculate nodal von mises stress from stress tensor
                    s11 = self.internal(1,:);
                    s22 = self.internal(2,:);
                    s12 = self.internal(3,:);
                    switch(self.sect.state)
                    case 'plane_stress'
                        sigma_v = sqrt( s11.^2 + s22.^2 - s11.*s22 + 3*s12.^2 );
                    case 'plane_strain'
                        nue = self.sect.nue;
                        sigma_v = sqrt( (s11.^2 + s22.^2)*(nue^2-nue+1) + (s11.*s22)*(2*nue^2-2*nue-1) + 3*s12.^2 );
                    otherwise
                        disp('Unknown plane state!');
                        sigma_v = 0.0;
                    end
                    lcolor = sigma_v;
                    falpha = 1.0;
                    ecolor = 'none';
                    marker = 'none';
                case 'tensor'
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [1.0 1.0 1.0];
                    mcolor = [0.1 0.1 0.1];
                    falpha = 1.0;
                    lwidth = 0.01;
                    mwidth = 0.01;
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.2 0.2 0.2];
                    falpha = 1.0;
                    lwidth = 0.5;
                    mwidth = 12.;
            end
            patch(XX1, XX2, -0.005*[1 1 1 1], lcolor, ...
                    'EdgeColor', ecolor, ...
                    'LineWidth',  lwidth, ...
                    'Marker', marker, ...
                    'MarkerSize', mwidth, ...
                    'FaceAlpha', falpha);

            % put a tensor representtaion if required
            if strcmp(config, 'tensor')
                % compute tensor representation
                S = [ mean(self.internal(1,:)), mean(self.internal(3,:));
                      mean(self.internal(3,:)), mean(self.internal(2,:)) ];
                [D,M] = eig(S);

                M = M * scale;

                % compute center point of quadrilateral to place tensor cross
                C = self.N([0,0]); CX1 = C*XX1'; CX2 = C*XX2';

                % plot tensor cross
                if M(1,1) < 0.0
                    color1 = 'b';
                else
                    color1 = 'r';
                end

                if M(2,2) < 0.0
                    color2 = 'b';
                else
                    color2 = 'r';
                end
                %plot( CX1, CX2, '*k' )
                plot( CX1+M(1,1)*[-D(1,1) +D(1,1)], CX2+M(1,1)*[-D(2,1) +D(2,1)], color1, 'linewidth', 2.0 );
                plot( CX1+M(2,2)*[-D(1,2) +D(1,2)], CX2+M(2,2)*[-D(2,2) +D(2,2)], color2, 'linewidth', 2.0 );
            end
        end
        %% print element data
        function str = print(self)
            %
            str_s11 = sprintf('%+4.3e ', self.internal(1,:)); % nodal s11 stresses
            str_s22 = sprintf('%+4.3e ', self.internal(2,:)); % nodal s22 stresses
            str_s12 = sprintf('%+4.3e ', self.internal(3,:)); % nodal s12 stresses
            %
            str = [ '\n   normal: ' str_s11 '| ' str_s22 '| ' str_s12 ];
            %
        end
    end

end
