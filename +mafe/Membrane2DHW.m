classdef Membrane2DHW < mafe.Membrane2DHR
    % Membrane finite element in the x-y plane (2D) -- Hu-Washizu

    properties

    end

    methods
        %% constructor
        function obj = Membrane2DHW(varargin)
            obj = obj@mafe.Membrane2DHR(varargin{:});
        end
        % ======================================================================
        %% stress ansatz for contra-variant components sigma^ij
        function S = S(~, z)
            % Pian-Sumihara
            S  = [ 1, 0, 0, z(2),    0;   % s11 = linear in z2
                   0, 1, 0,    0, z(1);   % s22 = linear in z1
                   0, 0, 1,    0,    0 ]; % s12 = constant
        end
        %% strain ansatz for co-variant components epsilon_ij
        function E = E(~, z)
            % % option 1: strain ansatz space matching E = inv(C)*S for C = elastmat --> too stiff ?
            % E  = [ 1, 0, 0, z(1), z(2),    0,    0;   %   e11 = linear in z1 + z2
            %        0, 1, 0,    0,    0, z(1), z(2);   %   e22 = linear in z1 + z2
            %        0, 0, 1,    0,    0,    0,    0 ]; % 2*e12 = constant
            % option 2: strain ansatz space uniformly linear
            E  = [ 1, 0, 0, z(1), z(2),    0,    0,    0,    0;   %   e11 = linear in z1 + z2
                   0, 1, 0,    0,    0, z(1), z(2),    0,    0;   %   e22 = linear in z1 + z2
                   0, 0, 1,    0,    0,    0,    0, z(1), z(2) ]; % 2*e12 = linear in z1 + z2
        end
        % ======================================================================
        %% calculation of element sub matrices of the mixed principle
        function [ele_mat_su, ele_mat_es, ele_mat_ee] = ele_sub_matrices(self)
            % sub-matrices of the mixed-hybrid form used (Hu-Washizu)
            displmndof = 2*4;
            stressndof = size(self.S([0,0]), 2);
            strainndof = size(self.E([0,0]), 2);

            ele_mat_su = zeros(stressndof,displmndof);
            ele_mat_es = zeros(strainndof,stressndof);
            ele_mat_ee = zeros(strainndof,strainndof);

            % Gauss integration parameters
            [GC, GW] = mafe.GaussCoordWeights(2);

            % elasticity matrix
            C = self.sect.elastMat2D();

            % transformation
            J0 = self.J([0,0]);
            T0 = self.T0(J0);
            T1 = self.T1(J0);

            % numerical integration of weak form
            for ii = 1:length(GC) % looping over gauss points in z1 direction
                for jj = 1:length(GC) % looping over gauss points in z2 direction
                    % gauss coordinates
                    z = [ GC(ii), GC(jj) ];
                    % gauss weights
                    w = GW(ii) * GW(jj);
                    % shape/ansatz functions
                    J  = self.J(z);   % Jacobi matrix
                    Nz = self.Nz(z);  % local derivative
                    Nx = inv(J) * Nz; % global derivative
                    detJ = det(J);    % Jacobian
                    % integration factor
                    factor = w * detJ * self.sect.t;
                    % strain approach (in global coordinates)
                    B = [    Nx(1,:), zeros(1,4) ;
                          zeros(1,4),    Nx(2,:) ;
                             Nx(2,:),    Nx(1,:) ];
                    % stress approach (in global coordinates)
                    S = T0 * self.S(z);
                    % strain approach (in global coordinates)
                    E = T1 * self.E(z);
                    % internal virtual work expressions
                    ele_mat_su = ele_mat_su + factor * S' * B;
                    ele_mat_es = ele_mat_es - factor * E' * S;
                    ele_mat_ee = ele_mat_ee + factor * E' * C * E;
                end
            end
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % obtain sub matrices
            [ele_mat_su, ele_mat_es, ele_mat_ee] = self.ele_sub_matrices();
            % static condensation of element-local strains
            ele_mat_ss = -ele_mat_es' * inv(ele_mat_ee) * ele_mat_es;
            % static condensation of element-local stresses
            ele_mat = -ele_mat_su' * inv(ele_mat_ss) * ele_mat_su;
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % TODO
            ele_mat = zeros(8,8);
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % TODO
            ele_mat = zeros(8,8);
        end
        %------------------------------------------------------------------
        %% post-process (compute internal forces)
        function postprocess(self, u, du, ddu)
            if ~exist('ddu', 'var')
                ddu = zeros(length(u),1);
            end
            if ~exist('du', 'var')
                du = zeros(length(u),1);
            end
            % obtain sub matrices
            [ele_mat_su, ele_mat_es, ele_mat_ee] = self.ele_sub_matrices();
            % compute local element dof
            ele_mat_ss = -ele_mat_es' * inv(ele_mat_ee) * ele_mat_es;
            s =  ele_mat_ss \ (-ele_mat_su * u);
            e =  ele_mat_ee \ (-ele_mat_es * s);
            % transformation
            J0 = self.J([0,0]);
            T0 = self.T0(J0);
            T1 = self.T1(J0);
            % evaluate stress ansatz at corner nodes and store tensor components
            self.internal = [ T0 * [ self.S( [-1., -1.] ) * s, ...
                                     self.S( [+1., -1.] ) * s, ...
                                     self.S( [+1., +1.] ) * s, ...
                                     self.S( [-1., +1.] ) * s ] ;
                              T1 * [ self.E( [-1., -1.] ) * e , ...
                                     self.E( [+1., -1.] ) * e, ...
                                     self.E( [+1., +1.] ) * e, ...
                                     self.E( [-1., +1.] ) * e ] ;
                            ];
        end
        %------------------------------------------------------------------
        %% print element data
        function str = print(self)
            %
            str_s11 = sprintf('%+4.3e ', self.internal(1,:)); % nodal s11 stresses
            str_s22 = sprintf('%+4.3e ', self.internal(2,:)); % nodal s22 stresses
            str_s12 = sprintf('%+4.3e ', self.internal(3,:)); % nodal s12 stresses
            %
            str_e11 = sprintf('%+4.3e ', self.internal(4,:)); % nodal e11 strains
            str_e22 = sprintf('%+4.3e ', self.internal(5,:)); % nodal e22 strains
            str_e12 = sprintf('%+4.3e ', self.internal(6,:)); % nodal e12 strains

            str_1 = [ '\n   stress: ' str_s11 '| ' str_s22 '| ' str_s12 ];
            str_2 = [ '\n   strain: ' str_e11 '| ' str_e22 '| ' str_e12 ];

            str = [ str_1 str_2 ];
        end
    end

end
