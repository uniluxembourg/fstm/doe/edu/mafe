classdef Point < mafe.Element
    % Point finite element -- (spring, dashpot, mass, force)

    properties
        node = mafe.Node.empty(0,1); % the node of the point element
        sect = mafe.Section0D(); % section properties (m,d,k)
        tdof = mafe.DofType.Disp1; % the active degree of freedom (direction)
    end

    methods
        %% constructor
        function obj = Point(node, section, tdof)
            if nargin >= 2
                obj.node = node;
                obj.sect = section;
            end
            if nargin >= 3
                obj.tdof = tdof;
            end
        end
        %% activation of dofs at the nodes of the element
        function activateDofTypes(self)
            ineed = [ self.tdof ];
            for i = 1:length(self.node)
                self.node(i).dof = [self.node(i).dof ineed];
            end
        end
        %% provide element index vector of dof
        function idx = getDofSysIndices(self)
            idx = [ self.node(1).idx( find(self.node(1).dof == self.tdof) ) ];
        end
        %% calculate transformation matrix local->global
        function [L, T] = transform(self)
            L = 0.0;
            T = eye(1,1);
        end
        %% calculation of element stiffness matrix
        function ele_mat = stiffness(self)
            % element matrix
            ele_mat = [ self.sect.k ];
        end
        %% calculation of element mass matrix
        function ele_mat = mass(self)
            % element matrix
            ele_mat = [ self.sect.m ];
        end
        %% calculation of element damping matrix
        function ele_mat = damping(self)
            % element matrix
            ele_mat = [ self.sect.d + ...
                        self.sect.d_alpha * self.sect.m + self.sect.d_beta * self.sect.k ];
        end
        %% calculation of element force vector
        function ele_vec = force(self)
            % local element vector
            ele_vec = [ 0 ]; % always zero, nodal forces are applied via constraint
        end
        %------------------------------------------------------------------
        %% graphical output: plot element
        function plot(self, config, scale)
            % my nodes
            n1 = self.node(1);
            % coordinates and displacements
            x1 = [ n1.ref(1) ];
            if length(n1.ref) > 1
                x2 = [ n1.ref(2) ];
            else
                x2 = [ 0 ];
            end
            if length(find(n1.dof == mafe.DofType.Disp1)) > 0
                u1 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp1) ) ];
            else
                u1 = [ 0 ];
            end
            if length(find(n1.dof == mafe.DofType.Disp2)) > 0
                u2 = [ n1.lhs( find(n1.dof == mafe.DofType.Disp2) ) ];
            else
                u2 = [ 0 ];
            end
            %
            switch config
                case 'deformed'
                    XX1 = x1+u1*scale;
                    XX2 = x2+u2*scale;
                    lcolor = [43.9, 50.6,  0.0]/100;
                    mcolor = [43.9, 50.6,  0.0]/100;
                    lwidth = 4.0;
                    mwidth = 150.;
                otherwise
                    XX1 = x1;
                    XX2 = x2;
                    lcolor = [0.6 0.6 0.6];
                    mcolor = [0.6 0.6 0.6];
                    lwidth = 2.0;
                    mwidth = 150.;
            end

			if self.sect.m == 0.0 % if the mass is zero, no need to draw it
				mwidth = 1.e-4;
			end

            plot(XX1         , XX2         , '-', 'color', lcolor, 'lineWidth',  lwidth);
            plot(XX1([1 end]), XX2([1 end]), '.', 'color', mcolor, 'markersize', mwidth);
        end
        %% print element data
        function str = print(self)
            str = sprintf('%+4.3e ', [ +self.internal(1) ]); % force/moment
        end
    end

end
