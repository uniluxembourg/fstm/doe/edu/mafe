classdef TimeFunction < handle
    % A time function descriptor,
    % represents the function f(t) = (f_c - i * f_s) * exp(i*Omega*t)

    properties
        factor_cos = 1.0; % factor for the cosine-related part
        factor_sin = 0.0; % factor for the sine-related part
        Omega = 0.0; % angular frequency
    end

    properties (Constant)
        Static = mafe.TimeFunction( 0.0, 1.0, 0.0 ); % default TimeFunction
    end

    methods
        %% constructor
        function obj = TimeFunction(Omega, factor_cos, factor_sin)
            if nargin >= 1
                obj.Omega = Omega;
            end
            if nargin >= 2
                obj.factor_cos = factor_cos;
            end
            if nargin >= 3
                obj.factor_sin = factor_sin;
            end
        end
        %% get factor
        function val = factor(self)
            val = (self.factor_cos - 1i * self.factor_sin);
        end
        %% get f(t)
        function val = eval_f(self, time)
            val = self.factor() * exp(1i * self.Omega * time);
        end
        %% get df(f)/dt
        function val = eval_dotf(self, time)
            val = i*self.Omega * self.eval_f(time);
        end
        %% get d^2f(f)/dt^2
        function val = eval_ddotf(self, time)
            val = i*self.Omega * self.eval_dotf(time);
        end

        %% get f(t) for cosine part
        function val = eval_cos_f(self, time)
            val = self.factor_cos * cos(self.Omega * time);
        end
        %% get f(t) for sine part
        function val = eval_sin_f(self, time)
            val = self.factor_sin * sin(self.Omega * time);
        end
        %% get df(t)/dt for cosine part
        function val = eval_cos_dotf(self, time)
            val = -self.Omega   * self.factor_cos * sin(self.Omega * time);
        end
        %% get df(t)/dt for sine part
        function val = eval_sin_dotf(self, time)
            val = +self.Omega   * self.factor_sin * cos(self.Omega * time);
        end
        %% get d^2f(t)/dt^2 for cosine part
        function val = eval_cos_ddotf(self, time)
            val = -self.Omega^2 * self.factor_cos * cos(self.Omega * time);
        end
        %% get d^2f(t)/dt^2 for sine part
        function val = eval_sin_ddotf(self, time)
            val = -self.Omega^2 * self.factor_sin * sin(self.Omega * time);
        end
    end

end
