classdef TimeIntegrationType < uint8
    % Enumeration of available time integration types
    enumeration
        CrankNicolson      (1),
        CentralDifference  (2),
        LinearAcceleration (3),
        FoxGoodwin         (4),
        GeneralisedNewmark (5),
        GeneralisedAlpha   (6),
        Bathe              (7),
    end
end
