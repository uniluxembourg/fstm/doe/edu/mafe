classdef FeProblemLinearised < mafe.FeProblem
    % A generic FE problem description

    properties
        %
    end

    methods
        %% constructor
        function obj = FeProblemLinearised(varargin)
            obj = obj@mafe.FeProblem(varargin{:});
        end
        %% assemble system matrix related to dx
        function A = assembleSystemMatrix_Linear_dx(self)
            % % create system tangent matrix
            % id1 = zeros(0,1);
            % id2 = zeros(0,1);
            % mat = zeros(0,1);
            % % loop over all elements and collect element matrices
            % for elem = self.elems
            %     % get element index vector
            %     e_idx = elem.getDofSysIndices();
            %     % get element matrix
            %     e_mat = elem.elemat_dx( x(e_idx) );
            %     % add element contribution to system matrix
            %     id1 = [ id1, repmat( e_idx, 1, length(e_idx) )  ];                   % TODO: clean up here
            %     id2 = [ id2; reshape( repmat( e_idx, length(e_idx), 1 ), [], 1 ) ];
            %     mat = [ mat; reshape( e_mat', [], 1 ) ];
            % end
            % A = sparse( id1, id2, mat );

            % create system damping matrix
            A = sparse(self.ndofs, self.ndofs);
            % loop over all elements and collect element damping matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.elemat_linear_dx();
                % add element contribution to system matrix
                A(e_idx,e_idx) = A(e_idx,e_idx) + e_mat;
            end
        end
        %% assemble system matrix related to dx
        function A = assembleSystemMatrix_Nonlinear_dx(self)
            % % create system tangent matrix
            % id1 = zeros(0,1);
            % id2 = zeros(0,1);
            % mat = zeros(0,1);
            % % loop over all elements and collect element matrices
            % for elem = self.elems
            %     % get element index vector
            %     e_idx = elem.getDofSysIndices();
            %     % get element matrix
            %     e_mat = elem.elemat_dx( x(e_idx) );
            %     % add element contribution to system matrix
            %     id1 = [ id1, repmat( e_idx, 1, length(e_idx) )  ];                   % TODO: clean up here
            %     id2 = [ id2; reshape( repmat( e_idx, length(e_idx), 1 ), [], 1 ) ];
            %     mat = [ mat; reshape( e_mat', [], 1 ) ];
            % end
            % A = sparse( id1, id2, mat );

            % create system damping matrix
            A = sparse(self.ndofs, self.ndofs);
            % loop over all elements and collect element damping matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.elemat_nonlinear_dx();
                % add element contribution to system matrix
                A(e_idx,e_idx) = A(e_idx,e_idx) + e_mat;
            end
        end
        %% assemble system matrix related to dot_dx
        function A = assembleSystemMatrix_dot_dx(self, x_lastiter)
            % create system tangent matrix
            id1 = zeros(0,1);
            id2 = zeros(0,1);
            mat = zeros(0,1);
            % loop over all elements and collect element matrices
            for elem = self.elems
                % get element index vector
                e_idx = elem.getDofSysIndices();
                % get element matrix
                e_mat = elem.elemat_dot_dx( x_lastiter(e_idx) );
                % add element contribution to system matrix
                id1 = [ id1, repmat( e_idx, 1, length(e_idx) )  ];                   % TODO: clean up here
                id2 = [ id2; reshape( repmat( e_idx, length(e_idx), 1 ), [], 1 ) ];
                mat = [ mat; reshape( e_mat', [], 1 ) ];
            end
            A = sparse( id1, id2, mat );
        end
        %% postprocess elements
        function postprocessElements(self, x, dot_x, ddot_x)
            if ~exist('x', 'var')
                x = zeros(self.ndofs,1);
            end
            if ~exist('dot_x', 'var')
                dot_x = zeros(self.ndofs,1);
            end
            if ~exist('ddot_x', 'var')
                ddot_x = zeros(self.ndofs,1);
            end
            for elem = self.elems
                idx = elem.getDofSysIndices();
                elem.postprocess( x(idx), dot_x(idx), ddot_x(idx) );
            end
        end
    end

end
