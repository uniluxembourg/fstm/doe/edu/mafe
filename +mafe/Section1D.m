classdef Section1D < mafe.Section
    % A truss/beam/member section descriptor

    properties
        % material properties
        E = 0.0; % elasticity modulus
        nue = 0.0; % Poisson's ratio
        rho = 0.0; % density
        % geometrical properties
        A = 0.0; % section area
        t = 0.0; % section thickness
        Iy = 0.0; % second area moment about local y-axis
        Iz = 0.0; % second area moment about local z-axis
        % additional effects
        d_u = 0.0; % viscous damping parameter in local axial direction  (x-axis)
        d_v = 0.0; % viscous damping parameter in local transv direction (y-axis)
        d_w = 0.0; % viscous damping parameter in local transv direction (z-axis)
        d_alpha = 0.0; % damping approach after Rayleigh / mass factor
        d_beta  = 0.0; % damping approach after Rayleigh / stiffness factor
    end

    methods
        %
    end

end
