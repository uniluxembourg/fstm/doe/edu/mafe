classdef Membrane2D < mafe.Membrane2DHR
    % The standard 2D elasticity element --> defaults to Membrane2DHR

    methods
        %% constructor
        function obj = Membrane2D(varargin)
            obj = obj@mafe.Membrane2DHR(varargin{:});
        end
    end

end
