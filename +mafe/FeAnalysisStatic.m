classdef FeAnalysisStatic < mafe.FeAnalysis
    % static analysis of finite element problem

    properties
        %
    end

    methods
        %% constructor
        function obj = FeAnalysisStatic(fep)
            if nargin > 0
                obj.fep = fep;
            end
        end
        %% calculate static response
        function analyse(self)
            % make sure the problem is initialised
            self.fep.init();
            % get system stiffness matrix and force vector
            K = self.fep.assembleSystemStiffnessMatrix();
            p = self.fep.assembleSystemForceVector();
            % apply constraints
            [f          ] = self.fep.applyConstraintsNeumann(p);
            [f, dbc, act] = self.fep.applyConstraintsDirichlet(K, f);
            % allocate solution vector
            u = zeros(self.fep.ndofs,1);
            % solve system of equations (active indices only)
            u(act) = K(act,act)\f(act);
            u(dbc) = f(dbc);
            % compute reaction forces
            r = K * u - p;
            % feedback of solution values (u and r) to nodal dofs
            self.fep.updateNodalDofs(u, r);
            % postprocess elements
            self.fep.postprocessElements(u);
        end
    end

end
