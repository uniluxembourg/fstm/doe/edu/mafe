classdef MgGeoArea4Line < mgen.MgGeoArea
    % A 4-line defined area of the geometry

    properties (Access = protected)
        geoLines  = mgen.MgGeoLine.empty(0,4);
    end

    methods
        %% constructor
        function obj = MgGeoArea4Line(geoLines)
            if nargin >= 1
                if length(geoLines) ~= 4
                    disp('MgGeoArea4Line: wrong number of input GeoLines');
                end
                obj.geoLines = geoLines;
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoArea4Line: ' );
        end

        %% ---------------------------------------------------------------------
        function change = prepareLines(self)

            change = false;

            for ii = 1:2
                left  = self.geoLines(ii  ).getNumTargetGridNodes();
                right = self.geoLines(ii+2).getNumTargetGridNodes();

                if left ~= right
                    if left > right
                        self.geoLines(ii+2).setNumTargetGridNodes(left);
                    else
                        self.geoLines(ii  ).setNumTargetGridNodes(right);
                    end
                    change = true;
                end
            end
        end

        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            dim0     = self.geoLines(1).getNumTargetGridNodes();
            dim1     = self.geoLines(2).getNumTargetGridNodes();
            dim      = dim0 * dim1;
            run_id   = 1 + length(gridnodes);

            grow = (dim0-1)*(dim1-1);
            bucket = mgen.MgGridNode(1,grow); bucket(:) = mgen.MgGridNode(-1, [0.0, 0.0]);
            gridnodes = [ gridnodes bucket ];

            self.gridNodes = mgen.MgGridNode.empty(0,dim);

            % check start points of lines
            self.geoLines(1).getCommonGridNode( self.geoLines(2) );
            self.geoLines(3).getCommonGridNode( self.geoLines(4) );

            % change once again, such that A/B and C/D run from the same side
            self.geoLines(3).switchStartGridNode();
            self.geoLines(4).switchStartGridNode();

            eps = 1.e-6;

            % create grid nodes
            for ii = 2:dim0-1 % straight edges

                % determine local coordinates of "same" points on both straight edges
                A = self.geoLines(1).getGridNode(   1).coord;
                B = self.geoLines(1).getGridNode(dim0).coord;
                C = self.geoLines(1).getGridNode(  ii).coord;

                z1 = norm((C-A)/(B-A));

                A = self.geoLines(3).getGridNode(   1).coord;
                B = self.geoLines(3).getGridNode(dim0).coord;
                C = self.geoLines(3).getGridNode(  ii).coord;

                z3 = norm((C-A)/(B-A));

                pos = (ii-1)*dim1 + 2;

                for jj = 2:dim1-1 % curved edges

                    % determine local coordinates of "same" points on both straight edges
                    A = self.geoLines(2).getGridNode(   1).coord;
                    B = self.geoLines(2).getGridNode(dim1).coord;
                    C = self.geoLines(2).getGridNode(  jj).coord;

                    z2 = norm((C-A)/(B-A));

                    A = self.geoLines(4).getGridNode(   1).coord;
                    B = self.geoLines(4).getGridNode(dim1).coord;
                    C = self.geoLines(4).getGridNode(  jj).coord;

                    z4 = norm((C-A)/(B-A));

                    A = self.geoLines(2).getGridNode(jj).coord;
                    B = self.geoLines(4).getGridNode(jj).coord;
                    C = self.geoLines(1).getGridNode(ii).coord;
                    D = self.geoLines(3).getGridNode(ii).coord;

                    zi = (ii-1)/(dim0-1); % local z along curve
                    zj = (jj-1)/(dim1-1); % local z along curve
                    zzi = (1-zi) * z2 + zi * z4; % interpolate outer pos
                    zzj = (1-zj) * z1 + zj * z3;

                    N = ( (1-zzj)*A + (zzj)*B + (1-zzi)*C + (zzi)*D )/2;

                    % position node
                    self.gridNodes(pos) = mgen.MgGridNode(run_id, N);

                    gnv(run_id) = self.gridNodes(pos);

                    run_id = run_id + 1;
                    pos = pos + 1;
                end
            end

            % sort in existing nodes
            for ii = 1:dim1
                self.gridNodes(ii) = self.geoLines(2).getGridNode(ii);
            end
            for ii = 2:dim0-1
                self.gridNodes((ii-1)*dim1+1) = self.geoLines(1).getGridNode(ii);
            end
            for ii = 2:dim0-1
                self.gridNodes((ii+0)*dim1) = self.geoLines(3).getGridNode(ii);
            end
            for ii = 1:dim1
                self.gridNodes(ii+dim1*(dim0-1)) = self.geoLines(4).getGridNode(ii);
            end
        end

        function gev = makeGridElems(self, gridelems)

            gev = gridelems;

            dim0     = self.geoLines(1).getNumTargetGridNodes();
            dim1     = self.geoLines(2).getNumTargetGridNodes();
            run_id   = 1 + length(gridelems);

            self.gridElems = mgen.MgGridQuad4Elem.empty(0, (dim0-1)*(dim1-1) );

            pos = 1;
            for ii = 1:dim0-1
                pu = (ii-1)*dim1+1;
                po = pu+dim1;
                for jj = 1:dim1-1
                    n1 = self.gridNodes(pu);
                    n2 = self.gridNodes(pu+1);
                    n3 = self.gridNodes(po+1);
                    n4 = self.gridNodes(po);

                    self.gridElems(pos) = mgen.MgGridQuad4Elem( run_id, [n1, n2, n3, n4] );
                    gev(end+1) = self.gridElems(pos);

                    run_id = run_id + 1;
                    pos = pos + 1;
                    pu = pu + 1;
                    po = po + 1;
                end
            end
        end

    end
end
