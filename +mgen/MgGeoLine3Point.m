classdef MgGeoLine3Point < mgen.MgGeoLine2Point
    % A 3-point line of the geometry

    properties (Access = protected)

    end

    methods
        %% constructor
        function obj = MgGeoLine3Point(geoPoints, numNodes, normFunction)
            if nargin >= 2
                obj.geoPoints = geoPoints;
                obj.numTargetGridNodes = numNodes;
                obj.normFunction = mgen.MgNormFunction( mgen.MgNormFunctionType.Linear );
            end
            if nargin >= 3
                obj.normFunction = normFunction;
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoLine3Point: gn1.id = %2d, gn2.id = %2d\n', self.gridNodes(1).id, self.gridNodes(end).id );

            for gn = self.gridNodes
                str = [ str, gn.print(), '\n' ];
            end
        end

        %% ---------------------------------------------------------------------
        function p = getGeoPoint(self,id)
            if self.switchGeoPoint
                p = self.geoPoints(3-id);
            else
                p = self.geoPoints(id);
            end
        end

        function g = getGridNode(self, id)
            if ~self.switchGridNode
                g = self.gridNodes(id);
            else
                g = self.gridNodes(end-id+1);
            end
        end

        function setNumTargetGridNodes(self, num)
            self.numTargetGridNodes = num;
        end

        function num = getNumTargetGridNodes(self)
            num = self.numTargetGridNodes;
        end

        function switchStartGridNode(self)
            self.switchGridNode = ~self.switchGridNode;
        end
        function switchStartGeoPoint(self)
            self.switchGeoPoint = ~self.switchGeoPoint;
        end

        function resetStartGridNode(self)
            self.switchGridNode = false;
        end
        function resetStartGeoPoint(self)
            self.switchGeoPoint = false;
        end

        function g = getCommonGridNode(self, geoline)

            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                geoline.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                self.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                geoline.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                disp('MgGeoLine3Point:getCommonGridNode() --> GridNodes not matching!')
            end
            g = self.getGridNode(1);
        end

        function g = getCommonGeoPoint(self, geoline)
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                geoline.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                self.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                geoline.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                disp('MgGeoLine3Point:getCommonGeoPoint() --> GeoPoints not matching!')
            end
            g = self.getGeoPoint(1);
        end

        function alignLines(self, geoline)
            self.getCommonGeoPoint(geoline);
            self.switchStartGeoPoint();

            if length(self.gridNodes) > 0
                self.getCommonGridNode(geoline);
                self.switchStartGridNode();
            end
        end

        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            if length(self.gridNodes) > 0
                return;
            end

            run_id   = 1+length(gridnodes);
            dim      = self.numTargetGridNodes;
            segments = dim-1;

            % x(t) = a2 * t^2 + a1 * t + a0

            A = [self.geoPoints(1).coord]';
            B = [self.geoPoints(3).coord]';
            C = [self.geoPoints(2).coord]';

            % interpolation matrix, resulting from evaluation at t=0, t=1/2 and t=1
            M = [1 0 0 0 0 0; 0 1 0 0 0 0; 1 0 0.5 0 0.25 0; 0 1 0 0.5 0 0.25; 1 0 1 0 1 0; 0 1 0 1 0 1];

            % right hand side = the coordinates of the three given points
            b = [A; B; C];

            % solve system of equations to obtain vectors a2, a1 and a0
            x = M\b;

            a0 = x(1:2);
            a1 = x(3:4);
            a2 = x(5:6);

            % occupy vector of GridNodes with the existing ones
            self.gridNodes = mgen.MgGridNode.empty(0,dim);
            self.gridNodes(1)   = self.geoPoints(1).gnode;
            self.gridNodes(dim) = self.geoPoints(2).gnode;

            % set function values
            self.normFunction.vals(1) = segments;
            if self.normFunction.vals(2) == 0
                self.normFunction.vals(1) = segments;
            end
            if self.normFunction.vals(1) == 1
                self.normFunction.vals(2) = segments;
            end

            % generate new GridNodes
            for ii = 1:segments-1
                scale = self.normFunction.eval( ii/segments );

                self.gridNodes(ii+1) = mgen.MgGridNode( run_id, [a2 * scale^2 + a1 * scale + a0]' );

                gnv(end+1) = self.gridNodes(ii+1);

                run_id = run_id + 1;
            end

        end

        function gp = getGeoPoints(self)
            gp = self.geoPoints;
        end

        function gn = getGridNodes(self)
            gn = self.gridNodes;
        end

    end

end
