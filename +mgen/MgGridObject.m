classdef MgGridObject < matlab.mixin.Heterogeneous & handle
    % A generic object of the grid

    properties
        id = -1;              % identifer
    end

    % methods (Abstract)
    %     print(obj)
    % end

end
