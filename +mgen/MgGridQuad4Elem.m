classdef MgGridQuad4Elem < mgen.MgGridElem
    % A node of the grid

    properties
        nodes = mgen.MgGridNode.empty(0,4);  % 4 corner nodes
    end

    methods
        %% constructor
        function obj = MgGridQuad4Elem(id, nodes)
            if nargin >= 1
                obj.id = id;
            end
            if nargin >= 2
                obj.nodes = nodes;
            end
        end
        %% print data
        function str = print(self)
            str1 = sprintf( 'MgGridQuad4Elem: id = %5d | ', self.id );
            str2 = sprintf( '%5d ', [ self.nodes.id ] );
            str = [ str1, str2 ];
        end
        %% graphical output: plot element
        function plot(self)
            % node coordinates
            X = [ self.nodes.coord ];
            % coordinates
            XX1 = X(1:2:end);
            XX2 = X(2:2:end);
            % plot defaults
            lcolor = [0.7 0.7 0.7];
            mcolor = [0.2 0.2 0.2];
            lwidth = 0.25;
            mwidth = 0.1;
            ecolor = [0.4 0.0 0.0];
            falpha = 0.8;
            marker = '.';
            %
            patch(XX1, XX2, -0.005*[1 1 1 1], lcolor, ...
                    'EdgeColor', ecolor, ...
                    'LineWidth',  lwidth, ...
                    'Marker', marker, ...
                    'MarkerSize', mwidth, ...
                    'FaceAlpha', falpha);
        end
    end

end
