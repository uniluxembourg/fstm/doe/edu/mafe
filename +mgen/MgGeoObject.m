classdef MgGeoObject < matlab.mixin.Heterogeneous & handle
    % A generic object of the geometric description

    properties

    end

    methods (Abstract)
        print(obj)
    end

end
