classdef MgGeoArea4LineAnnulus < mgen.MgGeoArea4Line
    % A 4-line defined annulus area of the geometry

    properties (Access = protected)

    end

    methods
        %% constructor
        function obj = MgGeoArea4LineAnnulus(geoLines)
            if nargin >= 1
                if length(geoLines) ~= 4
                    disp('MgGeoArea4LineAnnulus: wrong number of input GeoLines');
                end
                obj.geoLines = geoLines;
                % check type of geoLines
                if class(obj.geoLines(1)) ~= 'mgen.MgGeoLine2Point'
                    disp('MgGeoArea4LineAnnulus: 1st GeoLine is not of type MgGeoLine2Point');
                end
                if class(obj.geoLines(2)) ~= 'mgen.MgGeoLine2PointSegment'
                    disp('MgGeoArea4LineAnnulus: 2nd GeoLine is not of type MgGeoLine2PointSegment');
                end
                if class(obj.geoLines(3)) ~= 'mgen.MgGeoLine2Point'
                    disp('MgGeoArea4LineAnnulus: 3rd GeoLine is not of type MgGeoLine2Point');
                end
                if class(obj.geoLines(4)) ~= 'mgen.MgGeoLine2PointSegment'
                    disp('MgGeoArea4LineAnnulus: 4th GeoLine is not of type MgGeoLine2PointSegment');
                end
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoArea4LineAnnulus: ' );
        end

        %% ---------------------------------------------------------------------
        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            dim0     = self.geoLines(1).getNumTargetGridNodes();
            dim1     = self.geoLines(2).getNumTargetGridNodes();
            dim      = dim0 * dim1;
            run_id   = 1 + length(gridnodes);

            grow = (dim0-1)*(dim1-1);
            bucket = mgen.MgGridNode(1,grow); bucket(:) = mgen.MgGridNode(-1, [0.0, 0.0]);
            gridnodes = [ gridnodes bucket ];

            self.gridNodes = mgen.MgGridNode.empty(0,dim);

            % check start points of lines
            self.geoLines(1).getCommonGridNode( self.geoLines(2) );
            self.geoLines(3).getCommonGridNode( self.geoLines(4) );

            % change once again, such that A/B and C/D run from the same side
            self.geoLines(3).switchStartGridNode();
            self.geoLines(4).switchStartGridNode();

            % create grid nodes
            for ii = 2:dim0-1 % straight edges

                % determine local coordinates of "same" points on both straight edges
                A = self.geoLines(1).getGridNode(   1).coord;
                B = self.geoLines(1).getGridNode(dim0).coord;
                C = self.geoLines(1).getGridNode(  ii).coord;
                
                z1 = norm((C-A)/(B-A));

                A = self.geoLines(3).getGridNode(   1).coord;
                B = self.geoLines(3).getGridNode(dim0).coord;
                C = self.geoLines(3).getGridNode(  ii).coord;
                
                z3 = norm((C-A)/(B-A));
                
                pos = (ii-1)*dim1 + 2;
                
                for jj = 2:dim1-1 % curved edges

                    A = self.geoLines(2).getGridNode(jj).coord;
                    B = self.geoLines(4).getGridNode(jj).coord;
                    
                    z = (jj-1)/(dim1-1); % local z along curve
                    z = (1-z) * z1 + z * z3; % interpolate outer pos
                    
                    N = (1-z) * A + (z) * B; % new node coords

                    % position node
                    self.gridNodes(pos) = mgen.MgGridNode(run_id, N);

                    gnv(run_id) = self.gridNodes(pos);

                    run_id = run_id + 1;
                    pos = pos + 1;
                end
            end
            
            % sort in existing nodes
            for ii = 1:dim1
                self.gridNodes(ii) = self.geoLines(2).getGridNode(ii);
            end
            for ii = 2:dim0-1
                self.gridNodes((ii-1)*dim1+1) = self.geoLines(1).getGridNode(ii);
            end
            for ii = 2:dim0-1
                self.gridNodes((ii+0)*dim1) = self.geoLines(3).getGridNode(ii);
            end
            for ii = 1:dim1
                self.gridNodes(ii+dim1*(dim0-1)) = self.geoLines(4).getGridNode(ii);
            end
        end

    end
end
