classdef MgGeoAreaCircle < mgen.MgGeoArea
    % A circular area of the (disk) geometry

    properties (Access = protected)
        nrings = 2;   % the number of rings between center part and outer line
        ncircu = 2;   % the number of rings between center part and outer line
        recfac = 0.3; % fraction of the radius to be accupied by internal quadrilateral

        center;       % the center point of the circle

        ringNodes;    % generated grid nodes on the outer boundary (closed connectivity)
    end

    methods
        %% constructor
        function obj = MgGeoAreaCircle(nrings, ncircu, recfac, center)
            if nargin >= 1
                obj.nrings = nrings;
            end
            if nargin >= 2
                obj.ncircu = ncircu;
            end
            if nargin >= 3
                obj.recfac = recfac;
            end
            if nargin >= 4
                obj.center = center;
            else
                obj.center = [ 0.0, 0.0 ];
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoAreaCircle: ' );
        end

        %% ---------------------------------------------------------------------
        function change = prepareLines(self)

            change = false;

            % nothing to be done here
        end

        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            % provide your code that generates the grid nodes here and add them to gnv (the container holding all grid nodes)

            % remember to put the nodes along the outer ring into self.ringNodes (will facilitate the application of bc later)

        end

        function gev = makeGridElems(self, gridelems)

            gev = gridelems;

				% provide your code that generates the grid elements here and add them to gev (the container holding all grid elements)
        end

        function gn = getGridNodesOuter(self)
            gn = self.ringNodes;
        end

        function gn = getGridNodeCenter(self)
            gn = self.center;
        end

    end
end
