classdef MgGeoLine < mgen.MgGeoObject
    % An (unspecified) line of the geometry

    properties

    end

    methods (Abstract)
        getGeoPoint(obj, id)
        getGridNode(obj, id)

        setNumTargetGridNodes(obj, num)
        getNumTargetGridNodes(obj)

        switchStartGridNode(obj)
        switchStartGeoPoint(obj)
        resetStartGridNode(obj)
        resetStartGeoPoint(obj)

        getCommonGridNode(obj, geoline)
        getCommonGeoPoint(obj, geoline)

        alignLines(obj, geoline)

        makeGridNodes(obj, gridnodes)

        getGeoPoints(obj)
        getGridNodes(obj)
    end

end
