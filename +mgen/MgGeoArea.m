classdef MgGeoArea < mgen.MgGeoObject
    % An (unspecified) area of the geometry

    properties (Access = protected)
        gridNodes = mgen.MgGridNode.empty(0,0);
        gridElems = mgen.MgGridElem.empty(0,0);
    end

    methods (Abstract)
        prepareLines(obj)
        makeGridNodes(obj, gridnodes)
        makeGridElems(obj, gridelems)
    end

    methods

      function gn = getGridNodes(self)
          gn = self.gridNodes;
      end

      function ge = getGridElems(self)
          ge = self.gridElems;
      end

    end
end
