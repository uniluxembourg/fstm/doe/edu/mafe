classdef MgNormFunctionType < uint8
    % Enumeration of available function types
    enumeration
        Linear    (1), % linear
        Quadratic (2), % quadratic
        Cubic     (3), % cubic
        Pow       (4), % power law
        % reversed schemes
        QuadraticRev (5), % quadratic Reverse
        CubicRev     (6), % cubic reverse
        PowRev       (7)  % power law reverse
    end
end
