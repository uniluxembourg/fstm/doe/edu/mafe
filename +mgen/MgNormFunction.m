classdef MgNormFunction < handle
    % A description of a normalised distribution

    properties
        type = mgen.MgNormFunctionType.Linear;
        vals = zeros(1,2);
        inv  = false;
    end

    methods
        %% constructor
        function obj = MgNormFunction(type, vals)
            if nargin >= 1
                obj.type = type;
            end
            if nargin >= 2
                obj.vals = vals;
            end
        end
        %% evaluate function
        function val = eval(self, z)
            % check
            if z < 0.0 | z > 1.0
                disp('MgNormFunction:eval() --> Invalid normalised coordinate!');
            end
            % evaluate
            switch (self.type)
                case mgen.MgNormFunctionType.Linear
                    val = z;
                case mgen.MgNormFunctionType.Quadratic
                    val = z.^2;
                case mgen.MgNormFunctionType.Cubic
                    val = z.^3;
                case mgen.MgNormFunctionType.Pow
                    val = ( self.vals(1)^(z*self.vals(2)) - 1.0 ) / ( self.vals(1)^self.vals(2) - 1.0 );
                case mgen.MgNormFunctionType.QuadraticRev
                    val = ((2.0*z)-(z.^2));
                case mgen.MgNormFunctionType.CubicRev
                    val = (1.0-z.^3);
                case mgen.MgNormFunctionType.PowRev
                    val = 1.0 - ( (self.vals(1)^((1.0-z) * self.vals(2)) - 1.0) / (self.vals(1)^self.vals(2) - 1.0) );
                otherwise
                    disp('MgNormFunction:eval() --> Unknown MgNormFunctionType!');
                    val = 0.0*z;
            end
        end
    end
end
