classdef MgGrid < handle
    % A description of the discrete grid by its items (nodes, elements)

    properties

        nodes = mgen.MgGridNode.empty(0,0);
        elems = mgen.MgGridElem.empty(0,0);
    end

    methods
        %% print data
        function str = print(self)
            str = printf('MgGrid: ');
        end
        %% graphical output: plot element
        function plot(self)
            for ele = self.elems
                ele.plot();
            end
            for node = self.nodes
                node.plot();
            end
        end
    end

end
