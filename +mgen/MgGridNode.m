classdef MgGridNode < mgen.MgGridObject
    % A node of the grid

    properties
        coord = zeros(1,2);  % 2D coordinates
    end

    methods
        %% constructor
        function obj = MgGridNode(id, coord)
            if nargin >= 1
                obj.id = id;
            end
            if nargin >= 2
                obj.coord = coord;
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGridNode: id = %5d, x1 = %2.4f, x2 = %2.4f', self.id, self.coord(1), self.coord(2) );
        end
        %% graphical output: plot node
        function plot(self)
            % node coordinates
            X = [ self.coord ];
            % coordinates
            XX1 = X(1:2:end);
            XX2 = X(2:2:end);
            % plot defaults
            mcolor = [0.6 0.0 0.0];
            mwidth = 20.;
            marker = '.';
            %
            plot(XX1, XX2, marker, ...
                'MarkerSize', mwidth, ...
                'MarkerEdgeColor', mcolor, ...
                'MarkerFaceColor', mcolor);
%             t = text(XX1,XX2, sprintf( 'n%d', self.id));
%             t.FontSize = 12;
%             t.HorizontalAlignment = 'center';
%             t.VerticalAlignment = 'baseline';
        end
    end

end
