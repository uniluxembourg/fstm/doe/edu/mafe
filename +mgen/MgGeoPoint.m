classdef MgGeoPoint < mgen.MgGeoObject
    % A point of the geometry

    properties
        coord = zeros(1,2);        % 2D coordinates
        gnode = mgen.MgGridNode(); % generated grid node
    end

    methods
        %% constructor
        function obj = MgGeoPoint(coord)
            if nargin >= 1
                obj.coord = coord;
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoPoint: x1 = %2.4f, x2 = %2.4f', self.coord(1), self.coord(2) );
        end
        %% graphical output: plot point
        function plot(self)
            % point coordinates
            X = [ self.coord ];
            % coordinates
            XX1 = X(1:2:end);
            XX2 = X(2:2:end);
            % plot defaults
            mcolor = [0.0 0.0 0.6];
            mwidth = 50.;
            marker = '.';
            %
            plot(XX1, XX2, marker, ...
                'MarkerSize', mwidth, ...
                'MarkerEdgeColor', mcolor, ...
                'MarkerFaceColor', mcolor);
        end
        %% ---------------------------------------------------------------------
        % generate grid node
        function gnv = makeGridNodes(self, gridnodes)
            self.gnode = mgen.MgGridNode( 1 + length(gridnodes), self.coord );
            gnv = [gridnodes, self.gnode];
        end
        % provide generated grid node
        function gn = getGridNodes(self)
            gn = self.gnode;
        end
    end

end
