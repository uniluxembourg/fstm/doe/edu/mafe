classdef MgGeoArea4LineEllipsoidalVoid < mgen.MgGeoArea4Line
    % A 4-line defined area of the geometry with a ellipsoidal hole

    properties (Access = protected)
        nrings = 2;   % the number of rings between hole and outer lines
        parama = 1;   % ellipse parameter a
        paramb = 1;   % ellipse parameter b
        paramc = 0;   % ellipse rotation angle in rad

        center;       % the center point of the ellipse

        normFunction = mgen.MgNormFunction(); % distribution function towards void

        ringNodes;    % generated grid nodes on the inner boundary (closed connectivity)
    end

    methods
        %% constructor
        function obj = MgGeoArea4LineEllipsoidalVoid(geoLines, nrings, parama, paramb, paramc, nfunc, center)
            if nargin >= 1
                obj.geoLines = geoLines;
            end
            if nargin >= 2
                obj.nrings   = nrings;
            end
            if nargin >= 3
                obj.parama   = parama;
            end
            if nargin >= 4
                obj.paramb   = paramb;
            end
            if nargin >= 5
                obj.paramc   = paramc;
            end
            if nargin >= 6
                obj.normFunction = nfunc;
            end
            if nargin >= 7
                obj.center   = center;
            else
                X = zeros(1,0);
                for gl = geoLines
                    for gp = gl.getGeoPoints()
                        X(1,[end+1 end+2]) = gp.coord;
                    end
                end
                obj.center   = [ mean(X(1:2:end)), mean(X(2:2:end)) ];
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoArea4LineCircularHole: ' );
        end

        %% ---------------------------------------------------------------------
        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;
            for kk = 1:4
                if self.geoLines(kk).getNumTargetGridNodes() - 1 == 0
                    warning('increase the number of nodes in the %d line', kk);
                end
            end
            dim0     = self.geoLines(1).getNumTargetGridNodes() - 1;

            assert(dim0>0);
            dim1     = self.geoLines(2).getNumTargetGridNodes() - 1;
            assert(dim1>0);

            dim2     = self.geoLines(3).getNumTargetGridNodes() - 1;
            assert(dim2>0);
            dim3     = self.geoLines(4).getNumTargetGridNodes() - 1;
            assert(dim3>0);
            dim      = self.nrings*(dim0 + dim1 + dim2 + dim3);
            segments = self.nrings - 1;
            run_id   = 1 + length(gridnodes);

            % ellipse parametrised form
            %
            f0 = self.center';
            f1 = self.parama * [ cos(self.paramc); sin(self.paramc)];
            f2 = self.paramb * [-sin(self.paramc); cos(self.paramc)];
            %
            N = 1000; % sample points for closest_point identification
            t = linspace(0,(1-1/N)*2*pi,N);
            %
            e = f0*ones(1,N) + f1*sin(t) + f2*cos(t);

            self.gridNodes = mgen.MgGridNode.empty(0,dim);
            self.ringNodes = mgen.MgGridNode.empty(0,  0);

            % check start points of lines
            self.geoLines(1).getCommonGridNode( self.geoLines(2) );
            self.geoLines(3).getCommonGridNode( self.geoLines(4) );

            % change once again, such that A/B and C/D run from the same side
            self.geoLines(1).switchStartGridNode();
            self.geoLines(3).switchStartGridNode();

            pos = 1;

            % create grid nodes
            for mm = segments:-1:1 % loop over segments from outer to inner

                % set function values if the normFunction is not set by
                % user
                if  self.normFunction.vals == [0,0]
                    self.normFunction.vals(1) = segments;
                    if self.normFunction.vals(2) == 0
                        self.normFunction.vals(1) = segments;
                    end
                    if self.normFunction.vals(1) == 1
                        self.normFunction.vals(2) = segments;
                    end
                end
                % some bugs occurs if user sets the type of normFunction.
                if self.normFunction.vals(1)>self.normFunction.vals(2)
                   self.normFunction.vals=sort(self.normFunction.vals);
                end

                scale = self.normFunction.eval( mm/segments );

                if isnan(scale)
                    warning('normFunction can not eval %d/%d',mm,segments);
                end

		            for gline = self.geoLines % loop over the four GeoLines

                    for ii = 1:gline.getNumTargetGridNodes - 1 % loop over GridNodes - 1

                        % coordinates of current GridNode
                        P = gline.getGridNode(ii).coord; %
                        % compute distances
                        d = sqrt( (P(1)-e(1,:)).^2 + (P(2)-e(2,:)).^2 );
                        % find t for smallest distance
                        t_closest = t(find( d==min(d) ));
                        % coordinate of point on ellipse
                        E = f0 + f1*sin(t_closest) + f2*cos(t_closest);
                        % coordinate of sought grid node
                        N = P + scale * (E'-P);

                        % place node
                        self.gridNodes(pos) = mgen.MgGridNode(run_id, N);

                        if abs(scale - 1.0) < eps
                            self.ringNodes(end+1) = self.gridNodes(pos);
                        end

                        gnv(end+1) = self.gridNodes(pos);

                        run_id = run_id + 1;
                        pos = pos + 1;
                    end
                end
            end

	          % defensive programming: check if ringNodes is filled.
            assert(~isempty(self.ringNodes));

            % make sure the ring nodes represent a closed line in terms of node connectivity
            self.ringNodes(end+1) = self.ringNodes(1);

            % sort in existing nodes
            for gline = self.geoLines
                for ii = 1:gline.getNumTargetGridNodes - 1
                    % place node
                    self.gridNodes(pos) = gline.getGridNode(ii);
                    pos = pos + 1;
                end
            end
        end

        function gev = makeGridElems(self, gridelems)

            gev = gridelems;
            %
            dim0     = self.geoLines(1).getNumTargetGridNodes() - 1;
            dim1     = self.geoLines(2).getNumTargetGridNodes() - 1;
            dim2     = self.geoLines(3).getNumTargetGridNodes() - 1;
            dim3     = self.geoLines(4).getNumTargetGridNodes() - 1;
            dim      = dim0 + dim1 + dim2 + dim3;
            segments = self.nrings - 1;
            run_id   = 1 + length(gridelems);
            %
            self.gridElems = mgen.MgGridQuad4Elem.empty(0, segments*dim );
            %
            pos = 1;

            for ii = 1:segments
                pi = (ii-1) * dim + 1;
                po = (ii  ) * dim + 1;

                for jj = 1:dim-1

                    n1 = self.gridNodes(po);
                    n2 = self.gridNodes(po+1);
                    n3 = self.gridNodes(pi+1);
                    n4 = self.gridNodes(pi);

                    self.gridElems(pos) = mgen.MgGridQuad4Elem( run_id, [n1, n2, n3, n4] );
                    gev(end+1) = self.gridElems(pos);

                    run_id = run_id + 1;
                    pos = pos + 1;
                    pi = pi + 1;
                    po = po + 1;
                end

                n1 = self.gridNodes(po);
                n2 = self.gridNodes(po+1-dim);
                n3 = self.gridNodes(pi+1-dim);
                n4 = self.gridNodes(pi);

                self.gridElems(pos) = mgen.MgGridQuad4Elem( run_id, [n1, n2, n3, n4] );
                gev(end+1) = self.gridElems(pos);

                run_id = run_id + 1;
            end
        end

        function gn = getGridNodesInner(self)
            gn = self.ringNodes;
        end
    end
end
