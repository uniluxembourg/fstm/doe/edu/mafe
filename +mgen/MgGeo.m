classdef MgGeo < handle
    % A description of the geometry by its items (points, lines, areas)

    properties

        points = mgen.MgGeoPoint.empty(0,0);
        lines  = mgen.MgGeoLine.empty(0,0);
        areas  = mgen.MgGeoArea.empty(0,0);

        grid   = mgen.MgGrid();
    end

    methods
        %% constructor
        function obj = MgGeo(points, lines, areas)
            if nargin >= 2
                obj.points = points;
                obj.lines  = lines;
            end
            if nargin >= 3
                obj.areas  = areas;
            end
            % create new grid object
            obj.grid = mgen.MgGrid();
            % generate mesh with available data
            obj.makeMesh();
        end
        %% print data
        function str = print(self)
            str1 = sprintf('MgGeo: num points = %d, num lines = %d, num areas = %d \n', ...
                            length(self.points), length(self.lines), length(self.areas));
            str2 = sprintf('       mesh --> num nodes = %d, num elems = %d \n', ...
                            length(self.getAllGridNodes()), length(self.getAllGridElems()));
            str = [ str1, str2 ];
            for gn = self.getAllGridNodes()
                str = [ str, gn.print, '\n' ];
            end
            for ge = self.getAllGridElems()
                str = [ str, ge.print, '\n' ];
            end
            str = [ str, '-----------------', '\n' ];
        end
        %% graphical output: plot element
        function plot(self)
            self.grid.plot();
            for point = self.points
                point.plot();
            end
        end
        %% ---------------------------------------------------------------------
        function makeMesh(self)

            disp('Generating mesh...');

            % prepare lines
            run = true;

            %disp('Prepare lines...');
            while run
                run = false;
                for ii = 1:length(self.areas)
                    run = self.areas(ii).prepareLines();
                end
            end

            %
            disp('Make GridNodes on points...');

            for ii = 1:length(self.points)
                self.grid.nodes = self.points(ii).makeGridNodes( self.grid.nodes );
            end
            %
            disp('Make GridNodes on lines...');

            for ii = 1:length(self.lines)
                self.grid.nodes = self.lines(ii).makeGridNodes( self.grid.nodes );
            end
            %
            disp('Make GridNodes on areas...');

            for ii = 1:length(self.areas)
                self.grid.nodes = self.areas(ii).makeGridNodes( self.grid.nodes );
            end
            %
            disp('Make GridElems on areas...');

            for ii = 1:length(self.areas)
                self.grid.elems = self.areas(ii).makeGridElems( self.grid.elems );
            end
            %
            disp('Mesh is ready.');
        end
        %
        function gnv = getAllGridNodes(self)
            gnv = self.grid.nodes();
        end
        %
        function gev = getAllGridElems(self)
            gev = self.grid.elems();
        end

    end

end
