classdef MgGeoLine2PointSegment < mgen.MgGeoLine2Point
    % A 2-point constantly curved line of the geometry

    properties (Access = protected)
        
          signedRadius = 1.0;

    end

    methods
        %% constructor
        function obj = MgGeoLine2PointSegment(geoPoints, signedRadius, numNodes, normFunction)
            if nargin >= 3
                obj.geoPoints = geoPoints;
                obj.signedRadius = signedRadius;
                obj.numTargetGridNodes = numNodes;
                obj.normFunction = mgen.MgNormFunction( mgen.MgNormFunctionType.Linear );
            end
            if nargin >= 4
                obj.normFunction = normFunction;
            end
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoLine2PointSegment: gn1.id = %2d, gn2.id = %2d, R = %f\n', self.gridNodes(1).id, self.gridNodes(end).id, self.signedRadius );

            for gn = self.gridNodes
                str = [ str, gn.print(), '\n' ];
            end
        end

        %% ---------------------------------------------------------------------
        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            if length(self.gridNodes) > 0
                return;
            end

            run_id   = 1+length(gridnodes);
            dim      = self.numTargetGridNodes;
            segments = dim-1;
            x0       = self.geoPoints(1).coord(1);
            x1       = self.geoPoints(1).coord(2);
            y0       = self.geoPoints(2).coord(1);
            y1       = self.geoPoints(2).coord(2);

            R = self.signedRadius;
            q = sqrt((y0 - x0)^2 + (y1 - x1)^2);
            
            % check distance and radius
            if q > 2*R
                disp('MgGeoLine2PointSegment: Given radius too small! Adapting R=distance/2.');
                R = q/2;
            end
            
            z0 = (x0 + y0) / 2 + sqrt(R*R - ((q/2)*(q/2))) * ((x1 - y1)/q);
            z1 = (x1 + y1) / 2 + sqrt(R*R - ((q/2)*(q/2))) * ((y0 - x0)/q);
            
            lx0      = self.geoPoints(2).coord(1) - x0;
            lx1      = self.geoPoints(2).coord(2) - x1;
            
            phi  = 2 * asin(0.5*sqrt(lx0^2+lx1^2)/abs(R));
            phi0 = atan2(x1-z1,x0-z0);

            % occupy vector of GridNodes with the existing ones
            self.gridNodes = mgen.MgGridNode.empty(0,dim);
            self.gridNodes(1)   = self.geoPoints(1).gnode;
            self.gridNodes(dim) = self.geoPoints(2).gnode;

            % set function values
            self.normFunction.vals(1) = segments;
            if self.normFunction.vals(2) == 0
                self.normFunction.vals(1) = segments;
            end
            if self.normFunction.vals(1) == 1
                self.normFunction.vals(2) = segments;
            end

            % generate new GridNodes
            for ii = 1:segments-1
                scale = self.normFunction.eval( ii/segments );

                self.gridNodes(ii+1) = mgen.MgGridNode( run_id, [z0+R*cos(scale*phi+phi0), z1+R*sin(scale*phi+phi0)] );

                gnv(end+1) = self.gridNodes(ii+1);

                run_id = run_id + 1;
            end

        end

    end

end
