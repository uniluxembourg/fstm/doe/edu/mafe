classdef MgGeoLineMulti < mgen.MgGeoLine
    % A 2-point line of the geometry

    properties (Access = private)
        geoLines  = mgen.MgGeoLine.empty(0,0);

        geoPoints = mgen.MgGeoPoint.empty(0,2);
        gridNodes = mgen.MgGridNode.empty(0,0);

        numTargetGridNodes  = 0;

        switchGridNode = false;
        switchGeoPoint = false;
    end

    methods (Access = private)
        function b = alignMultiLine(self)

            self.geoLines(1).resetStartGridNode();
            self.geoLines(1).resetStartGeoPoint();

            for ii = 1:length(self.geoLines)-1
                self.geoLines(ii).alignLines( self.geoLines(ii+1) );
                % if ~b
                %     b = false;
                %     return;
                % end
            end
            b = true;
        end
    end

    methods
        %% constructor
        function obj = MgGeoLineMulti(geoLines, numNodes)
            if nargin >= 1
                obj.geoLines = geoLines;
            end
            if nargin >= 2
                obj.numTargetGridNodes = numNodes;
            end
            obj.init();
        end
        %% print data
        function str = print(self)
            str = sprintf( 'MgGeoLineMulti: num geo lines = %2d\n', length(self.geoLines) );

            for gl = self.geoLines
                str = [ str, gl.print(), '\n' ];
            end
        end
        %% initialise private data
        function init(self)
            if ~self.alignMultiLine();
                disp('MgGeoLineMulti: Involved could not be aligned!');
            end
            self.geoPoints(1) = self.geoLines(  1).getGeoPoint(1);
            self.geoPoints(2) = self.geoLines(end).getGeoPoint(2);
        end

        %% ---------------------------------------------------------------------
        function p = getGeoPoint(self,id)
            if self.switchGeoPoint
                p = self.geoPoints(3-id);
            else
                p = self.geoPoints(id);
            end
        end

        function g = getGridNode(self, id)
            if ~self.switchGridNode
                g = self.gridNodes(id);
            else
                g = self.gridNodes(end-id+1);
            end
        end

        function setNumTargetGridNodes(self, num)

            self.numTargetGridNodes = num;

            % determine a balanced distribution of nodes, rest to center
            lnum = length(self.geoLines);
            dnum = num-lnum*2+(lnum-1);
            target_num = (2+floor(dnum/lnum)) * ones(1,lnum);
            leftover = mod(dnum,lnum);
            middle = floor(lnum/2) + 1;
            target_num( middle ) = target_num( middle ) + leftover;

            for ii = 1:length(self.geoLines)
                self.geoLines(ii).setNumTargetGridNodes( target_num(ii) );
            end

        end

        function num = getNumTargetGridNodes(self)

            tnum = [];

            for gl = self.geoLines
                tnum(end+1) = gl.getNumTargetGridNodes() - 1;
            end

            tnum;

            num = sum( tnum ) + 1;

            if num < self.numTargetGridNodes
                self.setNumTargetGridNodes( self.numTargetGridNodes );
            else
                self.numTargetGridNodes = num;
            end

            num = self.numTargetGridNodes;
        end

        function switchStartGridNode(self)
            self.switchGridNode = ~self.switchGridNode;
        end
        function switchStartGeoPoint(self)
            self.switchGeoPoint = ~self.switchGeoPoint;
        end

        function resetStartGridNode(self)
            self.switchGridNode = false;
        end
        function resetStartGeoPoint(self)
            self.switchGeoPoint = false;
        end

        function g = getCommonGridNode(self, geoline)

            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                geoline.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                self.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                geoline.switchStartGridNode();
            end
            if self.getGridNode(1).id ~= geoline.getGridNode(1).id
                disp('MgGeoLineMulti:getCommonGridNode() --> GridNodes not matching!')
            end
            g = self.getGridNode(1);
        end

        function g = getCommonGeoPoint(self, geoline)
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                geoline.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                self.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                geoline.switchStartGeoPoint();
            end
            if self.getGeoPoint(1) ~= geoline.getGeoPoint(1)
                disp('MgGeoLineMulti:getCommonGeoPoint() --> GeoPoints not matching!')
            end
            g = self.getGeoPoint(1);
        end

        function alignLines(self, geoline)
            self.getCommonGeoPoint(geoline);
            self.switchStartGeoPoint();

            if length(self.gridNodes) > 0
                self.getCommonGridNode(geoline);
                self.switchStartGridNode();
            end
        end

        function gnv = makeGridNodes(self, gridnodes)

            gnv = gridnodes;

            if length(self.gridNodes) > 0
                return;
            end

            for gl = self.geoLines
                gnv = gl.makeGridNodes( gnv );
            end

            self.alignMultiLine();

            dim = self.getNumTargetGridNodes();
            self.gridNodes = mgen.MgGridNode.empty(0,dim);

            kk = 1;
            for ii = 1:length(self.geoLines)
                for jj = 1:self.geoLines(ii).getNumTargetGridNodes()-1
                    self.gridNodes(kk) = self.geoLines(ii).getGridNode(jj);
                    kk = kk + 1;
                end
            end

            %last
            last_gn = self.geoLines(end).getGridNodes();
            self.gridNodes(kk) = last_gn(end);
        end

        function gp = getGeoPoints(self)
            gp = self.geoPoints;
        end

        function gn = getGridNodes(self)
            gn = self.gridNodes;
        end

    end

end
