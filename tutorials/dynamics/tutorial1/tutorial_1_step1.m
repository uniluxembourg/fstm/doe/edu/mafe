%% Tutorial 1: Structural Dynamics
% A frame system with point mass and spring - static and dynamic analysis
%
% step 1 (task 2.1-2.2):
%
% definition of cross section and material properties
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of sections (containing geometrical data and material data)
s1 = mafe.Section1D(); % for the members
s1.A   = 5.0e-4; % [m^2] = 0.01m * 0.05m
s1.Iy  = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.E   = 2.0e11; % [N/m^2]
s1.rho = 8.0e+3; % [kg/m^3]
s2 = mafe.Section0D(); % for the point mass (m, but no k)
s2.m   = 50.0;   % [kg]
s2.k   = 0.00;   % [N]
s3 = mafe.Section0D(); % for the point spring (k, but no m)
s3.m   = 0.00;   % [kg]
s3.k   = 1000;   % [N]

% show the objects created above and the variables they hold
s1
s2
