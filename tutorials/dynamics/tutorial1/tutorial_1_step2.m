%% Tutorial 1: Structural Dynamics
% A frame system with point mass and spring - static and dynamic analysis
%
% step 2 (task 2.3):
%
% definition of nodes and beam/point elements
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of sections (containing geometrical data and material data)
s1 = mafe.Section1D(); % for the members
s1.A   = 5.0e-4; % [m^2] = 0.01m * 0.05m
s1.Iy  = 1.0e-7; % [m^4] = 0.01m * (0.05m)^3 / 12
s1.E   = 2.0e11; % [N/m^2]
s1.rho = 8.0e+3; % [kg/m^3]
s2 = mafe.Section0D(); % for the point mass (m, but no k)
s2.m   = 50.0;   % [kg]
s2.k   = 0.00;   % [N]
s3 = mafe.Section0D(); % for the point spring (k, but no m)
s3.m   = 0.00;   % [kg]
s3.k   = 1000;   % [N]
%% Definition of nodes in the system
% Node 1
n1 = mafe.Node2D( [0.0, 0.0] );
% Node 2
n2 = mafe.Node2D( [0.0, 2.5] );
% Node 3
n3 = mafe.Node2D( [2.0, 2.5] );
% Node 4
n4 = mafe.Node2D( [4.0, 2.5] );
% Vector of nodes
nodes = [ n1, n2, n3, n4 ];
%% Definition of elements in the system
% Element 1
e1 = mafe.Member2D( [n1, n2], s1 );
% Element 2
e2 = mafe.Member2D( [n2, n3], s1 );
% Element 3
e3 = mafe.Member2D( [n3, n4], s1 );
% Element 4
e4 = mafe.Point( n3, s2, mafe.DofType.Disp1 );
% Element 5
e5 = mafe.Point( n3, s2, mafe.DofType.Disp2 );
% Element 6
e6 = mafe.Point( n4, s3, mafe.DofType.Disp1 );
% vector of elements
elems = [ e1, e2, e3, e4, e5, e6 ];
