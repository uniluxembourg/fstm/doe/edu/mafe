%% Tutorial 2: Structural Dynamics
% A frame system with point mass and spring - dynamic analysis (free and forced)
%
% step 1 (task 2.1):
%
% definition of time functions 
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of time functions (used to describe time-dependent values)
tF = mafe.TimeFunction( 2.5, 1.0, 0.0 ); % force: Omega = 2.5 [rad/s], a_c = 1.0, a_s = 0.0
tU = mafe.TimeFunction( 5.0, 0.0, 1.0 ); % base : Omega = 5.0 [rad/s], a_c = 0.0, a_s = 1.0
tfuns = [tF, tU];
