%% Example Plate bending
% example on how to use the integrated mesh generator for
% a specifically supported L-shaped plate domain under area and point loads
%
% step 1 (task 1):
% 
% generate the geometrical description and finite element mesh
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% helfpul parameters (problem specific)
d = 2; n = 1; % d: length measure; n = number of grid nodes per d
% function that controls grid node distribution along geo lines
nf1 = mgen.MgNormFunction( mgen.MgNormFunctionType.Pow   , [0.0, 1.0] );
nf2 = mgen.MgNormFunction( mgen.MgNormFunctionType.PowRev, [0.0, 1.0] );
% create all geo points %% geo point coordinates specified in brackets []
gp1 = mgen.MgGeoPoint( [0*d, 0*d] );
gp2 = mgen.MgGeoPoint( [4*d, 0*d] );
gp3 = mgen.MgGeoPoint( [0*d, 3*d] );
gp4 = mgen.MgGeoPoint( [4*d, 3*d] );
gp5 = mgen.MgGeoPoint( [8*d, 3*d] );
gp6 = mgen.MgGeoPoint( [0*d, 8*d] );
gp7 = mgen.MgGeoPoint( [4*d, 8*d] );
gp8 = mgen.MgGeoPoint( [8*d, 8*d] );
% create all geo lines %% geo line identified as continuous line between specified geo points, n presenting number of nodes per element
gl1  = mgen.MgGeoLine2Point( [gp1, gp2], 4*n, nf2 );
gl2  = mgen.MgGeoLine2Point( [gp3, gp4],   2, nf2 );
gl3  = mgen.MgGeoLine2Point( [gp6, gp7],   2, nf2 );
gl4  = mgen.MgGeoLine2Point( [gp4, gp5], 4*n, nf1 );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],   2, nf1 );
gl6  = mgen.MgGeoLine2Point( [gp1, gp3], 3*n, nf2 );
gl7  = mgen.MgGeoLine2Point( [gp2, gp4],   2, nf2 );
gl8  = mgen.MgGeoLine2Point( [gp3, gp6], 5*n, nf1 );
gl9  = mgen.MgGeoLine2Point( [gp4, gp7],   2, nf1 );
gl10 = mgen.MgGeoLine2Point( [gp8, gp5],   2, nf2 );
% create all geo areas %% geo area identified as area enclosed by specified geo lines
ga1 = mgen.MgGeoArea4Line( [gl1, gl7, gl2, gl6] );
ga2 = mgen.MgGeoArea4Line( [gl2, gl9, gl3, gl8] );
ga3 = mgen.MgGeoArea4Line( [gl4, gl10, gl5, gl9] );
% create geo object and generate the mesh %% include all geo points, geo lines and geo areas
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10], ...
                  [ga1, ga2, ga3] );
% print geo and mesh data info
% fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
geo.plot();
