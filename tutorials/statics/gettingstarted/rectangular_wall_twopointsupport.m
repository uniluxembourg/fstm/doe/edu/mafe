%% 2D Elasticity Example
% example on how to use the integrated mesh generator for
% a rectangular block loaded one the top while fixed on outer boudaries
clear all; addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E     = 2.0e11; % [N/m^2] Young's modulus
s1.nue   = 0.0;    % [-] Poisson's ratio
s1.t     = 1.0e-2; % [m] Section thickness
s1.state = 'plane_stress';
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator                              
% helfpul parameters (problem specific)                                                       qqqqqqqqqqqqqqqqqqqqqq
lx = 10; ly = 5; n = 16+1; % lx, ly: total lengths ; n: number of elements per direction     ------------------------
%                                                                                           |                        | 
% create all geo points in order to determine problem geometry                              |                        |
gp1 = mgen.MgGeoPoint( [ 0.0,  0.0] ); % creating a corner point                            |                        | ly
gp2 = mgen.MgGeoPoint( [lx,  0.0] ); % creating a corner point                              |                        |
gp3 = mgen.MgGeoPoint( [lx,  ly] ); % creating a corner point                             |>|                        |
gp4 = mgen.MgGeoPoint( [0.0, ly] ); % creating a corner point                               -------------------------
%                                                                                           ^          lx           ^  
%                                                                                           -                       -
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1],  n ); % geoline identified as continuous line between gp2 and gp1, n presenting number of elements
gl2 = mgen.MgGeoLine2Point( [gp2, gp3],  n ); % geoline identified as continuous line between gp2 and gp3, n presenting number of elements
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],  n ); % geoline identified as continuous line between gp4 and gp3, n presenting number of elements
gl4 = mgen.MgGeoLine2Point( [gp4, gp1],  n ); % geoline identified as continuous line between gp4 and gp1, n presenting number of elements

% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] ); % geo area identified as area enclosed by specified geopoints

% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4],...
    [gl1, gl2, gl3, gl4],...
    [ga1] );
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), s1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% point support in lower left edge, fixed in x and y directions
nn = nodes( [gp1.getGridNodes.id] ); % get node on GeoPoint 1 
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% point support in lower right edge, fixed in x direction
nn = nodes( [gp2.getGridNodes.id] ); % get node on GeoPoint 2 
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% horizontal force on upper boundary
nn = nodes( [gl3.getGridNodes.id] ); % get nodes on GeoLine 3
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [0.0, -4e3] );

%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
title('reference and deformation')
fep.plotSystem('reference');
fep.plotSystem('deformed', 5e3); %deformation was scaled for visual aid
fig = figure(2); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('tensor', 1.e-7); title('tensor')
fig = figure(3); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('vonmises'); colorbar; title('vonmises')
%fep.printSystem();
