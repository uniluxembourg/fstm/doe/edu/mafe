%% Example Plate Bending
% example on how to use the integrated mesh generator for a simple
% rectangular plate with Navier-type support and constant area load
% Concrete slab in office building is used for this example
clear all; addpath([ '..' filesep '..' filesep '..' filesep ]);
%% Definition of sections
s1 = mafe.Section2D();
s1.E   = 3e10; % [N/m^2] Young's modulus for concrete
s1.nue = 0.2;   % [-] Poisson's ratio for concrete
s1.t   = 0.3;   % [m] Thickness of concrete section
%% Definition of element loads
p1 = mafe.EleLoad( mafe.DofType.Disp3, -3e10 ); % vertical load in z [N/m^2], (imposed load q, category B eurocode)
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% helfpul parameters (problem specific)
lx = 10; ly = 10.0; n = 16+1; % lx, ly: total lengths ; n: number of elements per direction       D--------lx---------C
%                                                                                                 |                   |
% create all geo points in order to determine problem geometry                                    |                   |
gp1 = mgen.MgGeoPoint( [ 0.0,  0.0] ); % creating a corner point - this will be column A         ly                  ly
gp2 = mgen.MgGeoPoint( [lx,  0.0] ); % creating a corner point - this will be column B            |                   |
gp3 = mgen.MgGeoPoint( [lx,  ly] ); % creating a corner point - this will be column C             |                   |
gp4 = mgen.MgGeoPoint( [0.0, ly] ); % creating a corner point - this will be column D             A--------lx---------B
%                                                                                             
% create all geo lines
gl1 = mgen.MgGeoLine2Point( [gp2, gp1],  n ); % geoline identified as continuous line between gp2 and gp1, n presenting number of elements
gl2 = mgen.MgGeoLine2Point( [gp2, gp3],  n ); % geoline identified as continuous line between gp2 and gp3, n presenting number of elements
gl3 = mgen.MgGeoLine2Point( [gp4, gp3],  n ); % geoline identified as continuous line between gp4 and gp3, n presenting number of elements
gl4 = mgen.MgGeoLine2Point( [gp4, gp1],  n ); % geoline identified as continuous line between gp4 and gp1, n presenting number of elements

% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl2, gl3, gl4] ); % geo area identified as area enclosed by specified geopoints

% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4],...
    [gl1, gl2, gl3, gl4],...
    [ga1] );
% print geo and mesh data info - %% this option can be activated in order to obtain data info in the command window
%fprintf(geo.print());
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Plate2D( nodes( [ ge.nodes.id ] ), s1, p1 );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0); % constraints need to be defined accurately in order to properly simulate the problem
% Dirichlet constraint represents possibility of displacement, while  Neumann represents force

% support in column A
nn = nodes( [gp1.getGridNodes.id] ); % get nodes on column A
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ); % disp3 representing z direction
% support in column B
nn = nodes( [gp2.getGridNodes.id] ); % get nodes on column B
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ); % disp3 representing z direction
% support in column C
nn = nodes( [gp3.getGridNodes.id] ); % get nodes on column C
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ); % disp3 representing z direction
% support in column D
nn = nodes( [gp4.getGridNodes.id] ); % get nodes on column D
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp3, mafe.ConstraintType.Dirichlet, 0.0 ); % disp3 representing z direction

%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const ); % FeProblem concerning nodes, elements and constraints.
% %% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep); % static analysis of the problem
% %% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%  -----------------------------------------------------------INSPECT RESULTS---
% %% Visualise system response
fig = figure(1); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
title('reference and deformation, rotate in 3D')
fep.plotSystem('reference'); %% starting point of the  problem
fep.plotSystem('deformed', 2e-5); %deformation was scaled for visual aid
fig = figure(2); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('moment22'); colorbar(); title('moment in yy direction')
fig = figure(3); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('shear13'); colorbar(); title('shear')
fig = figure(4); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('tensor', 8e-13); title('tensor')
fig = figure(5); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on; %% this is used to properly position the figure
fep.plotSystem('vonmises'); colorbar; title('vonmisses')
% % fep.printSystem();
