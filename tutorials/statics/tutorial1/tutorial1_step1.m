%% Example Membrane in Plane Stress 2d
% example on how to use the integrated mesh generator for
% a L-shaped domain under boundary loads
%
% step 1 (task 1.3):
% 
% generate the geometrical description and finite element mesh
% (coarse mesh without refinement)
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% helfpul parameters (problem specific)
d = 0.005; n = 1; % d: length measure; n = number of grid nodes per d
% create all geo points
gp1 = mgen.MgGeoPoint( [0*d, 0*d] );
gp2 = mgen.MgGeoPoint( [4*d, 0*d] );
gp3 = mgen.MgGeoPoint( [0*d, 3*d] );
gp4 = mgen.MgGeoPoint( [4*d, 3*d] );
gp5 = mgen.MgGeoPoint( [8*d, 3*d] );
gp6 = mgen.MgGeoPoint( [0*d, 8*d] );
gp7 = mgen.MgGeoPoint( [4*d, 8*d] );
gp8 = mgen.MgGeoPoint( [8*d, 8*d] );
% create all geo lines
gl1  = mgen.MgGeoLine2Point( [gp1, gp2], 4*n );
gl2  = mgen.MgGeoLine2Point( [gp3, gp4],   2 );
gl3  = mgen.MgGeoLine2Point( [gp6, gp7],   2 );
gl4  = mgen.MgGeoLine2Point( [gp4, gp5], 4*n );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],   2 );
gl6  = mgen.MgGeoLine2Point( [gp1, gp3], 3*n );
gl7  = mgen.MgGeoLine2Point( [gp2, gp4],   2 );
gl8  = mgen.MgGeoLine2Point( [gp3, gp6], 5*n );
gl9  = mgen.MgGeoLine2Point( [gp4, gp7],   2 );
gl10 = mgen.MgGeoLine2Point( [gp8, gp5],   2 );
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl7, gl2, gl6] );
ga2 = mgen.MgGeoArea4Line( [gl2, gl9, gl3, gl8] );
ga3 = mgen.MgGeoArea4Line( [gl4, gl10, gl5, gl9] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10], ...
                  [ga1, ga2, ga3] );
% print geo and mesh data info
fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot();