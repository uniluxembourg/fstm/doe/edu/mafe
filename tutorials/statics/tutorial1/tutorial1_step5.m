%% Example Membrane in Plane Stress 2d
% example on how to use the integrated mesh generator for
% a L-shaped domain under boundary loads
% that generate a stress peak in the concave corner (therefore mesh refinement)
%
% step 5 (task 4.1):
%
% visualise the analysis results for the deformed state, the stress states,
% the tensor field representation and the von Mises stress state
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% function that controls grid node distribution along geo lines
nf = mgen.MgNormFunction( mgen.MgNormFunctionType.Pow   , [0.0, 1.0] );
% helfpul parameters (problem specific)
d = 0.005; n = 5; % d: length measure; n = number of grid nodes per d
% create all geo points
gp1 = mgen.MgGeoPoint( [0*d, 0*d] );
gp2 = mgen.MgGeoPoint( [4*d, 0*d] );
gp3 = mgen.MgGeoPoint( [0*d, 3*d] );
gp4 = mgen.MgGeoPoint( [4*d, 3*d] );
gp5 = mgen.MgGeoPoint( [8*d, 3*d] );
gp6 = mgen.MgGeoPoint( [0*d, 8*d] );
gp7 = mgen.MgGeoPoint( [4*d, 8*d] );
gp8 = mgen.MgGeoPoint( [8*d, 8*d] );
% create all geo lines
gl1  = mgen.MgGeoLine2Point( [gp2, gp1], 4*n, nf );
gl2  = mgen.MgGeoLine2Point( [gp4, gp3],   2, nf );
gl3  = mgen.MgGeoLine2Point( [gp7, gp6],   2, nf );
gl4  = mgen.MgGeoLine2Point( [gp4, gp5], 4*n, nf );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],   2, nf );
gl6  = mgen.MgGeoLine2Point( [gp3, gp1], 3*n, nf );
gl7  = mgen.MgGeoLine2Point( [gp4, gp2],   2, nf );
gl8  = mgen.MgGeoLine2Point( [gp3, gp6], 5*n, nf );
gl9  = mgen.MgGeoLine2Point( [gp4, gp7],   2, nf );
gl10 = mgen.MgGeoLine2Point( [gp5, gp8],   2, nf );
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl7, gl2, gl6] );
ga2 = mgen.MgGeoArea4Line( [gl2, gl9, gl3, gl8] );
ga3 = mgen.MgGeoArea4Line( [gl4, gl10, gl5, gl9] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10], ...
                  [ga1, ga2, ga3] );
% print geo and mesh data info
%fprintf(geo.print());
% plot mesh
%cla; clf;
%fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
%geo.plot();
%  -----------------------------------------------------------DEFINE FE ITEMS---
%% Definition of sections
section = mafe.Section2D();
section.E     = 2.0e11; % [N/m^2]
section.nue   = 0.3;    % [-]
section.t     = 0.4e-2; % [m]
section.state = 'plane_stress';
%% Definition of nodes in the system
nodes = mafe.Node.empty(0,0);
for gn = geo.getAllGridNodes()
    nodes(end+1) = mafe.Node2D( gn.coord );
end
%% Definition of elements in the system
elems = mafe.Element.empty(0,0);
for ge = geo.getAllGridElems()
    elems(end+1) = mafe.Membrane2D( nodes( [ ge.nodes.id ] ), section );
end
%% Definition of constraints in the system
const = mafe.Constraint.empty(0,0);
% left edge: fix in x1
nn = nodes( [gl6.getGridNodes.id, gl8.getGridNodes.id] ); % get nodes on GeoLine 6+8
%nn = nodes( [gp1.getGridNodes.id] ); % get node on GeoPoint 1
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp1, mafe.ConstraintType.Dirichlet, 0.0 );
% bottom edge: fix in x2
nn = nodes( [gl1.getGridNodes.id] ); % get nodes on GeoLine 4
const(end+1) = mafe.ConstraintNode( nn, mafe.DofType.Disp2, mafe.ConstraintType.Dirichlet, 0.0 );
% define line load
q = 1.e5 * section.t;
% top edge: vertical distributed force q [N/m]
nn = nodes( [gl3.getGridNodes.id] ); % get nodes on GeoLine 3
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [  0.0,-q] );
nn = nodes( [gl5.getGridNodes.id] ); % get nodes on GeoLine 5
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [  0.0,+q] );
% right edge: vertical distributed force q [N/m]
nn = nodes( [gl10.getGridNodes.id] ); % get nodes on GeoLine 10
const(end+1) = mafe.ConstraintEdge( nn, [mafe.DofType.Disp1, mafe.DofType.Disp2], mafe.ConstraintType.Neumann, [-q,  0.0] );
%  --------------------------------------------DEFINE FE PROBLEM AND ANALYSIS---
%% Setup of the finite element problem
fep = mafe.FeProblem( nodes, elems, const );
%% Select an analysis type: static
ana = mafe.FeAnalysisStatic(fep);
%% Calculate response
ana.analyse(); disp( sprintf('---> number dof =  %5d', fep.ndofs) );
%% Print numerical values to screen
%fep.printSystem();
%  -----------------------------------------------------------INSPECT RESULTS---
%% Visualise system response
cla; clf;
fig = figure(1); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('reference');
fep.plotSystem('deformed', 1e4); % amplification by factor 10000 for visualisation
title('Undeformed and deformed state')
fig = figure(2); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('stress11'); colorbar; % with colorbar legend
title('stress s_{11}')
fig = figure(3); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('stress22'); colorbar; % with colorbar legend
title('stress s_{22}')
fig = figure(4); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('stress12'); colorbar; % with colorbar legend
title('stress s_{12}')
fig = figure(5); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('vonmises'); colorbar; % with colorbar legend
title('von Mises stress')
fig = figure(6); clf; subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
fep.plotSystem('tensor', 5.e-9); % amplification by factor for proper presentation
title('principal stress orientations')
