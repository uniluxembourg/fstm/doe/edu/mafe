%% Example Membrane in Plane Stress 2d
% example on how to use the integrated mesh generator for
% a L-shaped domain under boundary loads
% that generate a stress peak in the concave corner (therefore mesh refinement)
%
% step 2 (task 1.4-1.5):
% 
% mesh refinement towards the concave corner using MgNormFunction
% (finer mesh of 1000+ nodes with directional refinement)
%
addpath([ '..' filesep '..' filesep '..' filesep ]);
%  -----------------------------------------------------------MESH GENERATION---
%% Describe the goemetry of the problem for the mesh generator
% function that controls grid node distribution along geo lines
nf = mgen.MgNormFunction( mgen.MgNormFunctionType.Pow   , [0.0, 1.0] );
% helfpul parameters (problem specific)
d = 0.005; n = 5; % d: length measure; n = number of grid nodes per d
% create all geo points
gp1 = mgen.MgGeoPoint( [0*d, 0*d] );
gp2 = mgen.MgGeoPoint( [4*d, 0*d] );
gp3 = mgen.MgGeoPoint( [0*d, 3*d] );
gp4 = mgen.MgGeoPoint( [4*d, 3*d] );
gp5 = mgen.MgGeoPoint( [8*d, 3*d] );
gp6 = mgen.MgGeoPoint( [0*d, 8*d] );
gp7 = mgen.MgGeoPoint( [4*d, 8*d] );
gp8 = mgen.MgGeoPoint( [8*d, 8*d] );
% create all geo lines
gl1  = mgen.MgGeoLine2Point( [gp2, gp1], 4*n, nf );
gl2  = mgen.MgGeoLine2Point( [gp4, gp3],   2, nf );
gl3  = mgen.MgGeoLine2Point( [gp7, gp6],   2, nf );
gl4  = mgen.MgGeoLine2Point( [gp4, gp5], 4*n, nf );
gl5  = mgen.MgGeoLine2Point( [gp7, gp8],   2, nf );
gl6  = mgen.MgGeoLine2Point( [gp3, gp1], 3*n, nf );
gl7  = mgen.MgGeoLine2Point( [gp4, gp2],   2, nf );
gl8  = mgen.MgGeoLine2Point( [gp3, gp6], 5*n, nf );
gl9  = mgen.MgGeoLine2Point( [gp4, gp7],   2, nf );
gl10 = mgen.MgGeoLine2Point( [gp5, gp8],   2, nf );
% create all geo areas
ga1 = mgen.MgGeoArea4Line( [gl1, gl7, gl2, gl6] );
ga2 = mgen.MgGeoArea4Line( [gl2, gl9, gl3, gl8] );
ga3 = mgen.MgGeoArea4Line( [gl4, gl10, gl5, gl9] );
% create geo object and generate the mesh
geo = mgen.MgGeo( [gp1, gp2, gp3, gp4, gp5, gp6, gp7, gp8], ...
                  [gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10], ...
                  [ga1, ga2, ga3] );
% print geo and mesh data info
fprintf(geo.print());
% plot mesh
cla; clf;
fig = figure(1); subplot('Position',[0.05 0.05 0.90 0.90]); axis equal; hold on;
geo.plot();